import React from "react";
import { shallow } from "enzyme";
import { Table } from "./Table";

describe( "Table", () => {
    it( "renders 1 <Table /> component", () => {
        const component = shallow( <Table /> );
        expect( component ).toHaveLength( 1 );
    } );
} );
