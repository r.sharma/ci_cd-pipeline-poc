import React from "react";
import { shallow, mount } from "enzyme";
import { Toggle } from "./Toggle";

describe( "Toggle", () => {
    it( "renders 1 <Toggle /> component", () => {
        const component = shallow( <Toggle /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "mount wrapper label", () => {
        const props = {
            checked: true,
            label: "Label-1"
        };
        const component = mount( <Toggle {...props} /> );

        expect( component.childAt( 0 ).hasClass( "label-toggle-default" ) ).toEqual( true );
        expect( component.state().checked ).toEqual( true );
        expect( component.find( ".title" ).text() ).toEqual( props.label );
    } );

    describe( "mount wrapper without label", () => {
        const props = {
            checked: true
        };
        const component = mount( <Toggle {...props} /> );

        expect( component.childAt( 0 ).hasClass( "label-toggle-default" ) ).toEqual( true );
        expect( component.state().checked ).toEqual( true );
        expect( component.find( ".title" ) ).toHaveLength( 0 );
    } );
} );
