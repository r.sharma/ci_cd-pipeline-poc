import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import { CustomizeBlock } from "./CustomizeBlock";
import { listEnabledItem, columnsPosts } from "services/Post";
import { columnsProfiles } from "services/Profile";

describe( "CustomizeBlockDisplay", () => {
    describe( "check props in component block view", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();
            // creates the store with any initial state or middleware needed
            store = mockStore( {
                label: "label",
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "block",
                        page_card_display_view: "block-description-x1",
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "block",
                        page_card_display_view: "block-description-x1",
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                }
            } );
        } );

        it( "Should render the component", () => {
            const wrapper = shallow( <CustomizeBlock store={store} /> ).dive( {
                context: { store: store }
            } );
            expect( wrapper ).not.toBe( null );

            const props = wrapper.instance().props;

            const wrapBox = wrapper.find( ".customize-body" );
            expect( wrapBox.length ).toBe( 1 );
            expect( wrapBox.childAt( 0 ).type() ).toBe( "div" );
            expect(
                wrapBox
                    .childAt( 0 )
                    .childAt( 0 )
                    .type()
            ).toBe( "button" );
            expect(
                wrapBox
                    .childAt( 0 )
                    .childAt( 0 )
                    .childAt( 0 )
                    .type()
            ).toBe( "svg" );
            expect( wrapBox.childAt( 1 ).type() ).toBe( "div" );
            expect( wrapper.state().open ).toBeFalsy();
            expect( wrapBox.childAt( 1 ).props().className ).toBe( "customize-menu" );
            wrapper.instance().changeStateDropDown();
            expect( wrapper.state().open ).toBeTruthy();

            expect( props.pageView ).toBe( "block" );
            expect(
                wrapper
                    .find( ".customize-menu__header" )
                    .childAt( 0 )
                    .text()
            ).toBe( "Display options" );
        } );
    } );

    describe( "check props in component block table", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();

            // creates the store with any initial state or middleware needed
            store = mockStore( {
                label: "label",
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "row",
                        page_card_display_view: "block-description-x1",
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "row",
                        page_card_display_view: "block-description-x1",
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                }
            } );
        } );

        it( "Should render the component", () => {
            const wrapper = shallow( <CustomizeBlock store={store} /> ).dive( {
                context: { store: store }
            } );
            expect( wrapper ).not.toBe( null );

            const props = wrapper.instance().props;

            const wrapBox = wrapper.find( ".customize-body" );
            expect( wrapBox.length ).toBe( 1 );
            expect( wrapBox.childAt( 0 ).type() ).toBe( "div" );
            expect(
                wrapBox
                    .childAt( 0 )
                    .childAt( 0 )
                    .type()
            ).toBe( "button" );
            expect(
                wrapBox
                    .childAt( 0 )
                    .childAt( 0 )
                    .childAt( 0 )
                    .type()
            ).toBe( "svg" );
            expect( wrapBox.childAt( 1 ).type() ).toBe( "div" );
            expect( wrapper.state().open ).toBeFalsy();
            expect( wrapBox.childAt( 1 ).props().className ).toBe( "customize-menu" );
            wrapper.instance().changeStateDropDown();
            expect( wrapper.state().open ).toBeTruthy();

            expect( props.pageView ).toBe( "row" );

            expect(
                wrapper
                    .find( ".customize-menu__header" )
                    .childAt( 0 )
                    .text()
            ).toBe( "Show, Hide Columns" );

            expect( wrapper.state().selectAll ).toBeTruthy();

            expect(
                wrapper
                    .find( ".customize-menu__footer" )
                    .childAt( 0 )
                    .text()
            ).toBe( "Row hight:" );
            expect(
                wrapper
                    .find( ".customize-menu__footer" )
                    .find( "ul" )
                    .childAt( 0 )
                    .find( "button" )
                    .childAt( 0 )
                    .type()
            ).toBe( "svg" );
            expect(
                wrapper
                    .find( ".customize-menu__footer" )
                    .find( "ul" )
                    .childAt( 1 )
                    .find( "button" )
                    .childAt( 0 )
                    .type()
            ).toBe( "svg" );
            expect(
                wrapper
                    .find( ".customize-menu__footer" )
                    .find( "ul" )
                    .childAt( 2 )
                    .find( "button" )
                    .childAt( 0 )
                    .type()
            ).toBe( "svg" );
        } );
    } );
} );
