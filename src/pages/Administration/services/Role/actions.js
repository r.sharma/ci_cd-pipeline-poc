import { createAction } from "redux-actions";

export const itemViewDetailsRolesSaveAction = createAction(
    "itemViewDetailsRolesSaveAction"
);
export const getRolesListAction = createAction( "getRolesListAction" );
export const saveRolesListAction = createAction( "saveRolesListAction" );
export const setLoadingRolesList = createAction( "setLoadingRolesList" );
