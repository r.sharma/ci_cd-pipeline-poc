import React from "react";
import { StatusIcons } from "components";
import { tikSvg } from "components";
import "./ItemImageViewSmall.scss";

export const CardItemImageViewSmall = ( {
    img = null,
    imgCardAlt = "",
    addClassName = "",
    status = "",
    chooseCardToSelect = () => {},
    element = { id: null },
    selectedCardsList = []
} ) => {
    return (
        <div className={`card-item-image-view-small noselect ${addClassName}`}>
            <label>
                <input
                    type="checkbox"
                    onChange={event =>
                        chooseCardToSelect( {
                            value: event.target,
                            id: element.id
                        } )
                    }
                    checked={selectedCardsList.indexOf( element.id ) !== -1}
                />
                <div className="card-item-body">
                    <div className="img-box">
                        {img ? <img src={img} alt={imgCardAlt} /> : null}
                    </div>
                    <div className="svg-box">{tikSvg()}</div>
                </div>
                <div className="footer-card">
                    <StatusIcons status={status} tooltip={true} />
                </div>
            </label>
        </div>
    );
};
