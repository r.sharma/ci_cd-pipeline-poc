import React, { Component } from "react";
import {
    SideBar,
    Header,
    PageRoute,
    ViewDetails
} from "pages/Administration/components";
import { TabBar, NoMatch } from "components";
import { Route, Switch, Redirect, NavLink } from "react-router-dom";

export class Dashboard extends Component {
    constructor( props ) {
        super( props );
        const { match } = this.props;
        this.baseUrl =
            match.url[match.url.length - 1] === "/"
                ? match.url.slice( 0, -1 )
                : match.url;
    }

    render() {
        const { sideBarIsOpen, activeModuleState } = this.props;
        let pageClassName = "page bg-primary-transparent";
        if ( sideBarIsOpen ) pageClassName += " with-open-side-bar";

        if ( !activeModuleState ) {
            return (
                <Switch>
                    <Route
                        exact
                        path={this.baseUrl}
                        render={() => <Redirect to={`${this.baseUrl}/users`} />}
                    />
                    <Route component={NoMatch} />
                </Switch>
            );
        }

        return (
            <div>
                <SideBar />

                <div className={pageClassName}>
                    <div className="page-container scroll-m">
                        <Header title="Administration">
                            <TabBar>
                                <NavLink to={`${this.baseUrl}/users`} activeClassName="active">
                                    Users
                                </NavLink>
                                {/*
                                    <NavLink to={`${this.baseUrl}/roles`} activeClassName="active">
                                        Roles
                                    </NavLink>
                                */}
                            </TabBar>
                        </Header>

                        <ViewDetails />

                        <Switch>
                            <Route path={`${this.baseUrl}/users`} component={PageRoute} />
                            <Route path={`${this.baseUrl}/roles`} component={PageRoute} />
                            <Route component={NoMatch} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}
