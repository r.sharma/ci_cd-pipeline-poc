import { call, put, select, takeLatest } from "redux-saga/effects";

import * as PostMarkAPI from "./api";
import {
    isLoadingLabelListAction,
    setPostLabelListAction,
    getLabelListAction, setProfileMarkListAction
} from "./actions";
import { activeBrandSelector } from "services/Brand/selectors";
import { labelListSelector } from "./selectors";
import { activeModuleSelector } from "services/UserView/selectors";

// @TODO: Can we make it more error proof and remove this array?
// Other modules don't need to pull this, will error out.
const modulesWithLabelSupport = [
    "social-protect",
    "market-protect",
    "social-protect-add"
];
//WORKERS
export function* labelListWorker( { payload } ) {
    const activeModule = yield select( activeModuleSelector );

    if ( !modulesWithLabelSupport.includes( activeModule ) ) return;

    yield put( isLoadingLabelListAction( true ) );
    //Get brands list from API
    const activeBrand = yield select( activeBrandSelector );
    const labelListState = yield select( labelListSelector );
    //if not exist data we should make a call and save a data
    if( !labelListState ){
        const response = yield call( PostMarkAPI.getMarkList, { activeBrand: activeBrand, markType: payload.slice( 0, -1 ) } );
        if ( response ) {
            response.unshift( { id: "0", code: "UNHANDLED", name: "Unhandled" } );
            if( payload === "posts" ) yield put( setPostLabelListAction( response ) );
            if( payload === "profiles" ) yield put( setProfileMarkListAction( response ) );
        }
    }
    yield put( isLoadingLabelListAction( false ) );
}

// WATCHERS
export function* watchingLabelList() {
    yield takeLatest( getLabelListAction, labelListWorker );
}