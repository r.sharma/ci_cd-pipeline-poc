import React, { PureComponent, Fragment } from "react";
import { arrow, deleteSvgSmall } from "components/Icons/Svg";
import {
    StatusDropdown,
    Button,
    ChangeLabel,
    Loader,
    ButtonHoverSVG
} from "components";

import { GridTableViewTopBar } from "pages/SocialProtect/components";

import "./OptionsTopBar.scss";

export class OptionsTopBar extends PureComponent {
    state = {
        open: false,
        sameLabelId: false,
        optionValue: null,
        dataCards: [],
        selectInfringementType: false,
        startEnforcement: false
    };

    static getDerivedStateFromProps( nextProps, prevState ) {
        if (
            nextProps.selectedCardsListState &&
            nextProps.selectedCardsListState.length &&
            nextProps.cardListOnPageState &&
            nextProps.cardListOnPageState.length
        ) {
            const newData = [];
            nextProps.cardListOnPageState.forEach( el => {
                nextProps.selectedCardsListState.forEach( e => {
                    if ( e === el.id ) newData.push( el );
                } );
            } );
            let sameLabelId = false;
            if ( newData && newData.length ) {
                sameLabelId = !!newData.reduce( ( a, b ) => {
                    return ( ( a.mark && a.mark.id ) || NaN ) ===
            ( ( b.mark && b.mark.id ) || NaN )
                        ? a
                        : NaN;
                } );
            }
            return {
                sameLabelId: sameLabelId
                    ? ( newData[0].mark && newData[0].mark.id ) || true
                    : false,
                dataCards: newData,
                openChange: nextProps.selectedPostMarkIdState
            };
        }
        return {
            open: false,
            openChange: false,
            dataCards: [],
            selectInfringementType: false
        };
    }

    handleOpenCardList = () => {
        this.setState( prevState => ( {
            open: !prevState.open
        } ) );
    };

    setCurrentFunction = e => {
        const { selectedPostMarkIdAction } = this.props;
        if ( e !== 1 ) this.setState( { startEnforcement: true } );
        this.setState( { optionValue: null }, () => selectedPostMarkIdAction( e ) );
    };

    setCurrentInfringementType = e => {
        //Save ID InfringementType in store
        const {
            selectInfringementTypeInTopBarAction,
            selectEnforcementStatusInTopBarAction
        } = this.props;
        const { optionValue } = this.state;
        if ( optionValue === "type" ) {
            selectInfringementTypeInTopBarAction( e );
        } else if ( optionValue === "status" ) {
            selectEnforcementStatusInTopBarAction( e );
        }
    };

    setOptions = optionValue => {
        this.setState( {
            optionValue: optionValue,
            selectInfringementType: true
        } );
    };

    closeAndClear = () => {
        this.setCurrentFunction( null );
        this.setCurrentInfringementType( null );
        this.setState( {
            selectInfringementType: false,
            startEnforcement: false
        } );
    };

    closeTab = () => {
        this.setCurrentInfringementType( null );
        this.setState( {
            selectInfringementType: false
        } );
    };

    clickApplyInfringement = () => {
        const { selectedPostMarkIdAction } = this.props;
        this.setState( { startEnforcement: true } );
        selectedPostMarkIdAction( 1 );
    };

    startEnforcement = () => {
        const { startEnforcementAction } = this.props;
        const { optionValue } = this.state;
        return new Promise( ( resolve, reject ) => {
            startEnforcementAction( {
                optionValue: optionValue,
                resolve,
                reject
            } );
        } ).then( () => {
            this.setState( { startEnforcement: false } );
        } );
    };

    render() {
        const {
            open,
            dataCards,
            selectInfringementType,
            startEnforcement,
            sameLabelId,
            optionValue
        } = this.state;
        const {
            selectedCardsListState = [],
            chooseAllCardAction = () => {},
            chooseCardToSelectAction = () => {},
            deleteAllSelectedCardsAction = () => {},
            labelListState,
            selectedPostMarkIdState,
            startEnforcementLoaderState,
            infringementTypeListState,
            infringementTypeIdState,
            selectedEnforcementStatusIdState,
            statusListState,
            activeModuleState
        } = this.props;
        let changeListValues = [];
        let valueList = null;
        if ( optionValue === "type" ) {
            changeListValues = infringementTypeListState;
            valueList = infringementTypeIdState;
        } else if ( optionValue === "status" ) {
            changeListValues = statusListState;
            valueList = selectedEnforcementStatusIdState;
        }
        let wrapperClassName = "options-top-bar noselect";
        if ( selectedCardsListState.length ) wrapperClassName += " show";
        return (
            <Fragment>
                {open ? (
                    <div
                        role="button"
                        tabIndex={0}
                        className="bg-list-top-bar"
                        onClick={this.handleOpenCardList}
                        onKeyPress={e => e.keyCode === 13 ? this.handleOpenCardList() : false}
                    />
                ) : null}
                <div className={wrapperClassName}>
                    {startEnforcement ? (
                        <>
                        <div className="options-top-bar__list scroll-m">
                            <div>
                                <GridTableViewTopBar
                                    data={dataCards}
                                    chooseCardToSelect={chooseCardToSelectAction}
                                    activeModuleState={activeModuleState}
                                />
                            </div>
                        </div>
                        <div className="options-top-bar__options">
                            {startEnforcementLoaderState ? (
                                <Loader style={{ margin: "19px auto 19px" }} />
                            ) : (
                                <div className="select_block btn-row">
                                    <div>
                                        <Button
                                            text="Start Enforcement"
                                            handleClick={this.startEnforcement}
                                        />
                                        <Button
                                            text="Cancel"
                                            color="secondary"
                                            handleClick={this.closeAndClear}
                                        />
                                    </div>
                                    <div>
                                Are you sure you want to enforce the following this posts?
                                    </div>
                                </div>
                            )}
                        </div>
                        </>
                    ) : (
                        <div className="options-top-bar__options">
                            <div className="select_block">
                                <button
                                    type="button"
                                    className={open ? "btn-bar__list show" : "btn-bar__list"}
                                    onClick={this.handleOpenCardList}
                                >
                                    {arrow()}
                                </button>

                                <StatusDropdown
                                    wrapperClassName="status-dropdown"
                                    data={labelListState}
                                    defaultValue={
                                        sameLabelId && typeof sameLabelId === "number"
                                            ? sameLabelId
                                            : selectedPostMarkIdState || ""
                                    }
                                    handleChange={this.setCurrentFunction}
                                />

                                {(
                                    sameLabelId && sameLabelId === 1 )
                                    ||
                                    ( selectedPostMarkIdState && selectedPostMarkIdState === 1 ) ? (
                                        <div className="set-infringing-status-type">
                                            <ButtonHoverSVG onClick={() => this.setOptions( "type" )}>
                                                Set Type
                                            </ButtonHoverSVG>
                                            <ButtonHoverSVG onClick={() => this.setOptions( "status" )}>
                                                Set Status
                                            </ButtonHoverSVG>
                                        </div>
                                    ) : null}

                                {selectInfringementType ? (
                                    <ChangeLabel
                                        cssInner="top-bar"
                                        open={selectInfringementType}
                                        title={optionValue}
                                        defaultValue={valueList}
                                        data={changeListValues}
                                        handleChange={this.setCurrentInfringementType}
                                    >
                                        <Button
                                            text="Apply"
                                            handleClick={this.clickApplyInfringement}
                                        />
                                        <Button
                                            text="Cancel"
                                            color="secondary"
                                            handleClick={this.closeTab}
                                        />
                                    </ChangeLabel>
                                ) : null}

                                <div className="select-box">
                                    <span
                                        role="button"
                                        tabIndex={0}
                                        onClick={this.handleOpenCardList}
                                        onKeyPress={e => e.keyCode === 13 ? this.handleOpenCardList() : false}
                                    >
                                        {selectedCardsListState.length} selected
                                    </span>
                                    <span className="border" />
                                    <button
                                        type="button"
                                        className="select-all"
                                        onClick={() => chooseAllCardAction( false )}
                                    >
                                        Deselect All
                                    </button>
                                    <button
                                        type="button"
                                        className="select-all"
                                        onClick={() => chooseAllCardAction( true )}
                                    >
                                        Select All
                                    </button>
                                </div>
                            </div>
                            <div className="config_block">
                                <ul>
                                    {/*<li>*/}
                                    {/*<button type="button">{downloadSmall('#546369')} Export</button>*/}
                                    {/*</li>*/}
                                    {/*<li className="border" />*/}
                                    {/*<li>*/}
                                    {/*<button type="button">{editSvgSmall('#546369')} Edit</button>*/}
                                    {/*</li>*/}
                                    <li>
                                        <button
                                            type="button"
                                            onClick={deleteAllSelectedCardsAction}
                                        >
                                            {deleteSvgSmall( "#546369" )} Delete
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    )}
                </div>
            </Fragment>
        );
    }
}
