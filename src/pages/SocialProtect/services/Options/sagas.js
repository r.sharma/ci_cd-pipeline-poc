
import { takeLatest, put, call, select } from "redux-saga/effects";
import { urlUtil } from "utils";

//API
import * as CardAPI from "pages/SocialProtect/services/Card/api";

// Actions
import {
    filterValuesInObjectSaveAction,
    changeSearchWordTitleAction,
    changeSearchWordProfileAction,
    searchByFilterInTimeAction,
    searchByFilterValueAction,
    saveSearchWordTitlePostAction,
    saveSearchWordTitleProfileAction,
    saveSearchWordProfileNameInPostAction,
    saveSearchWordProfileNameInProfileAction,
    saveFilterValueAction
} from "pages/SocialProtect/services/Options/actions";
import {
    saveCardListAction,
    loadCardListAction
} from "pages/SocialProtect/services/Card/actions";
import { saveProfileListAction } from "pages/SocialProtect/services/Profile/actions";

// Selectors
import { activeModuleStateSelector } from  "services/UserView/selectors";
import { activeBrandSelector } from  "services/Brand/selectors";
import {
    activeFilterMainObjectSelector,
    activeFilterValueSelector
}from "pages/SocialProtect/services/Options/selectors";

//Utils
const { modifyURLByFiltersWorker }  = urlUtil;

// Workers
function* changeSearchWordTitleWorker( { payload } ) {
    const activeModuleState = yield select( activeModuleStateSelector );
    if ( activeModuleState === "posts" ) {
        yield put( saveSearchWordTitlePostAction( payload ) );
    } else if ( activeModuleState === "profiles" ) {
        yield put( saveSearchWordTitleProfileAction( payload ) );
    }
}

function* changeSearchWordProfileWorker( { payload } ) {
    const activeModuleState = yield select( activeModuleStateSelector );
    if ( activeModuleState === "posts" ) {
        yield put( saveSearchWordProfileNameInPostAction( payload ) );
    } else if ( activeModuleState === "profiles" ) {
        yield put( saveSearchWordProfileNameInProfileAction( payload ) );
    }
}

export function* filterValuesInObjectSaveWorker( { payload } ) {
    if ( !payload ) {
        console.warn( "Called filterValuesInObjectSaveWorker without payload" );
        return;
    }

    const { moduleState } = payload;
    //Add filter value in store without api call
    const activeModuleState = yield select( activeModuleStateSelector );
    const activeFilterValues = moduleState
        ? yield select( activeFilterMainObjectSelector )
        : yield select( activeFilterValueSelector );
    let newFilterData = { ...activeFilterValues };
    if ( moduleState ) newFilterData = { ...activeFilterValues[moduleState] };
    for ( let key in payload ) {
        if ( Object.prototype.hasOwnProperty.call( payload, key ) ) {
            if ( Object.prototype.hasOwnProperty.call( newFilterData, key ) ) newFilterData[key] = payload[key];
        }
    }
    yield put(
        saveFilterValueAction( { payload: newFilterData, activeModuleState: moduleState || activeModuleState } ),
    );
}

export function* searchByFilterInTimeWorker( { payload } ) {
    if ( !payload ) {
        console.warn( "Called searchByFilterInTimeWorker without payload" );
        return;
    }

    // Init save value and later API call by filters values
    yield call( filterValuesInObjectSaveWorker, { payload } );
    yield call( searchByFilterValueWorker );
}

function* searchByFilterValueWorker() {
    //select data
    const activeBrand = yield select( activeBrandSelector );
    const activeModuleState = yield select( activeModuleStateSelector );
    const activeFilterValues = yield select( activeFilterValueSelector );
    //Start loading
    yield put( loadCardListAction( true ) );
    let newObj = { ...activeFilterValues };
    newObj.brand = activeBrand;
    //select list if exists
    newObj.module = activeModuleState;
    //Modify URL
    yield call( modifyURLByFiltersWorker, newObj );

    //Make request
    const response = yield call( CardAPI.getCardsList, newObj );
    if ( activeModuleState === "posts" && response ) {
        yield put( saveCardListAction( response ) );
    } else if ( activeModuleState === "profiles" && response ) {
        yield put( saveProfileListAction( response ) );
    }
    //end loading
    yield put( loadCardListAction( false ) );
}

// Watchers
export function* changeSearchWordTitleWatcher() {
    yield takeLatest( changeSearchWordTitleAction, changeSearchWordTitleWorker );
}
export function* changeSearchWordProfileWatcher() {
    yield takeLatest( changeSearchWordProfileAction, changeSearchWordProfileWorker );
}
export function* filterValuesInObjectSaveWatcher() {
    yield takeLatest( filterValuesInObjectSaveAction, filterValuesInObjectSaveWorker );
}
export function* searchByFilterInTimeWatcher() {
    yield takeLatest( searchByFilterInTimeAction, searchByFilterInTimeWorker );
}
export function* searchByFilterWatcher() {
    yield takeLatest( searchByFilterValueAction, searchByFilterValueWorker );
}