import React from "react";
import { loadMore, arrowLeft, arrowRight, FooterWrapper } from "components";
import ReactPaginate from "react-paginate";

export const Footer = props => {
    const {
        cardListState = {},
        handlePageChange,
        handleLoadMore,
        getLoadMoreLoaderState
    } = props;
    if ( !cardListState || ( cardListState && !cardListState.totalCount ) )
        return null;
    return (
        <FooterWrapper
            left={cardListState.totalCount > cardListState.limit ? (
                <ReactPaginate
                    previousLabel={"<"}
                    previousClassName={"previous"}
                    nextClassName={"next"}
                    nextLabel={">"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    initialPage={cardListState.page - 1}
                    forcePage={cardListState.page - 1}
                    disableInitialCallback={true}
                    pageCount={Math.ceil(
                        cardListState.totalCount / cardListState.limit
                    )}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={4}
                    onPageChange={handlePageChange}
                    containerClassName={"pagination-list"}
                    activeClassName={"active"}
                />
            ) : null}
            center={
                cardListState.totalCount > cardListState.limit
                    ?
                    <div
                        role={"presentation"}
                        className={getLoadMoreLoaderState ? "load-more loading" : "load-more"}
                        onClick={getLoadMoreLoaderState ? null : handleLoadMore}
                    >
                        {loadMore()} See more
                    </div>
                    : null
            }
            right={(
                <>
                    {cardListState.totalCount > cardListState.limit
                        ? getCounter( cardListState )
                        : null}
                    {cardListState.totalCount > cardListState.limit ? (
                        <ul className="pagination-buttons">
                            <li>
                                <button
                                    disabled={cardListState.page - 1 === 0}
                                    onClick={e => {
                                        e.preventDefault();
                                        if ( cardListState.page - 1 === 0 ) return;
                                        handlePageChange( { selected: cardListState.page - 2 } );
                                    }}
                                >
                                    {arrowLeft()}
                                </button>
                            </li>
                            <li>
                                <button
                                    disabled={
                                        cardListState.page + 1 >
                                        Math.ceil( cardListState.totalCount / cardListState.limit )
                                    }
                                    onClick={e => {
                                        e.preventDefault();
                                        if (
                                            cardListState.page + 1 >
                                            Math.ceil( cardListState.totalCount / cardListState.limit )
                                        )
                                            return;
                                        handlePageChange( { selected: cardListState.page } );
                                    }}
                                >
                                    {arrowRight()}
                                </button>
                            </li>
                        </ul>
                    ) : null}
                </>
            )}
        />
    );
};


const getCounter = ( { limit, page, totalCount, results } ) => {
    const resultsLength = results.length;
    let startPoint = page === 1 ? page : page * limit + 1;
    let endPoint = page === 1 ? page * limit : page * limit + limit;
    if ( totalCount <= resultsLength ) return null;
    return (
        <div className="pagination-counter">
            {resultsLength !== 0 ? (
                <span>
                    {//if has items added with load more
                        limit === resultsLength
                            ? startPoint
                            : endPoint - resultsLength === limit
                                ? endPoint - resultsLength + 1 - limit
                                : endPoint - resultsLength + 1}
                    -{endPoint}
                </span>
            ) : null}
            of {totalCount}
        </div>
    );
};
