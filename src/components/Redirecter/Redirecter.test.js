import React from "react";
import { shallow } from "enzyme";
import { Redirecter } from "./Redirecter";

describe( "Redirecter", () => {
    const testUrl = "http://www.jahetisaltijdvoorbier.nl";

    beforeEach( () => {
        jest.resetModules(); // Clears cache

        global.window = Object.create( window );
        Object.defineProperty( window, "location", {
            value: {
                href: "http://www.ishetaltijdvoorbier.nl",
                pathname: "/",
                hostname: "www.ishetaltijdvoorbier.nl"
            }
        } );
    } );

    afterEach( () => {
        delete window.location;
    } );

    it( "Renders 1 <Redirecter /> component", () => {
        const component = shallow( <Redirecter url={testUrl} /> );

        expect( component ).toHaveLength( 1 );
    } );

    it( "Redirects", () => {
        shallow( <Redirecter url={testUrl} /> );

        expect(  window.location.href ).toEqual( testUrl );
    } );
} );
