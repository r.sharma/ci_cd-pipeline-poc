import React from "react";
import { shallow } from "enzyme";
import { ToggleView } from "./ToggleView";

describe( "ToggleView", () => {
    it( "renders 1 <ToggleView /> component", () => {
        const component = shallow( <ToggleView /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "mount wrapper label", () => {
        const props = {
            label: "Customize:",
            active: "web"
        };
        const component = shallow( <ToggleView {...props} /> );

        expect( component.props().className ).toEqual( "toggle-view-default" );
        expect( component.state().active ).toEqual( props.active );

        expect( component.find( ".list-view" ).children().length ).toBe( 4 );
    } );
} );
