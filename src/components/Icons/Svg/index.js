import React from "react";

export const logoSmall = () => (
  <svg width={32} height={32}>
    <g fill="none" fillRule="evenodd">
      <path
        fill="#F56B6B"
        d="M12.978 15.62c0 1.482 1.226 2.658 2.706 2.658a2.67 2.67 0 0 0 2.655-2.659c0-1.482-1.225-2.709-2.655-2.709a2.725 2.725 0 0 0-2.706 2.71m-6.381 0c0 1.482 1.225 2.658 2.705 2.658a2.671 2.671 0 0 0 2.655-2.659c0-1.482-1.225-2.709-2.655-2.709a2.725 2.725 0 0 0-2.705 2.71m12.763 0c0 1.482 1.225 2.658 2.706 2.658a2.67 2.67 0 0 0 2.655-2.659c0-1.482-1.226-2.709-2.655-2.709a2.725 2.725 0 0 0-2.706 2.71m-19.196 0a2.67 2.67 0 0 0 2.655 2.658c1.48 0 2.706-1.176 2.706-2.659a2.725 2.725 0 0 0-2.706-2.709c-1.43 0-2.655 1.227-2.655 2.71m25.526 0a2.67 2.67 0 0 0 2.655 2.658c1.481 0 2.706-1.176 2.706-2.659a2.725 2.725 0 0 0-2.706-2.709c-1.429 0-2.654 1.227-2.654 2.71m-3.727 9.047c1.48 0 2.706-1.175 2.706-2.658a2.726 2.726 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m-6.382 6.39c1.48 0 2.706-1.175 2.706-2.658a2.725 2.725 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m0-25.559c1.48 0 2.706-1.176 2.706-2.659A2.725 2.725 0 0 0 15.582.13c-1.43 0-2.655 1.227-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m6.382 6.389c1.48 0 2.706-1.175 2.706-2.658a2.726 2.726 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658"
      />
      <path
        d="M12.978 22.01c0 1.482 1.226 2.657 2.706 2.657a2.67 2.67 0 0 0 2.655-2.658c0-1.482-1.225-2.71-2.655-2.71a2.726 2.726 0 0 0-2.706 2.71m0-12.779c0 1.482 1.226 2.657 2.706 2.657A2.67 2.67 0 0 0 18.34 9.23c0-1.482-1.225-2.71-2.655-2.71a2.726 2.726 0 0 0-2.706 2.71M9.2 24.667c1.481 0 2.706-1.175 2.706-2.658A2.726 2.726 0 0 0 9.2 19.3c-1.429 0-2.654 1.228-2.654 2.71A2.67 2.67 0 0 0 9.2 24.667m0-12.78c1.481 0 2.706-1.175 2.706-2.658A2.726 2.726 0 0 0 9.2 6.52c-1.429 0-2.654 1.228-2.654 2.71A2.67 2.67 0 0 0 9.2 11.887"
        fill="#445BF4"
        opacity={0.5}
      />
    </g>
  </svg>
);

export const web = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__11"
        d="M9 7v6h3a1 1 0 0 0 1-1V7H9zm4-3a1 1 0 0 0-1-1l-7.99.001a1 1 0 0 0-1 1L3.009 5H13V4zM3 12a1 1 0 0 0 1 1.001h3V7H3.007L3 12zm9-11a3 3 0 0 1 3 3v8a3 3 0 0 1-3 3l-8 .001A3 3 0 0 1 1 12l.01-8a3 3 0 0 1 3-2.998L12 1z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__22" fill="#fff">
        <use xlinkHref="#prefix__11" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__11" />
      <g fill={fill} mask="url(#prefix__22)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const block = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__qq"
        d="M6 13h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 2v3h3v-3H6zm9-2h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 2v3h3v-3h-3zM6 4h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 2v3h3V6H6zm9-2h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-3a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 2v3h3V6h-3z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__2" fill="#fff">
        <use xlinkHref="#prefix__qq" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__qq" />
      <g fill={fill} mask="url(#prefix__2)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const row = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__rr"
        d="M7 7a1 1 0 1 1 0-2h13a1 1 0 0 1 0 2H7zm0 6a1 1 0 0 1 0-2h13a1 1 0 0 1 0 2H7zm0 6a1 1 0 0 1 0-2h13a1 1 0 0 1 0 2H7zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0 6a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0 6a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__1" fill="#fff">
        <use xlinkHref="#prefix__rr" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__rr" />
      <g fill={fill} mask="url(#prefix__1)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const mixedSvg = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__asdf2jiy"
        d="M6 13h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 2v3h3v-3H6zm9-11h3a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2h-3a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 2v12h3V6h-3zM6 4h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 2v3h3V6H6z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bbvn584" fill="#fff">
        <use xlinkHref="#prefix__asdf2jiy" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__asdf2jiy" />
      <g fill={fill} mask="url(#prefix__bbvn584)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const menuHeader = () => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__asdf7jhg"
        d="M6 4a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm6-12a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm6-12a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4zm0 6a2 2 0 1 1 0 4 2 2 0 0 1 0-4z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bdfg7jk" fill="#fff">
        <use xlinkHref="#prefix__asdf7jhg" />
      </mask>
      <use fill="#FF9393" xlinkHref="#prefix__asdf7jhg" />
      <g fill="#546369" mask="url(#prefix__bdfg7jk)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const arrow = () => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__a56dfhsdf"
        d="M2.116.704L6 4.58 9.884.704a.997.997 0 1 1 1.41 1.41l-4.586 4.59a1 1 0 0 1-1.414 0L.705 2.115a.998.998 0 0 1 1.41-1.41z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(6 8)">
        <mask id="prefix__bgfhfg6545" fill="#fff">
          <use xlinkHref="#prefix__a56dfhsdf" />
        </mask>
        <use fill="#000" fillRule="nonzero" xlinkHref="#prefix__a56dfhsdf" />
        <g fill="#546369" mask="url(#prefix__bgfhfg6545)">
          <path d="M-6-8h24v24H-6z" />
        </g>
      </g>
    </g>
  </svg>
);

export const addSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__aghjg85df2"
        d="M9 7h4a1 1 0 0 1 0 2H9v4a1 1 0 0 1-2 0V9H3a1 1 0 1 1 0-2h4V3a1 1 0 1 1 2 0v4z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bdfg94nd4" fill="#fff">
        <use xlinkHref="#prefix__aghjg85df2" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aghjg85df2" />
      <g fill={fill} mask="url(#prefix__bdfg94nd4)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const searchSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__akl123zxcg"
        d="M12.463 11.049l2.244 2.244a1 1 0 0 1-1.414 1.414l-2.244-2.244a6.333 6.333 0 1 1 1.414-1.414zm-5.13.618a4.333 4.333 0 1 0 0-8.667 4.333 4.333 0 0 0 0 8.667z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__b456hg54s" fill="#fff">
        <use xlinkHref="#prefix__akl123zxcg" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__akl123zxcg" />
      <g fill={fill} mask="url(#prefix__b456hg54s)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const uploadSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__auploadSmall"
        d="M7 10.414V13a1 1 0 0 0 2 0v-2.586l.293.293a1 1 0 1 0 1.414-1.414l-2-2a1 1 0 0 0-1.414 0l-2 2a1 1 0 0 0 1.414 1.414L7 10.414zm7.797-2.69c.505 1.557.061 3.199-1.254 4.102a.993.993 0 0 1-1.384-.262 1.003 1.003 0 0 1 .26-1.39c.54-.37.73-1.073.485-1.83-.223-.687-.886-1.16-1.643-1.16h-.684a.996.996 0 0 1-.962-.744C9.274 5.142 8.14 4.177 6.758 4.022c-1.385-.156-2.721.536-3.359 1.73-.652 1.222-.516 2.605.363 3.575.37.408.34 1.04-.066 1.412a.992.992 0 0 1-1.406-.066C.829 9.062.602 6.76 1.645 4.806 2.67 2.886 4.79 1.788 6.98 2.034c1.923.216 3.558 1.434 4.302 3.15 1.603.009 3.024 1.03 3.515 2.54z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__buploadSmall" fill="#fff">
        <use xlinkHref="#prefix__auploadSmall" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__auploadSmall" />
      <g fill={fill} mask="url(#prefix__buploadSmall)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const downloadSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__adownloadSmall"
        d="M7 10.586V8a1 1 0 1 1 2 0v2.586l.293-.293a1 1 0 0 1 1.414 1.414l-2 2a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414l.293.293zm7.797-2.863c.505 1.558.061 3.2-1.254 4.103a.993.993 0 0 1-1.384-.262 1.003 1.003 0 0 1 .26-1.39c.54-.37.73-1.073.485-1.83-.223-.687-.886-1.16-1.643-1.16h-.684a.996.996 0 0 1-.962-.744C9.274 5.142 8.14 4.177 6.758 4.022c-1.385-.156-2.721.536-3.359 1.73-.652 1.222-.516 2.605.363 3.575.37.408.34 1.04-.066 1.412a.992.992 0 0 1-1.406-.066C.829 9.062.602 6.76 1.645 4.806 2.67 2.886 4.79 1.788 6.98 2.034c1.923.216 3.558 1.434 4.302 3.15 1.603.009 3.024 1.03 3.515 2.54z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bdownloadSmall" fill="#fff">
        <use xlinkHref="#prefix__adownloadSmall" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__adownloadSmall" />
      <g fill={fill} mask="url(#prefix__bdownloadSmall)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const settingsSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__asettingsSmall"
        d="M13.569 11.434a2.09 2.09 0 0 1-3.438 1.6 2.091 2.091 0 0 1-4.174.006 2.09 2.09 0 0 1-2.991-2.91 2.091 2.091 0 0 1-.006-4.173 2.09 2.09 0 0 1 2.951-2.955 2.091 2.091 0 0 1 4.173-.077 2.09 2.09 0 0 1 2.914 2.986 2.091 2.091 0 0 1 .077 4.173c.318.376.494.854.494 1.35zM12.86 8.09h.05a.09.09 0 0 0 0-.182h-.098a1.9 1.9 0 0 1-1.738-1.151 1 1 0 0 1-.065-.214 1.905 1.905 0 0 1 .457-1.924l.033-.033a.09.09 0 0 0-.001-.13.09.09 0 0 0-.13.001l-.04.04a1.896 1.896 0 0 1-2.086.385A1.901 1.901 0 0 1 8.091 3.14v-.05a.09.09 0 0 0-.182 0v.098a1.9 1.9 0 0 1-1.151 1.738 1 1 0 0 1-.214.065 1.905 1.905 0 0 1-1.924-.457l-.033-.033a.09.09 0 0 0-.13.001.09.09 0 0 0 .001.13l.04.04c.544.555.694 1.385.404 2.037A1.907 1.907 0 0 1 3.14 7.953h-.05a.09.09 0 0 0 0 .182h.098c.757.003 1.44.455 1.734 1.141.314.71.163 1.54-.388 2.104l-.033.033a.09.09 0 0 0 .001.13.09.09 0 0 0 .13-.001l.04-.04c.555-.544 1.385-.694 2.037-.404a1.91 1.91 0 0 1 1.244 1.762v.05a.09.09 0 0 0 .182 0v-.098a1.896 1.896 0 0 1 1.141-1.734 1.904 1.904 0 0 1 2.104.388l.033.033a.09.09 0 0 0 .13-.001.09.09 0 0 0-.001-.13l-.04-.04a1.9 1.9 0 0 1-.383-2.09c.3-.694.982-1.144 1.741-1.147zM8 10a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bsettingsSmall" fill="#fff">
        <use xlinkHref="#prefix__asettingsSmall" />
      </mask>
      <use fill={fill} xlinkHref="#prefix__asettingsSmall" />
      <g fill={fill} mask="url(#prefix__bsettingsSmall)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const loadMore = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aloadMore"
        d="M2 16.312V20a1 1 0 0 1-2 0v-6a1 1 0 0 1 1-1h6a1 1 0 0 1 0 2H3.524l2.823 2.653a8 8 0 0 0 13.2-2.987 1 1 0 0 1 1.886.668 10 10 0 0 1-7.22 6.417 9.985 9.985 0 0 1-9.258-2.662L2 16.312zm20-8.624V4a1 1 0 0 1 2 0v6a1 1 0 0 1-1 1h-6a1 1 0 0 1 0-2h3.476l-2.823-2.653a8 8 0 0 0-13.2 2.987 1 1 0 1 1-1.886-.668 10 10 0 0 1 7.22-6.417 9.985 9.985 0 0 1 9.258 2.662L22 7.688z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bloadMore" fill="#fff">
        <use xlinkHref="#prefix__aloadMore" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aloadMore" />
      <g fill={fill} mask="url(#prefix__bloadMore)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const arrowRight = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aarrowRight"
        d="M16.586 13H5a1 1 0 0 1 0-2h11.586l-5.293-5.293a1 1 0 1 1 1.414-1.414l7 7a1 1 0 0 1 0 1.414l-7 7a1 1 0 0 1-1.414-1.414L16.586 13z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__barrowRight" fill="#fff">
        <use xlinkHref="#prefix__aarrowRight" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aarrowRight" />
      <g fill={fill} mask="url(#prefix__barrowRight)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const arrowLeft = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aarrowLeft"
        d="M7.414 13l5.293 5.293a1 1 0 0 1-1.414 1.414l-7-7a1 1 0 0 1 0-1.414l7-7a1 1 0 0 1 1.414 1.414L7.414 11H19a1 1 0 0 1 0 2H7.414z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__barrowLeft" fill="#fff">
        <use xlinkHref="#prefix__aarrowLeft" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aarrowLeft" />
      <g fill={fill} mask="url(#prefix__barrowLeft)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const userD = (fill = "#979797") => (
  <svg width={16} height={16}>
    <path
      fill={fill}
      fillRule="evenodd"
      d="M9.429 5.361a1 1 0 0 1 1.971.24V8.6a.8.8 0 1 0 1.6 0V8a5 5 0 1 0-1.96 3.97 1 1 0 0 1 1.216 1.588A7 7 0 1 1 15 8v.6a2.8 2.8 0 0 1-5.044 1.675 3 3 0 1 1-.527-4.913zM8 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"
    />
  </svg>
);

export const starBtn = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__adfg234lg"
        d="M5.493 4.703L7.11 1.544a1 1 0 0 1 1.78 0l1.617 3.159 3.633.511a1 1 0 0 1 .544 1.72L12.08 9.379l.614 3.446a1 1 0 0 1-1.437 1.067L8 12.242l-3.256 1.65a1 1 0 0 1-1.437-1.067l.614-3.446-2.605-2.445a1 1 0 0 1 .544-1.72l3.633-.511zm.586 3.96l-.451 2.539L8 9.999l2.372 1.203-.451-2.54 1.873-1.758-2.593-.366L8 4.194 6.8 6.538l-2.594.366 1.873 1.759z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bcvbkl61234" fill="#fff">
        <use xlinkHref="#prefix__adfg234lg" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__adfg234lg" />
      <g fill={fill} mask="url(#prefix__bcvbkl61234)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const logoBig = () => (
  <svg width={146} height={32}>
    <g fill="none" fillRule="evenodd">
      <path
        fill="#445BF4"
        d="M46.53 13.193C46.53 10.608 44.665 8 41.195 8H35v16h3.844v-5.746h2.35c3.47 0 5.337-2.475 5.337-5.06zm-3.843 0c0 .818-.615 1.592-1.669 1.592h-2.174v-3.183h2.174c1.054 0 1.67.774 1.67 1.591zM61.707 16c0-2.365.066-4.575-1.58-6.232C58.983 8.618 57.666 8 55.754 8c-1.91 0-3.228.619-4.37 1.768-1.648 1.657-1.582 3.867-1.582 6.232s-.066 4.575 1.582 6.232c1.142 1.15 2.46 1.768 4.37 1.768 1.911 0 3.23-.619 4.371-1.768 1.647-1.657 1.582-3.867 1.582-6.232zm-3.843 0c0 2.873-.22 3.381-.571 3.845-.286.376-.835.707-1.538.707-.702 0-1.252-.33-1.537-.707-.351-.464-.571-.972-.571-3.845s.22-3.403.571-3.867c.285-.376.835-.685 1.537-.685.703 0 1.252.309 1.538.685.351.464.57.994.57 3.867zm8.039 8h3.843V8h-3.843v16zm20.755 0V8h-3.844v8.044L77.806 8h-3.36v16h3.844v-8.044L83.297 24h3.36zm15.308-12.42V8h-11.53v3.58h3.843V24h3.844V11.58h3.843zM116.374 24v-3.58h-6.787v-2.763h5.799V14.21h-5.799v-2.63h6.787V8h-10.63v16h10.63zM133 24l-3.382-6.696c1.23-.619 2.547-1.967 2.547-4.11 0-2.586-1.866-5.194-5.337-5.194h-6.193v16h3.843v-5.989h1.296L128.542 24H133zm-6.347-9.215h-2.175v-3.183h2.175c1.054 0 1.669.774 1.669 1.591 0 .818-.615 1.592-1.67 1.592z"
      />
      <path
        fill="#F56B6B"
        d="M12.978 15.62c0 1.482 1.226 2.658 2.706 2.658a2.67 2.67 0 0 0 2.655-2.659c0-1.482-1.225-2.709-2.655-2.709a2.725 2.725 0 0 0-2.706 2.71m-6.381 0c0 1.482 1.225 2.658 2.705 2.658a2.671 2.671 0 0 0 2.655-2.659c0-1.482-1.225-2.709-2.655-2.709a2.725 2.725 0 0 0-2.705 2.71m12.763 0c0 1.482 1.225 2.658 2.706 2.658a2.67 2.67 0 0 0 2.655-2.659c0-1.482-1.226-2.709-2.655-2.709a2.725 2.725 0 0 0-2.706 2.71m-19.196 0a2.67 2.67 0 0 0 2.655 2.658c1.48 0 2.706-1.176 2.706-2.659a2.725 2.725 0 0 0-2.706-2.709c-1.43 0-2.655 1.227-2.655 2.71m25.526 0a2.67 2.67 0 0 0 2.655 2.658c1.481 0 2.706-1.176 2.706-2.659a2.725 2.725 0 0 0-2.706-2.709c-1.429 0-2.654 1.227-2.654 2.71m-3.727 9.047c1.48 0 2.706-1.175 2.706-2.658a2.726 2.726 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m-6.382 6.39c1.48 0 2.706-1.175 2.706-2.658a2.725 2.725 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m0-25.559c1.48 0 2.706-1.176 2.706-2.659A2.725 2.725 0 0 0 15.582.13c-1.43 0-2.655 1.227-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m6.382 6.389c1.48 0 2.706-1.175 2.706-2.658a2.726 2.726 0 0 0-2.706-2.71c-1.43 0-2.655 1.228-2.655 2.71a2.67 2.67 0 0 0 2.655 2.658m117.899-2.183c0-.878-.507-1.294-.904-1.48.332-.156.812-.628.812-1.358 0-1.127-.748-1.848-2.03-1.848h-2.64v6.58h2.75c1.172 0 2.012-.665 2.012-1.894zm-1.375-2.782c0 .453-.305.757-.85.757h-1.255V6.164h1.256c.544 0 .849.305.849.758zm.092 2.717c0 .444-.277.813-.858.813h-1.339V8.835h1.339c.581 0 .858.36.858.804zm7.403-2.57c0-1.118-.812-2.051-2.17-2.051h-2.547v6.58h1.284V9.122h1.263c1.358 0 2.17-.934 2.17-2.052zm-1.283 0c0 .537-.369.897-.95.897h-1.2V6.164h1.2c.581 0 .95.37.95.906z"
      />
      <path
        d="M12.978 22.01c0 1.482 1.226 2.657 2.706 2.657a2.67 2.67 0 0 0 2.655-2.658c0-1.482-1.225-2.71-2.655-2.71a2.726 2.726 0 0 0-2.706 2.71m0-12.779c0 1.482 1.226 2.657 2.706 2.657A2.67 2.67 0 0 0 18.34 9.23c0-1.482-1.225-2.71-2.655-2.71a2.726 2.726 0 0 0-2.706 2.71M9.2 24.667c1.481 0 2.706-1.175 2.706-2.658A2.726 2.726 0 0 0 9.2 19.3c-1.429 0-2.654 1.228-2.654 2.71A2.67 2.67 0 0 0 9.2 24.667m0-12.78c1.481 0 2.706-1.175 2.706-2.658A2.726 2.726 0 0 0 9.2 6.52c-1.429 0-2.654 1.228-2.654 2.71A2.67 2.67 0 0 0 9.2 11.887m28.642 17.92c0-.553-.315-.757-.47-.857a.953.953 0 0 0 .416-.779c0-.68-.422-1.138-1.212-1.138H35v3.934h1.656c.678 0 1.186-.409 1.186-1.16zm-.988-1.586c0 .155-.08.32-.337.32h-.582v-.64h.582c.257 0 .337.165.337.32zm.053 1.53c0 .166-.085.349-.342.349h-.63v-.691h.63c.257 0 .342.177.342.343zm5.198 1.216l-.823-1.64c.3-.156.62-.493.62-1.029 0-.646-.454-1.265-1.298-1.265h-1.507v3.934h.935v-1.464h.315l.673 1.464h1.085zm-1.138-2.669c0 .205-.15.398-.406.398h-.529v-.795h.53a.39.39 0 0 1 .405.397zm5.326 2.669l-1.383-3.934h-.722l-1.383 3.934h.977l.187-.586h1.17l.177.586h.977zm-1.4-1.392h-.662l.342-1.05.32 1.05zm5.364 1.392v-3.934h-.935v1.978l-1.218-1.978h-.817v3.934h.935v-1.978l1.218 1.978h.817zM54.467 29c0-.619.048-1.121-.396-1.58-.262-.27-.63-.387-1.026-.387h-1.41v3.934h1.41c.396 0 .764-.116 1.026-.387.444-.458.396-.961.396-1.58zm-.935 0c0 .669-.027.774-.102.884-.085.133-.214.221-.449.221h-.41v-2.21h.41c.235 0 .364.089.45.221.074.11.1.221.1.884zm6.714-.702c0-.646-.454-1.265-1.298-1.265h-1.506v3.934h.935v-1.403h.571c.844 0 1.298-.62 1.298-1.266zm-.934 0c0 .205-.15.398-.406.398h-.53v-.795h.53c.256 0 .406.193.406.397zm5.096 2.669l-.822-1.64c.299-.156.619-.493.619-1.029 0-.646-.454-1.265-1.298-1.265H61.4v3.934h.935v-1.464h.316l.673 1.464h1.084zm-1.138-2.669c0 .205-.15.398-.406.398h-.529v-.795h.53a.39.39 0 0 1 .405.397zm4.995.702c0-.591.016-1.144-.385-1.558-.277-.287-.598-.442-1.063-.442-.464 0-.785.155-1.063.442-.4.414-.384.967-.384 1.558 0 .591-.016 1.144.384 1.558.278.287.599.442 1.063.442.465 0 .786-.155 1.063-.442.401-.414.385-.967.385-1.558zm-.935 0c0 .718-.053.845-.139.961a.465.465 0 0 1-.374.177.464.464 0 0 1-.374-.177c-.085-.116-.139-.243-.139-.96 0-.72.054-.852.14-.968a.462.462 0 0 1 .373-.171.46.46 0 0 1 .374.171c.086.116.139.249.139.967zm4.77-1.105v-.862h-2.804v.862h.935v3.072h.935v-3.072h.935zm3.74 3.072v-.862h-1.65v-.69h1.41v-.862h-1.41v-.658h1.65v-.862h-2.586v3.934h2.586zm3.643-.403l-.62-.641c-.122.127-.218.215-.432.215a.437.437 0 0 1-.363-.165c-.086-.116-.145-.255-.145-.973s.06-.856.145-.972a.437.437 0 0 1 .363-.166c.214 0 .31.088.433.215l.62-.64c-.257-.266-.551-.437-1.053-.437-.444 0-.78.155-1.058.442-.4.414-.385.967-.385 1.558 0 .591-.015 1.144.385 1.558.278.287.614.442 1.058.442.502 0 .796-.171 1.052-.436zm3.74-2.669v-.862h-2.805v.862h.935v3.072h.935v-3.072h.935zm1.154 3.072h.934v-3.934h-.934v3.934zM89.462 29c0-.591.016-1.144-.384-1.558-.278-.287-.599-.442-1.064-.442-.464 0-.785.155-1.062.442-.401.414-.385.967-.385 1.558 0 .591-.016 1.144.385 1.558.277.287.598.442 1.062.442.465 0 .786-.155 1.064-.442.4-.414.384-.967.384-1.558zm-.935 0c0 .718-.053.845-.139.961a.465.465 0 0 1-.374.177.464.464 0 0 1-.373-.177c-.086-.116-.14-.243-.14-.96 0-.72.054-.852.14-.968a.462.462 0 0 1 .373-.171.46.46 0 0 1 .374.171c.086.116.14.249.14.967zm5.16 1.967v-3.934h-.934v1.978l-1.218-1.978h-.817v3.934h.934v-1.978l1.218 1.978h.818zm6.47 0l-.743-.9c.278-.315.406-.719.444-1.233h-.823c-.016.188-.07.442-.176.57l-.449-.536c.096-.067.267-.205.267-.205.235-.182.374-.376.374-.674 0-.58-.39-.989-1.063-.989-.726 0-1.116.398-1.116.978 0 .392.224.657.358.812-.283.193-.604.53-.604.984 0 .768.39 1.226 1.287 1.226.508 0 .716-.177.903-.32l.24.287h1.101zm-1.928-2.973c0 .089-.06.166-.15.233 0 0-.08.06-.128.094-.064-.067-.203-.205-.203-.32 0-.14.059-.238.24-.238.16 0 .24.099.24.231zm.058 2.045a.497.497 0 0 1-.374.143c-.235 0-.443-.17-.443-.414 0-.188.048-.304.224-.43l.593.7zm7.554.928l-.823-1.64c.3-.156.62-.493.62-1.029 0-.646-.454-1.265-1.298-1.265h-1.507v3.934h.935v-1.464h.315l.674 1.464h1.084zm-1.138-2.669c0 .205-.15.398-.406.398h-.529v-.795h.53a.39.39 0 0 1 .405.397zm4.808 2.669v-.862h-1.65v-.69h1.41v-.862h-1.41v-.658h1.65v-.862h-2.586v3.934h2.586zm3.825-1.205c0-.364-.08-.657-.283-.861-.16-.166-.406-.277-.748-.326l-.46-.067a.421.421 0 0 1-.261-.116.264.264 0 0 1-.075-.182c0-.171.133-.365.46-.365.165 0 .48-.016.72.232l.588-.607c-.326-.337-.737-.47-1.282-.47-.865 0-1.389.525-1.389 1.243 0 .337.086.602.262.79.17.183.427.299.764.348l.46.067c.122.016.202.055.25.105.054.06.075.138.075.232 0 .226-.176.353-.545.353-.304 0-.652-.072-.85-.276l-.598.619c.385.409.86.519 1.443.519.801 0 1.469-.436 1.469-1.238zm3.809 1.205v-.862h-1.651v-.69h1.41v-.862h-1.41v-.658h1.65v-.862h-2.585v3.934h2.586zm4.375 0l-1.384-3.934h-.72l-1.385 3.934h.978l.187-.586h1.17l.176.586h.978zm-1.4-1.392h-.662l.342-1.05.32 1.05zm5.401 1.392l-.823-1.64c.3-.156.62-.493.62-1.029 0-.646-.454-1.265-1.298-1.265h-1.506v3.934h.934v-1.464h.315l.674 1.464h1.084zm-1.138-2.669c0 .205-.15.398-.406.398h-.529v-.795h.53a.39.39 0 0 1 .405.397zm4.594 2.266l-.62-.641c-.122.127-.218.215-.432.215a.437.437 0 0 1-.363-.165c-.086-.116-.145-.255-.145-.973s.06-.856.145-.972a.437.437 0 0 1 .363-.166c.214 0 .31.088.433.215l.62-.64c-.257-.266-.55-.437-1.053-.437-.443 0-.78.155-1.058.442-.4.414-.384.967-.384 1.558 0 .591-.017 1.144.384 1.558.278.287.615.442 1.058.442.502 0 .796-.171 1.052-.436zm4.023.403v-3.934h-.935v1.514h-.993v-1.514h-.935v3.934h.935v-1.558h.993v1.558H133z"
        fill="#445BF4"
        opacity={0.5}
      />
    </g>
  </svg>
);

export const draggingSvg = (index = 9999, fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id={"prefix__adraggingSvg" + index}
        d="M4 14c0 1.1-.9 2-2 2s-2-.9-2-2 .9-2 2-2 2 .9 2 2zM2 6C.9 6 0 6.9 0 8s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-6C.9 0 0 .9 0 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm6 4c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
      />
    </defs>
    <g fill="none" fillRule="evenodd" transform="translate(7 4)">
      <mask id={"prefix__bdraggingSvg" + index} fill="#fff">
        <use xlinkHref={`#prefix__adraggingSvg${index}`} />
      </mask>
      <use
        fill={fill}
        fillRule="nonzero"
        xlinkHref={`#prefix__adraggingSvg${index}`}
      />
      <g fill={fill} mask={`url(#prefix__bdraggingSvg${index})`}>
        <path d="M-7-4h24v24H-7z" />
      </g>
    </g>
  </svg>
);

export const tableL = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__atableL"
        d="M5 4a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1H5zm0-2h14a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3zm13 10a1 1 0 0 1-1 1H7a1 1 0 0 1 0-2h10a1 1 0 0 1 1 1z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__btableL" fill="#fff">
        <use xlinkHref="#prefix__atableL" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__atableL" />
      <g fill={fill} mask="url(#prefix__btableL)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const tableM = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__atableM"
        d="M5 6a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1H5zm0-2h14a3 3 0 0 1 3 3v10a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V7a3 3 0 0 1 3-3zm13 8a1 1 0 0 1-1 1H7a1 1 0 0 1 0-2h10a1 1 0 0 1 1 1z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__btableM" fill="#fff">
        <use xlinkHref="#prefix__atableM" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__atableM" />
      <g fill={fill} mask="url(#prefix__btableM)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const tableS = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__atableS"
        d="M5 9a1 1 0 0 0-1 1v4a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1H5zm0-2h14a3 3 0 0 1 3 3v4a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3v-4a3 3 0 0 1 3-3zm13 5a1 1 0 0 1-1 1H7a1 1 0 0 1 0-2h10a1 1 0 0 1 1 1z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__btableS" fill="#fff">
        <use xlinkHref="#prefix__atableS" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__atableS" />
      <g fill={fill} mask="url(#prefix__btableS)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const checkedSVG = (fill = "#445bf4") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__ayyy"
        d="M17.444 5C18.3 5 19 5.7 19 6.556v10.888C19 18.3 18.3 19 17.444 19H6.556A1.56 1.56 0 0 1 5 17.444V6.556C5 5.7 5.7 5 6.556 5h10.888zM17 17V7H7v10h10zm-2.838-8.55a1 1 0 0 1 1.67 1.1l-3.724 5.65a1 1 0 0 1-1.423.257l-2.273-1.653a1 1 0 1 1 1.176-1.617l1.427 1.037 3.147-4.774z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="prefix__b7gkb" fill="#fff">
        <use xlinkHref="#prefix__ayyy" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__ayyy" />
      <g fill={fill} mask="url(#prefix__b7gkb)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const uncheckedSVG = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__a67g"
        d="M17 17H7V7h10v10zm.444-12H6.556C5.7 5 5 5.7 5 6.556v10.888C5 18.3 5.7 19 6.556 19h10.888C18.3 19 19 18.3 19 17.444V6.556C19 5.7 18.3 5 17.444 5z"
      />
    </defs>
    <use fill={fill} fillRule="evenodd" xlinkHref="#prefix__a67g" />
  </svg>
);

export const tikSvg = (fill = "#fff", id = "") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id={"prefix__atikSvg" + id}
        d="M5.75 10.836l7.543-7.543a1 1 0 0 1 1.414 1.414l-8.25 8.25a1 1 0 0 1-1.414 0l-3.75-3.75a1 1 0 1 1 1.414-1.414l3.043 3.043z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id={"prefix__btikSvg" + id} fill="#fff">
        <use xlinkHref={"#prefix__atikSvg" + id} />
      </mask>
      <use fill="#000" fillRule="nonzero" xlinkHref={"#prefix__atikSvg" + id} />
      <g fill={fill} mask={`url(#prefix__btikSvg${id})`}>
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const cardView1 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView1"
        d="M5 4a1 1 0 0 0-1 1v12h16V5a1 1 0 0 0-1-1H5zm0-2h14a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView1" fill="#fff">
        <use xlinkHref="#prefix__acardView1" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView1" />
      <g fill={fill} mask="url(#prefix__bcardView1)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const cardView2 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView2"
        d="M7 6a1 1 0 0 0-1 1v8h12V7a1 1 0 0 0-1-1H7zm0-2h10a3 3 0 0 1 3 3v10a3 3 0 0 1-3 3H7a3 3 0 0 1-3-3V7a3 3 0 0 1 3-3z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView2" fill="#fff">
        <use xlinkHref="#prefix__acardView2" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView2" />
      <g fill={fill} mask="url(#prefix__bcardView2)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const cardView3 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView3"
        d="M9 8a1 1 0 0 0-1 1v5h8V9a1 1 0 0 0-1-1H9zm0-2h6a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3H9a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView3" fill="#fff">
        <use xlinkHref="#prefix__acardView3" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView3" />
      <g fill={fill} mask="url(#prefix__bcardView3)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const cardView4 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView4"
        d="M5 2h14a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3zm0 2a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1H5z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView4" fill="#fff">
        <use xlinkHref="#prefix__acardView4" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView4" />
      <g fill={fill} mask="url(#prefix__bcardView4)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const cardView5 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView5"
        d="M7 4h10a3 3 0 0 1 3 3v10a3 3 0 0 1-3 3H7a3 3 0 0 1-3-3V7a3 3 0 0 1 3-3zm0 2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1H7z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView5" fill="#fff">
        <use xlinkHref="#prefix__acardView5" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView5" />
      <g fill={fill} mask="url(#prefix__bcardView5)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const cardView6 = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acardView6"
        d="M9 8c-.552 0-1 .597-1 1.333V15a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V9.333C16 8.597 15.552 8 15 8H9zm0-2h6a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3H9a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcardView6" fill="#fff">
        <use xlinkHref="#prefix__acardView6" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acardView6" />
      <g fill={fill} mask="url(#prefix__bcardView6)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const addIcon = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__adfg644"
        d="M13 11h6a1 1 0 0 1 0 2h-6v6a1 1 0 0 1-2 0v-6H5a1 1 0 0 1 0-2h6V5a1 1 0 0 1 2 0v6z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bdfgd5" fill="#fff">
        <use xlinkHref="#prefix__adfg644" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__adfg644" />
      <g fill={fill} mask="url(#prefix__bdfgd5)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const moreHoriz = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__a0h24v24H0z"
        d="M2 0C.9 0 0 .9 0 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zM8 0C6.9 0 6 .9 6 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(4 10)">
        <mask id="prefix__b0h24v24H0z" fill="#fff">
          <use xlinkHref="#prefix__a0h24v24H0z" />
        </mask>
        <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__a0h24v24H0z" />
        <g fill={fill} mask="url(#prefix__b0h24v24H0z)">
          <path d="M-4-10h24v24H-4z" />
        </g>
      </g>
    </g>
  </svg>
);

export const deleteSvg = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aghasdd5"
        d="M17 5h4a1 1 0 0 1 0 2h-1v13a3 3 0 0 1-3 3H7a3 3 0 0 1-3-3V7H3a1 1 0 1 1 0-2h4V4a3 3 0 0 1 3-3h4a3 3 0 0 1 3 3v1zm-2 0V4a1 1 0 0 0-1-1h-4a1 1 0 0 0-1 1v1h6zm3 2H6v13a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V7zm-9 4a1 1 0 0 1 2 0v6a1 1 0 0 1-2 0v-6zm4 0a1 1 0 0 1 2 0v6a1 1 0 0 1-2 0v-6z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bsdfjhj" fill="#fff">
        <use xlinkHref="#prefix__aghasdd5" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aghasdd5" />
      <g fill={fill} mask="url(#prefix__bsdfjhj)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const deleteSvgSmall = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__adeleteSvgSmall"
        d="M11.286 4H13a1 1 0 0 1 0 2v6.8c0 1.204-.948 2.2-2.143 2.2H5.143C3.948 15 3 14.004 3 12.8V6a1 1 0 1 1 0-2h1.714v-.8c0-1.204.948-2.2 2.143-2.2h2.286c1.195 0 2.143.996 2.143 2.2V4zm-2 0v-.8c0-.121-.075-.2-.143-.2H6.857c-.068 0-.143.079-.143.2V4h2.572zM11 6H5v6.8c0 .121.075.2.143.2h5.714c.068 0 .143-.079.143-.2V6z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bdeleteSvgSmall" fill="#fff">
        <use xlinkHref="#prefix__adeleteSvgSmall" />
      </mask>
      <use
        fill={fill}
        fillRule="nonzero"
        xlinkHref="#prefix__adeleteSvgSmall"
      />
      <g fill={fill} mask="url(#prefix__bdeleteSvgSmall)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const editSvg = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aeditSvg"
        d="M16 4.414l-12 12V20h3.586l12-12L16 4.414zm.707-2.121l5 5a1 1 0 0 1 0 1.414l-13 13A1 1 0 0 1 8 22H3a1 1 0 0 1-1-1v-5a1 1 0 0 1 .293-.707l13-13a1 1 0 0 1 1.414 0z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__beditSvg" fill="#fff">
        <use xlinkHref="#prefix__aeditSvg" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aeditSvg" />
      <g fill={fill} mask="url(#prefix__beditSvg)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const editSvgSmall = (fill = "#979797", id = "") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id={"prefix__aeditSvgSmall" + id}
        d="M3 11.08V13h1.92l7.666-7.667-1.92-1.919L3 11.081zm8.374-9.787l3.333 3.333a1 1 0 0 1 0 1.414L6.04 14.707a1 1 0 0 1-.707.293H2a1 1 0 0 1-1-1v-3.333a1 1 0 0 1 .293-.707L9.96 1.293a1 1 0 0 1 1.414 0z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id={"prefix__beditSvgSmall" + id} fill="#fff">
        <use xlinkHref={"#prefix__aeditSvgSmall" + id} />
      </mask>
      <use
        fill={fill}
        fillRule="nonzero"
        xlinkHref={"#prefix__aeditSvgSmall" + id}
      />
      <g fill={fill} mask={`url(#prefix__beditSvgSmall${id})`}>
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const maximizeCard = () => (
  <svg width={24} height={24}>
    <path
      fill="#445bf4"
      fillRule="evenodd"
      d="M8 2a1 1 0 1 1 0 2H5a1 1 0 0 0-1 1v3a1 1 0 1 1-2 0V5a3 3 0 0 1 3-3h3zm14 6a1 1 0 0 1-2 0V5a1 1 0 0 0-1-1h-3a1 1 0 0 1 0-2h3a3 3 0 0 1 3 3v3zm-6 14a1 1 0 0 1 0-2h3a1 1 0 0 0 1-1v-3a1 1 0 0 1 2 0v3a3 3 0 0 1-3 3h-3zM2 16a1 1 0 0 1 2 0v3a1 1 0 0 0 1 1h3a1 1 0 0 1 0 2H5a3 3 0 0 1-3-3v-3z"
    />
  </svg>
);

export const externalLinkCard = () => (
  <svg width={24} height={24}>
    <path
      fill="#445bf4"
      fillRule="evenodd"
      d="M18.586 4H15a1 1 0 0 1 0-2h6a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V5.414l-9.293 9.293a1 1 0 1 1-1.414-1.414L18.586 4zM17 13a1 1 0 0 1 2 0v6a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V8a3 3 0 0 1 3-3h6a1 1 0 0 1 0 2H5a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h11a1 1 0 0 0 1-1v-6z"
    />
  </svg>
);

export const message = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__acn33f"
        d="M5.63 11.473a1 1 0 0 1 .827.038A4.5 4.5 0 1 0 4.49 9.545a1 1 0 0 1 .038.826l-.734 1.837 1.837-.735zm.343 2.017L2.37 14.931c-.816.326-1.626-.483-1.3-1.3l1.44-3.601a6.5 6.5 0 1 1 3.462 3.46z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bfghjk6" fill="#fff">
        <use xlinkHref="#prefix__acn33f" />
      </mask>
      <use fill={"#fff"} fillRule="nonzero" xlinkHref="#prefix__acn33f" />
      <g fill={fill} mask="url(#prefix__bfghjk6)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const viewsSvg = (fill = "#979797") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__aviewsSvg"
        d="M1.122 7.521c.316-.58.9-1.407 1.747-2.236C4.32 3.868 6.034 3 8 3c1.966 0 3.681.868 5.13 2.285.849.829 1.432 1.656 1.748 2.236a1 1 0 0 1 0 .958c-.316.58-.9 1.407-1.747 2.236C11.68 12.132 9.966 13 8 13c-1.966 0-3.681-.868-5.13-2.285-.849-.829-1.432-1.656-1.748-2.236a1 1 0 0 1 0-.958zm2.096.528c.297.422.648.844 1.05 1.236C5.374 10.368 6.624 11 8 11s2.625-.632 3.733-1.715A8.926 8.926 0 0 0 12.816 8a8.926 8.926 0 0 0-1.084-1.285C10.626 5.632 9.376 5 8 5s-2.625.632-3.733 1.715A8.926 8.926 0 0 0 3.184 8l.034.049zM8 10a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bviewsSvg" fill="#fff">
        <use xlinkHref="#prefix__aviewsSvg" />
      </mask>
      <use fill={"#fff"} fillRule="nonzero" xlinkHref="#prefix__aviewsSvg" />
      <g fill={fill} mask="url(#prefix__bviewsSvg)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const calendar = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__adf35"
        d="M20 9V6a1 1 0 0 0-1-1h-2v1a1 1 0 0 1-2 0V5H9v1a1 1 0 1 1-2 0V5H5a1 1 0 0 0-1 1v3h16zm0 2H4v9a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1v-9zM9 3h6V2a1 1 0 0 1 2 0v1h2a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V6a3 3 0 0 1 3-3h2V2a1 1 0 1 1 2 0v1z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bdfg534" fill="#fff">
        <use xlinkHref="#prefix__adf35" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__adf35" />
      <g fill={fill} mask="url(#prefix__bdfg534)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const extrLink = () => (
  <svg width={16} height={16}>
    <path
      fill="#979797"
      fillRule="evenodd"
      d="M11.586 3H10a1 1 0 1 1 0-2h4a1 1 0 0 1 1 1v4a1 1 0 0 1-2 0V4.414L7.374 10.04A1 1 0 1 1 5.96 8.626L11.586 3zM11 8.667a1 1 0 0 1 2 0v4A2.333 2.333 0 0 1 10.667 15H3.333A2.333 2.333 0 0 1 1 12.667V5.333A2.333 2.333 0 0 1 3.333 3h4a1 1 0 1 1 0 2h-4A.333.333 0 0 0 3 5.333v7.334c0 .184.15.333.333.333h7.334c.184 0 .333-.15.333-.333v-4z"
    />
  </svg>
);

export const chanel = () => (
  <svg width={16} height={16}>
    <path
      fill="#979797"
      fillRule="evenodd"
      d="M5 1a1 1 0 1 1 0 2H3.333A.333.333 0 0 0 3 3.333V5a1 1 0 1 1-2 0V3.333A2.333 2.333 0 0 1 3.333 1H5zm10 4a1 1 0 0 1-2 0V3.333A.333.333 0 0 0 12.667 3H11a1 1 0 0 1 0-2h1.667A2.333 2.333 0 0 1 15 3.333V5zm-4 10a1 1 0 0 1 0-2h1.667c.184 0 .333-.15.333-.333V11a1 1 0 0 1 2 0v1.667A2.333 2.333 0 0 1 12.667 15H11zM1 11a1 1 0 0 1 2 0v1.667c0 .184.15.333.333.333H5a1 1 0 0 1 0 2H3.333A2.333 2.333 0 0 1 1 12.667V11z"
    />
  </svg>
);

export const closeSvg = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__acloseSvg"
        d="M13.414 12l4.293 4.293a1 1 0 0 1-1.414 1.414L12 13.414l-4.293 4.293a1 1 0 1 1-1.414-1.414L10.586 12 6.293 7.707a1 1 0 0 1 1.414-1.414L12 10.586l4.293-4.293a1 1 0 0 1 1.414 1.414L13.414 12z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bcloseSvg" fill="#fff">
        <use xlinkHref="#prefix__acloseSvg" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__acloseSvg" />
      <g fill={fill} mask="url(#prefix__bcloseSvg)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const starSvg = (fill = "#cbdae0") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__astarSvg"
        d="M5.493 4.703L7.11 1.544a1 1 0 0 1 1.78 0l1.617 3.159 3.633.511a1 1 0 0 1 .544 1.72L12.08 9.379l.614 3.446a1 1 0 0 1-1.437 1.067L8 12.242l-3.256 1.65a1 1 0 0 1-1.437-1.067l.614-3.446-2.605-2.445a1 1 0 0 1 .544-1.72l3.633-.511zm.586 3.96l-.451 2.539L8 9.999l2.372 1.203-.451-2.54 1.873-1.758-2.593-.366L8 4.194 6.8 6.538l-2.594.366 1.873 1.759z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bstarSvg" fill="#fff">
        <use xlinkHref="#prefix__astarSvg" />
      </mask>
      <use fill="#979797" fillRule="nonzero" xlinkHref="#prefix__av" />
      <g fill={fill} mask="url(#prefix__bstarSvg)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const arrowNext = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aarrowNext"
        d="M.704 9.884L4.58 6 .704 2.116a.997.997 0 1 1 1.41-1.41l4.589 4.587a1 1 0 0 1 0 1.414l-4.588 4.588a.998.998 0 0 1-1.41-1.41z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(8 6)">
        <mask id="prefix__barrowNext" fill="#fff">
          <use xlinkHref="#prefix__av" />
        </mask>
        <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aarrowNext" />
        <g fill={fill} mask="url(#prefix__barrowNext)">
          <path d="M-8-6h24v24H-8z" />
        </g>
      </g>
    </g>
  </svg>
);

export const arrowPrev = (fill = "#979797") => (
  <svg width={24} height={24}>
    <defs>
      <path
        id="prefix__aarrowPrev"
        d="M6.706 9.884L2.83 6l3.876-3.884a.997.997 0 1 0-1.41-1.41L.706 5.292a1 1 0 0 0 0 1.414l4.588 4.588a.998.998 0 0 0 1.41-1.41z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(8 6)">
        <mask id="prefix__barrowPrev" fill="#fff">
          <use xlinkHref="#prefix__aarrowPrev" />
        </mask>
        <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__aarrowPrev" />
        <g fill={fill} mask="url(#prefix__barrowPrev)">
          <path d="M-8-6h24v24H-8z" />
        </g>
      </g>
    </g>
  </svg>
);

export const starActiveSvg = (fill = "#445bf4") => (
  <svg width={16} height={16}>
    <defs>
      <path
        id="prefix__astarActiveSvg"
        d="M5.493 4.703L7.11 1.544a1 1 0 0 1 1.78 0l1.617 3.159 3.633.511a1 1 0 0 1 .544 1.72L12.08 9.379l.614 3.446a1 1 0 0 1-1.437 1.067L8 12.242l-3.256 1.65a1 1 0 0 1-1.437-1.067l.614-3.446-2.605-2.445a1 1 0 0 1 .544-1.72l3.633-.511z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h16v16H0z" />
      <mask id="prefix__bstarActiveSvg" fill="#fff">
        <use xlinkHref="#prefix__astarActiveSvg" />
      </mask>
      <use
        fill="#979797"
        fillRule="nonzero"
        xlinkHref="#prefix__astarActiveSvg"
      />
      <g fill={fill} mask="url(#prefix__bstarActiveSvg)">
        <path d="M0 0h16v16H0z" />
      </g>
    </g>
  </svg>
);

export const sendSvg = (fill = "#fff") => (
  <svg width={22} height={22}>
    <defs>
      <path
        id="prefix__asendSvg"
        d="M18.194 4.391L4.711 9.111l6.068 2.696 7.415-7.416zm1.415 1.415l-7.416 7.415 2.696 6.068 4.72-13.483zm-9.367 7.952L1.594 9.914c-.828-.368-.78-1.559.076-1.858l20-7a1 1 0 0 1 1.274 1.274l-7 20c-.3.855-1.49.904-1.858.076l-3.844-8.648z"
      />
    </defs>
    <g fill="none" fillRule="evenodd" transform="translate(-1 -1)">
      <path d="M0 0h24v24H0z" />
      <mask id="prefix__bsendSvg" fill="#fff">
        <use xlinkHref="#prefix__asendSvg" />
      </mask>
      <use fill={fill} fillRule="nonzero" xlinkHref="#prefix__asendSvg" />
      <g fill={fill} mask="url(#prefix__bsendSvg)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

export const sliderPrevSvg = (fill = "#546369") => (
  <svg width={48} height={48}>
    <defs>
      <path
        id="prefix__asliderPrevSvg"
        d="M6.706 9.884L2.83 6l3.876-3.884a.997.997 0 1 0-1.41-1.41L.706 5.292a1 1 0 0 0 0 1.414l4.588 4.588a.998.998 0 0 0 1.41-1.41z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(8 6)">
        <mask id="prefix__bsliderPrevSvg" fill="#fff">
          <use xlinkHref="#prefix__asliderPrevSvg" />
        </mask>
        <use
          fill="#000"
          fillRule="nonzero"
          xlinkHref="#prefix__asliderPrevSvg"
        />
        <g fill={fill} mask="url(#prefix__bsliderPrevSvg)">
          <path d="M-8-6h24v24H-8z" />
        </g>
      </g>
    </g>
  </svg>
);

export const sliderNextSvg = (fill = "#546369") => (
  <svg width={48} height={48}>
    <defs>
      <path
        id="prefix__asliderNextSvg"
        d="M.704 9.884L4.58 6 .704 2.116a.997.997 0 1 1 1.41-1.41l4.589 4.587a1 1 0 0 1 0 1.414l-4.588 4.588a.998.998 0 0 1-1.41-1.41z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <g transform="translate(8 6)">
        <mask id="prefix__bsliderNextSvg" fill="#fff">
          <use xlinkHref="#prefix__asliderNextSvg" />
        </mask>
        <use
          fill="#000"
          fillRule="nonzero"
          xlinkHref="#prefix__asliderNextSvg"
        />
        <g fill={fill} mask="url(#prefix__bsliderNextSvg)">
          <path d="M-8-6h24v24H-8z" />
        </g>
      </g>
    </g>
  </svg>
);

export const addUser = ( fill = "#546369", width=48, height=48 ) => (
    <svg width={width} height={height}>
      <defs>
          <path
            id="prefix__aAddUsersSvg"
            d="M20 10h2a1 1 0 0 1 0 2h-2v2a1 1 0 0 1-2 0v-2h-2a1 1 0 0 1 0-2h2V8a1 1 0 0 1 2 0v2zm-2 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H6a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h7a5 5 0 0 1 5 5v2zm-8.5-9a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
          />
      </defs>
      <g fill="none" fill-rule="evenodd">
          <path d="M0 0h24v24H0z"/>
          <mask id="baddUsersSvg" fill="#fff">
              <use xlinkHref="#prefix__aAddUsersSvg"/>
          </mask>
          <use fill="#000" fill-rule="nonzero" xlinkHref="#prefix__aAddUsersSvg"/>
          <g fill={fill} mask="url(#baddUsersSvg)">
              <path d="M0 0h24v24H0z"/>
          </g>
      </g>
  </svg>
)

export const addPost = ( fill = "#546369", width=48, height=48 ) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
  >
    <defs>
        <path
          id="prefix__aAddPost"
          d="M3 6a1 1 0 1 1 0-2h13a1 1 0 0 1 0 2H3zm0 5a1 1 0 0 1 0-2h8a1 1 0 0 1 0 2H3zm0 10a1 1 0 0 1 0-2h16a1 1 0 0 1 0 2H3zm0-5a1 1 0 0 1 0-2h11a1 1 0 0 1 0 2H3zm17-7h2a1 1 0 0 1 0 2h-2v2a1 1 0 0 1-2 0v-2h-2a1 1 0 0 1 0-2h2V7a1 1 0 0 1 2 0v2z"
          />
    </defs>
      <use fill={fill} fillRule="evenodd" xlinkHref="#prefix__aAddPost"/>
  </svg>
)

export const exportSvg = (fill = "#546369") => (
  <svg width={51} height={43}>
    <path
      fill={fill}
      d="M27.142 35.003v-13.92a2.087 2.087 0 1 0-4.174 0v13.92L18.19 30.17a2.087 2.087 0 0 0-2.968 2.934l8.348 8.444a2.087 2.087 0 0 0 2.968 0l8.348-8.444a2.087 2.087 0 1 0-2.968-2.934l-4.777 4.832zm9.353-22.341h1.083c5.729.004 10.72 3.93 12.128 9.53 1.407 5.592-1.114 11.44-6.134 14.208a2.087 2.087 0 1 1-2.015-3.655c3.354-1.85 5.047-5.776 4.101-9.535-.944-3.752-4.273-6.372-8.081-6.374h-2.63a2.087 2.087 0 0 1-2.022-1.57C31.41 9.332 26.442 4.963 20.437 4.27c-6-.693-11.808 2.427-14.605 7.858-2.802 5.44-2.001 12.066 2.012 16.66A2.087 2.087 0 0 1 4.7 31.532C-.44 25.65-1.464 17.176 2.12 10.216 5.711 3.246 13.184-.77 20.916.123c7.187.83 13.208 5.741 15.58 12.539z"
    />
  </svg>
);

export const importSvg = (fill = "#546369") => (
  <svg width={51} height={44}>
    <path
      fill={fill}
      d="M27.13 27.08V41a2.087 2.087 0 11-4.173 0V27.08l-4.777 4.832a2.087 2.087 0 11-2.969-2.935l8.348-8.444a2.087 2.087 0 012.969 0l8.347 8.444a2.087 2.087 0 11-2.968 2.935L27.13 27.08zm9.354-13.5h1.083c5.728.004 10.719 3.93 12.128 9.53 1.407 5.591-1.114 11.44-6.134 14.207a2.087 2.087 0 11-2.016-3.654c3.355-1.85 5.048-5.777 4.102-9.535-.944-3.752-4.274-6.372-8.082-6.374h-2.63a2.087 2.087 0 01-2.021-1.57C31.397 10.25 26.43 5.88 20.426 5.186c-6.001-.693-11.808 2.428-14.605 7.858-2.802 5.44-2.001 12.066 2.012 16.66a2.087 2.087 0 11-3.144 2.746C-.45 26.567-1.475 18.094 2.11 11.134 5.7 4.164 13.173.148 20.905 1.04c7.187.83 13.207 5.74 15.58 12.539z"
    />
  </svg>
);

export const warningSvg = (fill = "#f56b6b") => (
  <svg width={44} height={42}>
    <path
      d="M24.642 1.963l19.004 35.625A3 3 0 0141 42H3a3 3 0 01-2.647-4.412L19.348 1.963a3 3 0 015.294 0zM22 12a3 3 0 00-3 3v12a3 3 0 006 0V15a3 3 0 00-3-3zm0 21a3 3 0 100 6 3 3 0 000-6z"
      fill={fill}
      fillRule="evenodd"
    />
  </svg>
);

export const AppSvg = () => (
  <svg
    width="35px"
    height="56px"
    viewBox="0 0 35 56"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <defs>
      <path
        d="M8.16670312,0 L26.8334062,0 C31.3437031,0 35,3.65629688 35,8.16670312 L35,47.8334062 C35,52.3437031 31.3437031,56 26.8332969,56 L8.16670312,56 C3.65629688,56 0,52.3437031 0,47.8332969 L0,8.16670312 C0,3.65629688 3.65629688,0 8.16670312,0 Z"
        id="path-1AppSvg"
      />
    </defs>
    <g id="UI-Kit" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Icons" transform="translate(-325.000000, -1192.000000)">
        <g id="Illustration/App" transform="translate(290.000000, 1168.000000)">
          <g
            id="015-security-system"
            transform="translate(35.000000, 24.000000)"
          >
            <g id="Group-2">
              <mask id="mask-2" fill="white">
                <use xlinkHref="#path-1AppSvg" />
              </mask>
              <use
                id="Shape"
                fill="#B8CBF3"
                fillRule="nonzero"
                xlinkHref="#path-1AppSvg"
              />
            </g>
            <g
              id="Group"
              transform="translate(2.296875, 4.593750)"
              fill="#FAFAFA"
              fillRule="nonzero"
            >
              <polygon
                id="Rectangle-path"
                points="0.036421875 5.90625 30.3697188 5.90625 30.3697188 38.5729531 0.036421875 38.5729531"
              />
              <circle id="Oval" cx="15.203125" cy="44.40625" r="2.33329687" />
              <path
                d="M17.5364219,0.072953125 L12.8697188,0.072953125 C12.2253906,0.072953125 11.7030156,0.595328125 11.7030156,1.23965625 C11.7030156,1.88398438 12.2255,2.40625 12.8698281,2.40625 L17.5365312,2.40625 C18.18075,2.40625 18.703125,1.883875 18.703125,1.23954687 C18.703125,0.59521875 18.18075,0.072953125 17.5364219,0.072953125 Z"
                id="Shape"
              />
              <path
                d="M24.583125,0.072953125 L24.5598281,0.072953125 C23.9155,0.072953125 23.393125,0.595328125 23.393125,1.23965625 C23.393125,1.88398438 23.9155,2.40625 24.5598281,2.40625 C25.2041563,2.40625 25.7265313,1.883875 25.7265313,1.23954687 C25.7265313,0.59521875 25.2041563,0.07284375 24.5598281,0.07284375 L24.583125,0.07284375 L24.583125,0.072953125 Z"
                id="Shape"
              />
            </g>
            <path
              d="M27.1157031,16.3682969 L17.7824062,14.035 C17.5969062,13.9895 17.4032031,13.9895 17.2177031,14.035 L7.88440625,16.3682969 C7.364875,16.4979063 7.00010937,16.9645 7.00010937,17.5 L7.00010937,31.5 C7.00010937,35.1072969 14.7001094,38.5932969 17.0521094,39.578 C17.3395469,39.697875 17.6629687,39.697875 17.9504062,39.578 C20.3,38.5932969 28,35.1072969 28,31.5 L28,17.5 C28,16.9645 27.6352344,16.4979063 27.1157031,16.3682969 Z"
              id="Shape"
              fill="#546CAB"
              fillRule="nonzero"
            />
            <g
              id="Group"
              transform="translate(11.593750, 18.703125)"
              fill="#FAFAFA"
              fillRule="nonzero"
            >
              <polygon
                id="Shape"
                points="5.90625 0.105875 0.072953125 1.27257812 0.072953125 8.13017187 5.90625 8.13017187"
              />
              <polygon
                id="Shape"
                points="5.90625 8.13017188 5.90625 15.8395781 11.7395469 12.9951719 11.7395469 8.13017188"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export const DomainSvg = () => (
  <svg
    width="49px"
    height="56px"
    viewBox="0 0 49 56"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <defs>
      <path
        d="M48.0633252,7.03536842 L24.7819955,0.0490136452 C24.5640601,-0.0162651072 24.3317194,-0.0162651072 24.113784,0.0490136452 L0.832454343,7.03536842 C0.339398664,7.18262768 0.00130957684,7.63619493 0.00130957684,8.15089279 L0.00130957684,33.7675634 C0.00130957684,44.0654503 15.6975702,53.222269 24.102216,55.8397505 C24.3295367,55.9112515 24.5733363,55.9112515 24.800657,55.8397505 C33.1959176,53.2221598 48.8921782,44.0654503 48.8921782,33.7675634 L48.8921782,8.15078363 C48.8921782,7.63674074 48.5553987,7.18339181 48.0633252,7.03536842 Z"
        id="path-1DomainSvg"
      />
    </defs>
    <g id="UI-Kit" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Icons" transform="translate(-510.000000, -1192.000000)">
        <g
          id="Illustration/Domain"
          transform="translate(482.000000, 1168.000000)"
        >
          <g id="030-bug" transform="translate(28.000000, 24.000000)">
            <g id="Group-3">
              <mask id="mask-2" fill="white">
                <use xlinkHref="#path-1DomainSvg" />
              </mask>
              <use
                id="Shape"
                fill="#B8CBF3"
                fillRule="nonzero"
                xlinkHref="#path-1DomainSvg"
              />
            </g>
            <path
              d="M33.7592539,27.9455302 C33.1163608,27.9455302 32.5952584,27.4241715 32.5952584,26.7810994 C32.5952584,26.3405224 32.8439688,25.9376062 33.2377149,25.7402417 L37.8940245,23.4114932 C38.4693653,23.1240702 39.1687884,23.3575673 39.4562405,23.9331774 C39.7435835,24.5086784 39.5101514,25.2082963 38.9347016,25.4958285 L34.278392,27.824577 C34.1169866,27.9043743 33.9393207,27.9458558 33.7592539,27.9455302 Z M38.4154543,34.931883 L33.7591448,34.931883 C33.1162517,34.931883 32.5950401,34.4105263 32.5950401,33.7674542 C32.5950401,33.1243821 33.1162517,32.6030253 33.7591448,32.6030253 L38.4154543,32.6030253 C39.0583474,32.6030253 39.579559,33.1243821 39.579559,33.7674542 C39.579559,34.4105263 39.0583474,34.931883 38.4154543,34.931883 Z M36.0873541,44.2469882 C35.7784031,44.2475322 35.4818931,44.125271 35.2631938,43.9069474 L31.7709889,40.41377 C31.324314,39.9512515 31.3371915,39.2140819 31.7995813,38.7672827 C32.2507305,38.3313996 32.9658686,38.3313996 33.4170178,38.7672827 L36.9092227,42.26046 C37.3631002,42.715883 37.3620089,43.4531618 36.9067127,43.9071657 C36.6891047,44.1241793 36.394559,44.2463314 36.0873541,44.2469882 Z M15.1341247,27.9455373 C14.9538396,27.9452008 14.7760646,27.902846 14.6149866,27.8220663 L9.95867706,25.4933177 C9.3833363,25.2058947 9.1497951,24.5062768 9.43713808,23.9306667 C9.72448107,23.3551657 10.4239042,23.1215595 10.9993541,23.4089825 L15.6556637,25.737731 C16.2316592,26.0232982 16.4672739,26.7218246 16.1817862,27.2980897 C15.9845857,27.6962027 15.5781804,27.9472749 15.1341247,27.9455373 Z M15.1341247,34.931883 L10.4778151,34.931883 C9.83492205,34.931883 9.31371047,34.4105263 9.31371047,33.7674542 C9.31371047,33.1243821 9.83492205,32.6030253 10.4778151,32.6030253 L15.1341247,32.6030253 C15.7770178,32.6030253 16.2982294,33.1243821 16.2982294,33.7674542 C16.2982294,34.4105263 15.7770178,34.931883 15.1341247,34.931883 Z M12.8060245,44.2469884 C12.1631314,44.2481871 11.6410468,43.7277037 11.6398445,43.0846316 C11.6393007,42.7746121 11.7624009,42.4771462 11.9818641,42.2581676 L15.474069,38.7649903 C15.9364588,38.318191 16.6734232,38.3310721 17.120098,38.7935906 C17.5558597,39.2448655 17.5558597,39.9602027 17.120098,40.4114776 L13.6278931,43.904655 C13.4100668,44.1231969 13.1144298,44.2463314 12.8060245,44.2469884 Z"
              id="Combined-Shape"
              fill="#6D85C0"
              fillRule="nonzero"
            />
            <g
              id="Group-2"
              transform="translate(13.000000, 18.000000)"
              fill="#546CAB"
              fillRule="nonzero"
            >
              <path
                d="M20.4589243,4.42390643 C20.3948641,4.3754386 20.3262205,4.33330214 20.2540846,4.29815205 L16.9272138,0.970354776 C16.7085145,0.752140351 16.4120045,0.629769981 16.1030535,0.63031397 L6.79054343,0.63031397 C6.48159243,0.629769981 6.18508241,0.752031189 5.96638307,0.970354776 L2.63951225,4.30055361 C2.56726726,4.3357037 2.49873274,4.37784016 2.43467261,4.42630799 C1.71058575,4.99220273 0.970238307,6.50594932 0.970238307,13.4409981 C0.970238307,22.7887407 6.40880178,28.5757349 11.446853,28.5757349 C16.4849042,28.5757349 21.9234677,22.7887407 21.9234677,13.4385965 C21.9232494,6.50354776 21.182902,4.99220273 20.4589243,4.42390643 Z"
                id="Shape"
              />
            </g>
            <path
              d="M25.6107394,23.3125576 L25.6107394,45.411306 C25.6107394,46.0544873 25.0895278,46.5757349 24.4466347,46.5757349 C23.8037416,46.5757349 23.2825301,46.0543782 23.2825301,45.411306 L23.2825301,23.3129219 C21.764221,23.2011674 20.3252968,22.5273638 19.2619109,21.3993138 C18.610069,20.5088811 18.3937706,19.3720702 18.6729287,18.3043587 C18.7061047,18.1910487 18.7563051,18.0834152 18.8218931,17.9852788 L20.9381402,14.8099745 C20.8367821,13.3046703 20.329568,11.643961 19.7903252,11.643961 C19.1474321,11.643961 18.6262205,11.1226043 18.6262205,10.4795322 C18.6262205,9.83646004 19.1474321,9.31510331 19.7903252,9.31510331 C22.2562881,9.31510331 22.9817806,12.2606602 23.1948654,13.9728187 L25.6985268,13.9728187 C25.9116288,12.2606441 26.6371429,9.31521248 29.1030535,9.31521248 C29.7459465,9.31521248 30.2671581,9.8365692 30.2671581,10.4796413 C30.2671581,11.1227135 29.7459465,11.6440702 29.1030535,11.6440702 C28.5593211,11.6440702 28.0515831,13.306268 27.9541693,14.8083704 L30.0714855,17.9852788 C30.1369644,18.0835244 30.1872739,18.1910487 30.2204499,18.3043587 C30.4988441,19.3714152 30.2825457,20.5070253 29.6313586,21.3969123 C28.5682717,22.5257114 27.1293338,23.2002915 25.6107394,23.3125576 Z"
              id="Combined-Shape"
              fill="#6D85C0"
              fillRule="nonzero"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export const MarketSvg = () => (
  <svg
    width="54px"
    height="56px"
    viewBox="0 0 54 56"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <defs>
      <path
        d="M26.1545244,19.831423 C26.1547439,18.2288187 27.2513171,16.8319844 28.8144634,16.4430409 L38.1802805,14.1142924 C38.7379512,13.9788226 39.3202073,13.9788226 39.877878,14.1142924 L45.6893537,15.5604678 L49.5293902,1.55511891 C49.700061,0.934861598 49.3329268,0.294409357 48.7092927,0.124662768 C48.6079878,0.0970448343 48.5035,0.0833970713 48.3984634,0.0833970713 L1.56915854,0.0833970713 C0.922585366,0.0840545809 0.398939024,0.605957115 0.399706473,1.24902924 C0.399706473,1.35164133 0.413536585,1.45381676 0.440646341,1.55282651 L6.5284878,23.7391969 C7.2267561,26.2666199 9.53360976,28.0210682 12.1690732,28.0288187 L24.9838659,28.0288187 C25.1843902,28.0288187 25.3817317,27.9781676 25.557561,27.8821053 L26.1546341,27.5490526 L26.1546341,19.831423 L26.1545244,19.831423 Z"
        id="path-1MarketSvg"
      />
    </defs>
    <g id="UI-Kit" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Icons" transform="translate(-699.000000, -1192.000000)">
        <g
          id="Illustration/Market"
          transform="translate(674.000000, 1168.000000)"
        >
          <g
            id="009-secure-shopping"
            transform="translate(25.000000, 24.000000)"
          >
            <g id="Group-2" transform="translate(2.000000, 14.000000)">
              <mask id="mask-2" fill="white">
                <use xlinkHref="#path-1MarketSvg" />
              </mask>
              <use
                id="Shape"
                fill="#B8CBF3"
                fillRule="nonzero"
                xlinkHref="#path-1MarketSvg"
              />
            </g>
            <g id="Group" fill="#6D85C0" fillRule="nonzero">
              <path
                d="M15.2765122,16.4120394 C14.629939,16.4117115 14.1060732,15.8901365 14.1064023,15.2470643 C14.1064023,15.0177154 14.1747805,14.7934971 14.3024268,14.6025731 L23.6682439,0.629863548 C24.0271463,0.0948615984 24.754061,-0.0495594542 25.2920854,0.307290448 C25.83,0.664249513 25.9752073,1.38722807 25.6164146,1.92233918 L25.6164146,1.92233918 L16.2505976,15.8950487 C16.033061,16.218386 15.6675732,16.4123665 15.2765122,16.4120394 Z"
                id="Shape"
              />
              <path
                d="M38.6911098,16.4120401 C38.2987317,16.4125848 37.9321463,16.2176218 37.7147195,15.8927563 L28.3489024,1.92004678 C27.99,1.38504483 28.1352073,0.661957115 28.6732317,0.304998051 C29.2112561,-0.0519610136 29.9381707,0.092460039 30.2970732,0.62757115 L39.6628902,14.6002807 C40.0223415,15.134846 39.878122,15.8579337 39.3407561,16.2155478 C39.1484634,16.3433762 38.9223659,16.4118207 38.6911098,16.4120401 Z"
                id="Shape"
              />
              <path
                d="M52.7398902,16.412039 L1.22773171,16.412039 C0.581158537,16.412039 0.0569634146,15.8906823 0.0569634146,15.2476101 C0.0569634146,14.604538 0.581158537,14.0831813 1.22773171,14.0831813 L52.7398902,14.0831813 C53.3864634,14.0831813 53.9106585,14.604538 53.9106585,15.2476101 C53.9106585,15.8906823 53.3864634,16.412039 52.7398902,16.412039 Z"
                id="Shape"
              />
            </g>
            <path
              d="M50.6817439,32.7136062 L41.3159268,30.3848577 C41.1297805,30.3394464 40.9354024,30.3394464 40.7492561,30.3848577 L31.383439,32.7136062 C30.8620976,32.842963 30.496061,33.3086472 30.496061,33.8431033 L30.496061,47.8158129 C30.496061,51.416078 38.2228902,54.8952827 40.5830854,55.8780663 C40.8715244,55.9977076 41.1960732,55.9977076 41.4845122,55.8780663 C43.8494268,54.8952827 51.5715366,51.416078 51.5715366,47.8158129 L51.5715366,33.8431033 C51.5710976,33.3079922 51.2040732,32.8419805 50.6817439,32.7136062 Z"
              id="Shape"
              fill="#546CAB"
              fillRule="nonzero"
            />
            <g
              id="Group"
              transform="translate(35.121951, 35.040936)"
              fill="#FAFAFA"
              fillRule="nonzero"
            >
              <polygon
                id="Shape"
                points="5.91058537 0.0992280702 0.0569634146 1.26365692 0.0569634146 8.10559064 5.91058537 8.10559064"
              />
              <polygon
                id="Shape"
                points="5.91058537 8.10559064 5.91058537 15.7998596 11.7643171 12.9611072 11.7643171 8.10559064"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export const SocialSvg = () => (
  <svg
    width="56px"
    height="56px"
    viewBox="0 0 56 56"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <defs>
      <path
        d="M32.6317031,47.7400546 L32.6317031,33.767345 C32.6321406,32.1640858 33.726,30.766924 35.2847031,30.378963 L37.6764063,29.7828304 C38.2012969,28.8658713 38.497375,27.8362573 38.5397031,26.7809903 C38.49825,26.3610448 38.5975625,25.9392437 38.822,25.5816296 C39.1857813,25.4906979 39.4836094,25.2306745 39.6222969,24.8829942 C40.2945156,23.2598674 40.7153906,21.5440624 40.8705938,19.7946355 C40.8705938,19.6996647 40.8587813,19.6050214 40.8355938,19.5128889 C40.66825,18.8332476 40.2679375,18.2334035 39.7038906,17.8174971 L39.7038906,11.6438519 C39.7038906,7.89217934 38.5558906,6.35288889 37.3448906,5.46092788 C37.1187031,3.65386355 35.1774063,0 28.0374063,0 C21.7027344,0.254565302 16.6257656,5.32163743 16.3707031,11.643961 L16.3707031,17.8176062 C15.8067656,18.2335127 15.4063438,18.8333567 15.239,19.5129981 C15.2158125,19.6051306 15.2041094,19.6997739 15.204,19.7947446 C15.35975,21.545154 15.7815,23.2617232 16.4547031,24.8855049 C16.5557656,25.2146277 16.8207813,25.4677739 17.1547031,25.5539025 C17.283,25.6190721 17.528,25.9568187 17.528,26.7812086 C17.8251719,29.2467368 19.0596875,31.5043119 20.9767031,33.0875945 C20.7434063,34.6129123 19.593,38.6347602 17.1594063,39.6477817 L6.81570313,43.0827758 C4.1584375,43.9594542 2.13992188,46.1385419 1.47240625,48.8512125 L0.07240625,54.4402963 C-0.086078125,55.0636101 0.291703125,55.6971852 0.916234375,55.8553606 C1.01029688,55.8791579 1.10698438,55.8911657 1.20410938,55.8911657 L39.7974063,55.8911657 C35.9614063,53.7809591 32.6317031,51.0143626 32.6317031,47.7400546 Z"
        id="path-1SocialSvg"
      />
    </defs>
    <g
      id="UI-KitSocialSvg"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g id="IconsSocialSvg" transform="translate(-1082.000000, -1192.000000)">
        <g
          id="Illustration/SocialSocialSvg"
          transform="translate(1058.000000, 1168.000000)"
        >
          <g
            id="001-user-1SocialSvg"
            transform="translate(24.000000, 24.000000)"
          >
            <path
              d="M55.0807031,32.6379571 L45.7474062,30.3092086 C45.5619062,30.2637973 45.3682031,30.2637973 45.1827031,30.3092086 L35.8494062,32.6379571 C35.329875,32.7673138 34.9651094,33.2329981 34.9651094,33.7674542 L34.9651094,47.7401637 C34.9651094,51.3404288 42.6651094,54.8196335 45.0171094,55.8024172 C45.3045469,55.9220585 45.6279687,55.9220585 45.9154062,55.8024172 C48.2651094,54.8196335 55.9651094,51.3404288 55.9651094,47.7401637 L55.9651094,33.7674542 C55.9648906,33.2329981 55.6002344,32.7672047 55.0807031,32.6379571 Z"
              id="ShapeSocialSvg"
              fill="#546CAB"
              fillRule="nonzero"
            />
            <g id="Group-2SocialSvg">
              <mask id="mask-2SocialSvg" fill="white">
                <use xlinkHref="#path-1SocialSvg" />
              </mask>
              <use
                id="ShapeSocialSvg"
                fill="#B8CBF3"
                fillRule="nonzero"
                xlinkHref="#path-1SocialSvg"
              />
            </g>
            <g
              id="GroupSocialSvg"
              transform="translate(39.593750, 35.040936)"
              fill="#FAFAFA"
              fillRule="nonzero"
            >
              <polygon
                id="ShapeSocialSvg"
                points="5.87125 0.0328576998 0.037953125 1.19728655 0.037953125 8.04162183 5.87125 8.04162183"
              />
              <polygon
                id="ShapeSocialSvg"
                points="5.87125 8.04162183 5.87125 15.7358908 11.7046562 12.8971384 11.7046562 8.04162183"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);
