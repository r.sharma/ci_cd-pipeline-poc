import React, { PureComponent } from "react";
import { ViewDetailsWrapper } from "components";
import { UsersDetail } from "pages/Administration/components/Users/Detail";

export class ViewDetails extends PureComponent {
    myRef = React.createRef();

    closePanel = () => {
        const { showViewDetailsPanelAction } = this.props;

        showViewDetailsPanelAction( null );
    };

    render() {
        const {
            activeModuleState,
            userOverview,
            userRoles,
            userBrands
        } = this.props;

        if ( !userOverview || !userRoles || !userBrands ) return null;

        return (
            <ViewDetailsWrapper myRef={ this.myRef } >
                {activeModuleState === "users" ? (
                    <UsersDetail
                        userOverview={userOverview}
                        userRoles={userRoles}
                        userBrands={userBrands}
                        closePanel={this.closePanel}
                    />
                ) : (
                    <button onClick={this.closePanel}>Close</button>
                )}
            </ViewDetailsWrapper>
        );
    }
}
