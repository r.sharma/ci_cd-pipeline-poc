import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import { PostCounter } from "./PostCounter";

storiesOf( "PostCounter", module )
    .add( "equals 1", () => <PostCounter count="1" /> )
    .add( "more or less 1", () => (
        <Fragment>
            <PostCounter count="23" />
            <br />
            <PostCounter count="0" />
        </Fragment>
    ) );
