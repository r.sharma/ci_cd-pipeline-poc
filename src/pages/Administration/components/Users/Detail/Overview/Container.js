import { connect } from "react-redux";
import { UsersDetailOverview as component } from "./Overview";

const mapDispatchToProps = {};

const mapStateToProps = state => {
    return {};
};

export const UsersDetailOverview = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
