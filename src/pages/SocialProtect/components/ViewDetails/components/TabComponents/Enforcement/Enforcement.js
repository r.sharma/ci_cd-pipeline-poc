import React from "react";
import moment from "moment";
import { dataFormat } from "config";

export const Enforcement = ( { data } ) => {
    return (
        <div className="info-wrapper">
            <div className="tab-title">Enforcement status</div>
            <ul className="list-info">
                <li>
                    <span className="list-title">Reported date</span>
                    <span>
                        {data.postEnforcedAt || data.profileEnforcedAt
                            ? moment( data.postEnforcedAt || data.profileEnforcedAt ).format(
                                dataFormat
                            )
                            : "No Data"}
                    </span>
                </li>
                <li>
                    <span className="list-title">Removed date</span>
                    <span>
                        {data.profileEnforcementSuccessfulAt ||
            data.postEnforcementSuccessfulAt
                            ? moment(
                                data.profileEnforcementSuccessfulAt ||
                    data.postEnforcementSuccessfulAt
                            ).format( dataFormat )
                            : "No Data"}
                    </span>
                </li>
            </ul>
        </div>
    );
};
