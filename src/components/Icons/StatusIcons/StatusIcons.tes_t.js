import React from "react";
import { shallow } from "enzyme";
import { StatusIcons } from "./StatusIcons";

describe( "StatusIcons", () => {
    it( "renders 1 <StatusIcons /> component", () => {
        const component = shallow( <StatusIcons /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const props = {
            status: "Unhandled",
            text: true,
            type: "card-status"
        };
        const component = shallow( <StatusIcons {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );
        const status = props.status
            .toString()
            .toLowerCase()
            .replace( / /g, "-" );
        expect( component.props().className ).toEqual(
            `status-icons ${props.type} ${status}`
        );
        expect( component.find( "span" ).text() ).toEqual( props.status );
    } );
} );
