import React from "react";
import { storiesOf } from "@storybook/react";
import { TextAreaInput } from "./TextAreaInput";

storiesOf( "Text Input", module )
    .add( "normal state", () => (
        <TextAreaInput inputClassName="test" placeholder={"placeholder"} />
    ) )
    .add( "error", () => (
        <TextAreaInput
            placeholder={"placeholder"}
            meta={{
                touched: true,
                error: "Error message"
            }}
        />
    ) )
    .add( "success", () => (
        <TextAreaInput
            placeholder={"placeholder"}
            meta={{
                success: true
            }}
        />
    ) );
