import React from "react";
import { storiesOf } from "@storybook/react";
import { CardActions } from "./Actions";

storiesOf( "Card Actions", module )
    .add( "vertically state", () => (
        <div style={{ padding: "100px 0 0 300px" }}>
            <CardActions />
        </div>
    ) )
    .add( "horizontally state", () => (
        <div style={{ padding: "100px 0 0 300px" }}>
            <CardActions horizontally={true} />
        </div>
    ) );
