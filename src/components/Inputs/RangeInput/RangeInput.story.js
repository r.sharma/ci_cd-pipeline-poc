import React from "react";
import { storiesOf } from "@storybook/react";
import { RangeInput } from "./RangeInput";
import { action } from "@storybook/addon-actions";

storiesOf( "Range Input", module ).add( "normal state", () => (
    <RangeInput
        min={0}
        max={100}
        minRange={0}
        maxRange={250}
        onChange={data => {
            action( "change-input", data );
        }}
    />
) );
