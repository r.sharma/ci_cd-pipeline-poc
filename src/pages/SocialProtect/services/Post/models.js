export function PostsTemplate() {
    return {
        Brand: { key: "brandID" },
        Platform: { key: "platform" },
        "Profile name": { key: "profileName", profile: true },
        "Profile ID": { key: "profileID", profile: true },
        "Profile URL": { key: "profileURL", profile: true },
        "Profile followers": { key: "profileFollowers", profile: true },
        "Profile views": { key: "profileViews", profile: true },
        "Profile likes": { key: "profileLikes", profile: true },
        "Profile posts": { key: "profilePosts", profile: true },
        "Profile picture": { key: "profileImage", profile: true },
        "Profile description": { key: "profileDescription", profile: true },
        "Profile evidence URL": { key: "evidenceURL", profile: true },
        "Profile connected to": { key: "connectedTo", profile: true },
        "Profile country": { key: "countryCode2", profile: true },
        "Profile submitted by": { key: "submittedBy", profile: true },
        "Profile comments": { key: "note", profile: true },
        "Listing ID": { key: "listingID" },
        "Listing URL": { key: "listingURL" },
        "Evidence URL": { key: "evidenceURL" },
        "Listing title": { key: "listingTitle" },
        "Listing description": { key: "listingDescription" },
        "Listing image URL": { key: "listingImage" },
        "Post likes": { key: "listingLikes" },
        "Post shares": { key: "listingShares" },
        "Post followers": { key: "listingFollowers" },
        "Post views": { key: "listingViews" },
        "Connected to": { key: "connectedTo" },
        Country: { key: "countryCode2" },
        Label: { key: "mark" },
        "Infringement type": { key: "infringementType" },
        "Enforcement status": { key: "enforcementStatus" },
        "Submitted by": { key: "submittedBy" },
        Comments: { key: "note" }
    };
}

export const columnsPosts = {
    taskIds: [
        "Status",
        "Platform",
        "Profile name",
        "Posts",
        "Title",
        "Views",
        "Date",
        "Description"
    ]
};

export const listEnabledItem = {
    Title: {
        droppableId: "Title",
        label: "Title",
        value: true,
        disabled: true,
        visibilityPosts: true,
        visibilityProfiles: false,
        fieldNamePosts: "listingTitle",
        fieldNameProfiles: null,
        headerTable: true
    },
    Profile: {
        droppableId: "Profile",
        label: "Profile name",
        value: true,
        disabled: true,
        visibilityPosts: false,
        visibilityProfiles: true,
        fieldNamePosts: "profileName",
        fieldNameProfiles: "profileName",
        headerTable: true
    },
    "Profile name": {
        droppableId: "Profile name",
        label: "Profile name",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: false,
        fieldNamePosts: "profileName",
        fieldNameProfiles: "profileName",
        headerTable: true
    },
    Platform: {
        droppableId: "Platform",
        label: "Platform",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: true,
        fieldNamePosts: "platform.name.raw",
        fieldNameProfiles: "platform.name.raw",
        headerTable: true
    },
    "Posts in DB": {
        droppableId: "Posts in DB",
        label: "Posts in DB",
        value: true,
        disabled: false,
        visibilityPosts: false,
        visibilityProfiles: true,
        fieldNamePosts: "countPostsInDB",
        fieldNameProfiles: "countPostsInDB",
        headerTable: true
    },
    Posts: {
        droppableId: "Posts",
        label: "Posts",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: false,
        fieldNamePosts: "profilePosts",
        fieldNameProfiles: "profilePosts",
        headerTable: false
    },
    Views: {
        droppableId: "Views",
        label: "Views",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: true,
        fieldNamePosts: "listingViews",
        fieldNameProfiles: "profileViews",
        headerTable: true
    },
    Date: {
        droppableId: "Date",
        label: "Date",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: true,
        fieldNamePosts: "createdAt",
        fieldNameProfiles: "createdAt",
        headerTable: true
    },
    Description: {
        droppableId: "Description",
        label: "Description",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: true,
        fieldNamePosts: "listingDescription.raw",
        fieldNameProfiles: "profileDescription",
        headerTable: true
    },
    Status: {
        droppableId: "Status",
        label: "Status",
        value: true,
        disabled: false,
        visibilityPosts: true,
        visibilityProfiles: true,
        fieldNamePosts: "status",
        fieldNameProfiles: "status",
        headerTable: false
    }
};
