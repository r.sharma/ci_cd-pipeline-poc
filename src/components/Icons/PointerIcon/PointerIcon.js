import React from "react";
import "./PointerIcon.scss";

export const PointerIcon = ( {
    wrapperClassName = null,
    ImgClassName = null,
    borderRadius = null,
    imgAlt = null,
    size = null,
    img = "",
    style = {}
} ) => (
    <div style={style} className={wrapperClassName}>
        <img
            className={ImgClassName}
            style={{ height: size, width: size, borderRadius: borderRadius }}
            src={`${process.env.REACT_APP_STATIC_URL}${img}`}
            alt={imgAlt ? `icon-${imgAlt}` : null}
        />
    </div>
);
