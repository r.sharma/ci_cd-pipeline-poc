import React from "react";
import moment from "moment";
import { RowLinkView, SocialTitle, StatusIcons, Checkbox } from "components";
import {
    CardActions,
    GridTableHeadOrderDirectionByFields
} from "pages/SocialProtect/components";
import { dataFormat } from "config";
import "./GridTableView.scss";

const initialData = {
    Platform: { value: true },
    Chanel: { value: true },
    Posts: { value: true },
    Views: { value: true },
    Date: { value: true },
    Description: { value: true },
    Status: { value: true }
};

export const GridTableView = ( {
    data,
    activeModuleState = "posts",
    tableView = "large",
    columnSelected = initialData,
    chooseCardToSelect = () => {},
    selectedCardsList = [],
    columnOrderList = [],
    activeFilterValueState: { orderDirection = null } = {},
    chooseAllCardAction = () => {},
    selectedAllCardsState = false,
    deleteOneCardsAction = () => {},
    showViewDetailsPanelAction = () => {},
    changeOrderDirectionAction = () => {},
    userPushToRouteAction = () => {}
} ) => {
    if ( data && !data.length ) return "No items...";
    let newArr = [];
    let visibleValue = undefined;
    let fieldName = null;
    if ( activeModuleState === "posts" ) {
        visibleValue = "visibilityPosts";
        fieldName = "fieldNamePosts";
    } else if ( activeModuleState === "profiles" ) {
        visibleValue = "visibilityProfiles";
        fieldName = "fieldNameProfiles";
    }
    for ( let key in columnSelected ) {
        if ( columnSelected[key][visibleValue] ) {
            columnSelected[key].order = columnOrderList.indexOf(
                columnSelected[key].droppableId
            );
            newArr.push( columnSelected[key] );
        }
    }
    newArr = newArr.sort( ( a, b ) =>
        a.order > b.order ? 1 : b.order > a.order ? -1 : 0
    );
    return (
        <div className={`table-main ${tableView}`}>
            <table align="center">
                <thead>
                    <tr>
                        <th>
                            <Checkbox
                                handleChange={() => {
                                    chooseAllCardAction( !selectedAllCardsState );
                                }}
                                defaultValue={selectedAllCardsState}
                            />
                        </th>
                        {newArr.length
                            ? newArr.map( ( e, i ) => {
                                if ( !e.value ) return null;
                                return (
                                    <th key={i}>
                                        {e.headerTable ? (
                                            <GridTableHeadOrderDirectionByFields
                                                orderDirection={orderDirection}
                                                label={e.label}
                                                fieldName={e[fieldName]}
                                                handleClick={() => changeOrderDirectionAction( e )}
                                            />
                                        ) : (
                                            e.label
                                        )}
                                    </th>
                                );
                            } )
                            : null}
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {data && data.length
                        ? data.map( ( e, i ) => (
                            <tr key={i}>
                                <td>
                                    <Checkbox
                                        handleChange={( { value } ) => {
                                            chooseCardToSelect( {
                                                value: value,
                                                id: e.id
                                            } );
                                        }}
                                        defaultValue={selectedCardsList.indexOf( e.id ) !== -1}
                                    />
                                </td>
                                {newArr.length
                                    ? newArr.map( ( el, i ) => {
                                        if ( !el.value ) return null;
                                        return getRowValue(
                                            e,
                                            el,
                                            i,
                                            showViewDetailsPanelAction,
                                            userPushToRouteAction
                                        );
                                    } )
                                    : null}

                                <td>
                                    <CardActions
                                        editBtn={false}
                                        horizontally={true}
                                        id={e.id}
                                        deleteOneCardsAction={deleteOneCardsAction}
                                    />
                                </td>
                            </tr>
                        ) )
                        : null}
                </tbody>
            </table>
        </div>
    );
};

const getRowValue = (
    data,
    elem,
    i,
    showViewDetailsPanelAction,
    userPushToRouteAction
) => {
    if ( elem.droppableId === "Title" || elem.droppableId === "Profile" ) {
        const URL = data.profileURL || data.listingURL || null;
        return (
            <td key={i}>
                <RowLinkView
                    label={data.listingTitle || data.profileName || null}
                    handleClick={() => showViewDetailsPanelAction( data.id )}
                    iconLink={URL}
                    handleClickIcon={() => {
                        if ( URL ) window.open( URL, "_blank" );
                    }}
                />
            </td>
        );
    }
    if ( elem.droppableId === "Profile name" ) {
        return (
            <td key={i}>
                <RowLinkView
                    channel={true}
                    iconLink={( data.profile && data.profile.profileURL ) || null}
                    label={( data.profile && data.profile.profileName ) || null}
                    tooltip={
                        data.profile && data.profile.profileURL
                            ? "Show all posts from this channel"
                            : false
                    }
                    handleClickIcon={() =>
                        userPushToRouteAction( {
                            profileURL: data.profile.profileURL,
                            module: "posts"
                        } )
                    }
                />
            </td>
        );
    }
    if ( elem.droppableId === "Platform" ) {
        return (
            <td key={i}>
                <SocialTitle code={data.platform.code} title={data.platform.name} />
            </td>
        );
    }
    if ( elem.droppableId === "Posts" ) {
        if ( Object.prototype.hasOwnProperty.call( data, "profile" ) ) {
            return (
                <td key={i}>{( data.profile && data.profile.profilePosts ) || 0}</td>
            );
        } else {
            return (
                <td key={i}>
                    <RowLinkView
                        label={data.countPostsInDB || 0}
                        iconLink={!!data.profileURL}
                        handleClickIcon={() =>
                            userPushToRouteAction( {
                                profileURL: data.profileURL,
                                module: "posts"
                            } )
                        }
                        tooltip={
                            data.profileURL ? "Show all posts from this channel" : false
                        }
                        channel={true}
                    />
                </td>
            );
        }
    }
    if ( elem.droppableId === "Posts in DB" ) {
        if ( Object.prototype.hasOwnProperty.call( data, "profile" ) ) {
            return (
                <td key={i}>{( data.profile && data.profile.profilePosts ) || 0}</td>
            );
        } else {
            return (
                <td key={i}>
                    <RowLinkView
                        label={data.countPostsInDB || 0}
                        iconLink={!!data.profileURL}
                        handleClickIcon={() =>
                            userPushToRouteAction( {
                                profileURL: data.profileURL,
                                module: "posts"
                            } )
                        }
                        tooltip={
                            data.profileURL ? "Show all posts from this channel" : false
                        }
                        channel={true}
                    />
                </td>
            );
        }
    }
    if ( elem.droppableId === "Views" ) {
        return (
            <td key={i}>
                {data.profileViews || ( data.profile && data.profile.profileViews ) || 0}
            </td>
        );
    }
    if ( elem.droppableId === "Date" ) {
        return <td key={i}>{moment( data.createdAt ).format( dataFormat )}</td>;
    }
    if ( elem.droppableId === "Description" ) {
        return (
            <td key={i} className="description">
                {data.listingDescription || data.profileDescription || "No Description"}
            </td>
        );
    }
    if ( elem.droppableId === "Status" ) {
        return (
            <td key={i}>
                <StatusIcons
                    status={{
                        mark: ( data.mark && data.mark.name ) || null,
                        infringementType:
              ( data.infringementType && data.infringementType.name ) || null,
                        enforcementStatus:
              ( data.enforcementStatus && data.enforcementStatus.name ) || null
                    }}
                    text={true}
                    tooltip={true}
                />
            </td>
        );
    }
    return <td key={i}>elem.label</td>;
};
