import React from "react";
import { storiesOf } from "@storybook/react";
import { TabBar } from "./TabBar";

storiesOf( "Tab Bar", module )
    .add( "normal state", () => (
        <TabBar>
            <button type="button">Button</button>
            <a>Link</a>
        </TabBar>
    ) )
    .add( "active state", () => (
        <TabBar>
            <button type="button" className="active">
        Button
            </button>
            <a>Link</a>
        </TabBar>
    ) );
