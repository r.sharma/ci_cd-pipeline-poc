import React from "react";
import { storiesOf } from "@storybook/react";
import { Footer } from "./";

const cardListStateFixture = {
    totalCount: 10,
    limit: 20,
    page: 1,
    results: [ 1, 2, 3, 4, 5 ]
};

storiesOf( "Social Protect Footer ", module ).add( "With pagination", () => (
    <Footer
        cardListState={{
            ...cardListStateFixture,
            limit: cardListStateFixture.totalCount / 2
        }}
    />
) );

storiesOf( "Social Protect Footer ", module ).add( "Without pagination", () => (
    <Footer cardListState={cardListStateFixture} />
) );
