import React from "react";
import { Dropdown } from "components";
import { ToggleView } from "pages/SocialProtect/components/PageRoute/components";
import { PostCounter } from "./PostCounter";
import "./FilterCustomize.scss";

export const FilterCustomize = ( {
    cardListState,
    activeView,
    pageViewChange,
    handleChangeLimitPage,
    activeFilterValueState,
    statusListState,
    statusListLoaderState,
    platformListLoaderState,
    platformListState,
    handleChangeFilter,
    activeModuleState
} ) => {
    const postPerPageList = [ "20", "50", "100", "200" ];
    if ( activeView !== "block" ) postPerPageList.push( "500" );
    return (
        <div className="filter-customize">
            <div className="filter-customize__filter">
                <Dropdown
                    data={statusListState}
                    defaultAll={true}
                    defaultValue={activeFilterValueState.enforcement || "All"}
                    wrapperClassName="filter-dropdown"
                    widthBtn="120px"
                    label="Status:"
                    handleChange={id => handleChangeFilter( "enforcement", id )}
                />
                <Dropdown
                    data={platformListState}
                    defaultAll={true}
                    dropHeight={"250px"}
                    defaultValue={activeFilterValueState.platform || "All"}
                    wrapperClassName="filter-dropdown"
                    widthBtn="120px"
                    label="Platform:"
                    handleChange={id => handleChangeFilter( "platform", id )}
                />
            </div>
            <div className="filter-customize__counter">
                <PostCounter
                    postCount={cardListState ? cardListState.totalCount : null}
                />
            </div>
            <div className="filter-customize__customize">
                <Dropdown
                    data={postPerPageList}
                    defaultValue={cardListState ? cardListState.limit : "10"}
                    widthBtn="60px"
                    wrapperClassName="filter-dropdown"
                    label={`${
                        activeView === "block"
                            ? activeModuleState === "posts"
                                ? "Posts"
                                : "Profiles"
                            : "Rows"
                    } per page:`}
                    handleChange={handleChangeLimitPage}
                />
                <ToggleView
                    mixed={false}
                    active={activeView}
                    pageViewChange={pageViewChange}
                />
            </div>
        </div>
    );
};
