import React from "react";
import { shallow } from "enzyme";
import { CompanySelect } from "./CompanySelect";

describe( "CompanySelect", () => {
    it( "renders 1 <CompanySelect /> component", () => {
        const component = shallow( <CompanySelect /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <CompanySelect /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check Avatar", () => {
        const component = shallow( <CompanySelect /> );

        const Avatar = component.find( "Avatar" );
        expect( Avatar ).toHaveLength( 1 );
    } );

    describe( "check menu-header", () => {
        const component = shallow( <CompanySelect /> );

        expect(
            component
                .find( ".menu-header" )
                .childAt( 0 )
                .type()
        ).toEqual( "svg" );
    } );

    describe( "check arrow", () => {
        const component = shallow( <CompanySelect /> );

        expect(
            component
                .find( ".company-select__box" )
                .childAt( 1 )
                .childAt( 0 )
                .type()
        ).toEqual( "svg" );
    } );
} );
