import React from "react";
import { storiesOf } from "@storybook/react";
import { RowLinkView } from "./RowLinkView";

storiesOf( "Row Link View", module ).add( "normal state", () => (
    <div style={{ margin: "100px" }}>
        <RowLinkView src="#" tooltip="tooltip" />
        <br />
        <RowLinkView src="#" label="label-tooltip" tooltip="123" />
        <br />
        <RowLinkView src="#" label="123" labelLink="123" channel={true} />
        <br />
        <RowLinkView
            src="#"
            label="123"
            iconLink="123"
            labelLink="123"
            channel={true}
        />
        <br />
        <RowLinkView
            src="#"
            label="123"
            iconLink="123"
            tooltip="tooltip"
            labelLink="123"
        />
    </div>
) );
