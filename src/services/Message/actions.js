import { createAction } from "redux-actions";

export const saveMessageBodyAction = createAction( "saveMessageBodyAction" );
export const showAndHideGlobalMessageAction = createAction( "showAndHideGlobalMessageAction" );
