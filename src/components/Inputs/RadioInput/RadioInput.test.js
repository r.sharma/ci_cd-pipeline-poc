import React from "react";
import { shallow } from "enzyme";
import { RadioInput } from "./RadioInput";

describe( "RadioInput", () => {
    it( "renders 1 <RadioInput /> component", () => {
        const component = shallow( <RadioInput /> );
        expect( component ).toHaveLength( 1 );
    } );

    // describe('check props in component', () => {
    //     const component = shallow(
    //         <RadioInput name="checkbox" label="Label-1" value={false} />,
    //     );
    //     const props = component.instance().props;
    //
    //     expect(props.name).toEqual('checkbox');
    //     expect(props.label).toEqual('Label-1');
    //     expect(props.value).toEqual(false);
    // });
} );
