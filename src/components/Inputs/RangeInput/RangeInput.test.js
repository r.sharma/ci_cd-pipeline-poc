import React from "react";
import { mount } from "enzyme";
import { RangeInput } from "./RangeInput";

describe( "RangeInput", () => {
    it( "renders 1 <RangeInput /> component", () => {
        const component = mount( <RangeInput /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const props = {
            min: 0,
            max: 100,
            minRange: 0,
            maxRange: 200
        };
        const component = mount( <RangeInput {...props} /> );

        expect( component.state().value ).toEqual( {
            min: props.min,
            max: props.max
        } );

        expect( component.find( "InputRange" ).props().maxValue ).toEqual(
            props.maxRange
        );
        expect( component.find( "InputRange" ).props().minValue ).toEqual(
            props.minRange
        );

        expect( component.find( "InputRange" ).props().value ).toEqual(
            component.state().value
        );
    } );
} );
