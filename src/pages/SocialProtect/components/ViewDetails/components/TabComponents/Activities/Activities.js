import React from "react";

export const Activities = () => {
    return (
        <div className="info-wrapper">
            <div className="tab-title">Activity history</div>
            <ul className="activity-history">
                <li>
                    <div className="date">8 days ago</div>
                    <div className="content-box">
                        <div>
                            <img
                                className="logo-img"
                                src="https://image.shutterstock.com/image-vector/cowboy-hog-pig-cartoon-bbq-450w-1094621420.jpg"
                                alt=""
                            />
                        </div>
                        <div>
                            <span>Benjamin Bell Changed status for</span>
                            <p>5 Tips For Offshore Software Development</p>
                        </div>
                    </div>
                </li>

                <li>
                    <div className="date">8 days ago</div>
                    <div className="content-box">
                        <div>
                            <img
                                className="logo-img"
                                src="https://image.shutterstock.com/image-vector/cowboy-hog-pig-cartoon-bbq-450w-1094621420.jpg"
                                alt=""
                            />
                        </div>
                        <div>
                            <span>Benjamin Bell Changed status for</span>
                            <p>5 Tips For Offshore Software Development</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    );
};
