import React from "react";
import { shallow } from "enzyme";
import { ProdSwitcher } from "./ProdSwitcher";
import { BASE_URL } from "config";

describe( "ProdSwitcher", () => {
    beforeEach( () => {
        jest.resetModules(); // Clears cache

        global.window = Object.create( window );
        Object.defineProperty( window, "location", {
            value: {
                href: `${BASE_URL}/social-protect/posts`,
                pathname: "/social-protect/posts",
                hostname: BASE_URL
            }
        } );
    } );

    afterEach( () => {
        delete window.location;
    } );

    it( "renders 1 <ProdSwitcher /> component", () => {
        const getProdPath = () => {};

        const component = shallow(
            <ProdSwitcher
                activeModuleState="posts"
                activeModule="social-protect"
                getProdPath={getProdPath}
            />
        );

        expect( component ).toHaveLength( 1 );
    } );
} );
