import React from "react";
import { storiesOf } from "@storybook/react";
import { Toggle } from "./Toggle";

storiesOf( "Toggle", module )
    .add( "normal state", () => <Toggle label="Label-1" /> )
    .add( "active state", () => <Toggle checked={true} label="Label-1" /> );
