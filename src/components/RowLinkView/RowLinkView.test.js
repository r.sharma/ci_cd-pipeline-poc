import React from "react";
import { shallow } from "enzyme";
import { RowLinkView } from "./RowLinkView";

describe( "RowLinkView", () => {
    it( "renders 1 <RowLinkView /> component", () => {
        const component = shallow( <RowLinkView /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <RowLinkView src="#" tooltip="tooltip" /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check props", () => {
        const props = {
            src: "#",
            tooltip: "tooltip",
            label: "label-tooltip",
            labelLink: "123"
        };
        const component = shallow( <RowLinkView {...props} /> );

        expect( component.props().className ).toEqual( "external-link" );
        expect( component.find( "Tooltip" ).props().title ).toEqual( "tooltip" );
        expect(
            component
                .find( ".label" )
                .find( "a" )
                .text()
        ).toEqual( "label-tooltip" );
        expect(
            component
                .find( ".label" )
                .find( "a" )
                .props().href
        ).toEqual( props.labelLink );
    } );
    describe( "check props 2", () => {
        const component = shallow(
            <RowLinkView src="#" tooltip="tooltip" label="label-tooltip" />
        );
        expect( component.props().className ).toEqual( "external-link" );
        expect( component.find( ".label" ).find( "a" ) ).toHaveLength( 0 );
        expect( component.find( ".label" ).text() ).toEqual( "label-tooltip" );
    } );
} );
