import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import { OverviewWorkloadContainer } from "./PostsOverviewContainer";
import { fakeDataProfile, fakeData } from "../../../fakeDataCardList";
import { brandsData } from "../../../brands_json";
import { listEnabledItem, columnsPosts } from "services/Post";
import { columnsProfiles } from "services/Profile";

const socialProtect = {
    profiles: {
        profile_list_loader: false,
        profile_list_data: fakeDataProfile.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    },
    posts: {
        card_list_loader: false,
        card_list_data: fakeData.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    }
};
const socialProtectLoader = {
    profiles: {
        profile_list_loader: false,
        profile_list_data: fakeDataProfile.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    },
    posts: {
        card_list_loader: true,
        card_list_data: fakeData.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    }
};

describe( "PostsOverviewDisplay", () => {
    describe( "check loader", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();

            // creates the store with any initial state or middleware needed
            store = mockStore( {
                match: { url: "/social-protect" },
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "row",
                        page_card_display_view: "block",
                        item_view_detail: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "row",
                        page_card_display_view: "x1",
                        item_view_detail: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                },
                socialProtect: socialProtectLoader,
                brands: brandsData.payload
            } );
        } );

        it( "Should render the component", () => {
            const component = shallow(
                <OverviewWorkloadContainer
                    store={store}
                    match={{ isExact: true, path: "", url: "/social-protect" }}
                />
            ).dive( {
                context: { store: store }
            } );
            expect( component ).not.toBe( null );

            //const props = component.instance().props;
            // expect(component.find('.loader')).toHaveLength(1);
        } );
    } );

    describe( "check props in component block view", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();

            // creates the store with any initial state or middleware needed
            store = mockStore( {
                match: { url: "/social-protect" },
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "row",
                        page_card_display_view: "block",
                        item_view_detail: null,
                        search_title: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "row",
                        page_card_display_view: "x1",
                        item_view_detail: null,
                        search_title: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                },
                socialProtect: socialProtect,
                brands: brandsData.payload
            } );
        } );

        it( "Should render the component", () => {
            const component = shallow(
                <OverviewWorkloadContainer
                    store={store}
                    match={{ isExact: true, path: "", url: "/social-protect" }}
                />
            ).dive( {
                context: { store: store }
            } );
            expect( component ).not.toBe( null );

            const props = component.instance().props;

            expect( component.find( "SideBar" ) ).toHaveLength( 1 );
            expect( component.find( "SideBar" ).props().sideBarIsOpen ).toEqual(
                props.sideBarIsOpen
            );
            expect( component.find( "SideBar" ).props().sideBarStateChange ).toEqual(
                props.sideBarStateChangeAction
            );

            expect( component.find( "OptionsTopBar" ) ).toHaveLength( 1 );
            expect(
                component.find( "OptionsTopBar" ).props().selectedCardsListState
            ).toEqual( props.selectedCardsListState );
            const cardListOnPageState =
        props.cardListState && props.cardListState.results;
            expect(
                component.find( "OptionsTopBar" ).props().cardListOnPageState
            ).toEqual( cardListOnPageState );
            expect(
                component.find( "OptionsTopBar" ).props().selectedAllCardsState
            ).toEqual( props.selectedAllCardsState );

            expect( component.find( "Header" ) ).toHaveLength( 1 );
            expect( component.find( "Header" ).props().brandsState ).toEqual(
                props.brandsState
            );
            expect( component.find( "Header" ).props().brandsFavoriteListState ).toEqual(
                props.brandsFavoriteListState
            );
            expect( component.find( "Header" ).props().activeBrandListState ).toEqual(
                props.activeBrandListState
            );

            expect( component.find( "ViewDetails" ) ).toHaveLength( 1 );
            expect( component.find( "ViewDetails" ).props().activeModuleState ).toEqual(
                props.activeModuleState
            );
            expect(
                component.find( "ViewDetails" ).props().viewDetailItemDataState
            ).toEqual( props.viewDetailItemDataState );

            expect( component.find( "Switch" ) ).toHaveLength( 1 );
            expect( component.find( "Switch" ).find( "Route" ) ).toHaveLength( 4 );

            const baseUrl =
        props.match.url[props.match.url.length - 1] === "/"
            ? props.match.url
            : props.match.url + "/";
            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 0 )
                    .props().exact
            ).toEqual( true );
            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 0 )
                    .props().path
            ).toEqual( baseUrl );

            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 1 )
                    .props().exact
            ).toEqual( undefined );
            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 1 )
                    .props().path
            ).toEqual( `${baseUrl}posts` );

            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 2 )
                    .props().exact
            ).toEqual( undefined );
            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 2 )
                    .props().path
            ).toEqual( `${baseUrl}profiles` );

            expect(
                component
                    .find( "Switch" )
                    .find( "Route" )
                    .at( 3 )
                    .props().exact
            ).toEqual( undefined );
        } );
    } );
} );
