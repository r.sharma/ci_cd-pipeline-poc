import React from "react";
import { storiesOf } from "@storybook/react";
import { TextInput } from "./TextInput";

storiesOf( "Text Input", module )
    .add( "normal state", () => (
        <TextInput inputClassName="test" placeholder={"placeholder"} />
    ) )
    .add( "error", () => (
        <TextInput
            placeholder={"placeholder"}
            meta={{
                touched: true,
                error: "Error message"
            }}
        />
    ) )
    .add( "success", () => (
        <TextInput
            placeholder={"placeholder"}
            meta={{
                success: true
            }}
        />
    ) )
    .add( "add icon", () => (
        <div>
            <TextInput
                placeholder={"placeholder"}
                childrenStyle={{
                    marginRight: "10px"
                }}
            >
                <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Instagram_simple_icon.svg/512px-Instagram_simple_icon.svg.png"
                    alt="icon"
                    style={{ width: "20px", height: "20px" }}
                />
            </TextInput>
            <TextInput
                placeholder={"placeholder"}
                wrapperStyle={{ flexDirection: "row-reverse" }}
                childrenStyle={{
                    marginRight: "10px"
                }}
            >
                <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Instagram_simple_icon.svg/512px-Instagram_simple_icon.svg.png"
                    alt="icon"
                    style={{ width: "20px", height: "20px" }}
                />
            </TextInput>
        </div>
    ) )
    .add( "with label", () => (
        <TextInput label="Label" placeholder={"placeholder"} />
    ) );
