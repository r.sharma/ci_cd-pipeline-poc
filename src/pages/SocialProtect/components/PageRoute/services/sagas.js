import { takeLatest, call, put, select } from "redux-saga/effects";
import { history } from "services/Navigation/history";

//Actions
import { userPushToRouteAction, removeChipsAction } from "./actions";
import { saveCardListAction } from "pages/SocialProtect/services/Card/actions";
import { searchByFilterInTimeAction } from "pages/SocialProtect/services/Options/actions";
import { activeFilterValueSelector } from "pages/SocialProtect/services/Options/selectors";

// Sagas
import { filterValuesInObjectSaveWorker } from "pages/SocialProtect/services/Options/sagas";

function* userPushToRouteWorker( { payload } ) {
    const { profileURL, module } = payload;
    if ( profileURL && module ) {
        yield put( saveCardListAction( {} ) );

        //save filter value
        yield call( filterValuesInObjectSaveWorker, {
            payload: {
                profileUrl: payload.profileURL,
                moduleState: module,
                postMark: null,
                page: 1,
                selectedPostMarkId: null,
                selectedInfringementTypeId: null,
                platform: null,
                enforcement: null,
                orderDirection: "-createdAt",
                profileName: null,
                profileId: null,
                listingId: null,
                batchUploadId: null,
                createdAtFrom: null,
                createdAtTo: null
            }
        } );
        //make api call by filter values from listen
        //push to post module
        history.push( "/social-protect/posts" );
    }
}

function* removeAllChipsWorker( { payload } ) {
    const activeFilterValueState = yield select( activeFilterValueSelector );
    const newFilter = { ...activeFilterValueState };
    if( payload ){
        newFilter[payload] = null;
    }else{
        for( let key in activeFilterValueState ){
            if( activeFilterValueState[key] && key !== "page" && key !== "limit" && key !== "orderDirection" ){
                newFilter[key] = null;
            }
        }
    }
    yield put( searchByFilterInTimeAction( newFilter ) );
}

export function* userPushToRouteWatcher() {
    yield takeLatest( userPushToRouteAction, userPushToRouteWorker );
}

export function* removeChipsWatcher() {
    yield takeLatest( removeChipsAction, removeAllChipsWorker );
}
