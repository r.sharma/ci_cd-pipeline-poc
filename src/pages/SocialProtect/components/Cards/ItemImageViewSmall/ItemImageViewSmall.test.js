import React from "react";
import { shallow } from "enzyme";
import { CardItemImageViewSmall } from "./ItemImageViewSmall";

describe( "CardItemImageView", () => {
    it( "renders 1 <CardItemImageViewSmall /> component", () => {
        const component = shallow( <CardItemImageViewSmall /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <CardItemImageViewSmall /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "CardItem status", () => {
        const props = {
            img: "http://cache.jpg",
            imgCardAlt: "imgCardAlt",
            titleHeader: "titleHeader",
            name: "name",
            date: "2019-04-08T07:32:33.278Z",
            text: "text",
            channel: "channel",
            posts: "0",
            views: "0",
            status: "channel",
            imgHeader: "imgHeader",
            selectedCardsList: [ "342" ],
            element: { id: "123" }
        };
        const component = shallow( <CardItemImageViewSmall {...props} /> );

        const title = component.find( "label" );
        expect( title ).toHaveLength( 1 );

        const StatusIcons = component.find( "StatusIcons" );
        expect( StatusIcons ).toHaveLength( 1 );
        expect( StatusIcons.props().status ).toEqual( props.status );

        const imgBox = component.find( ".img-box" );
        expect( imgBox ).toHaveLength( 1 );
        expect( imgBox.childAt( 0 ).props().src ).toEqual( props.img );
        expect( imgBox.childAt( 0 ).props().alt ).toEqual( props.imgCardAlt );

        const svgBox = component.find( ".svg-box" );
        expect( svgBox ).toHaveLength( 1 );
        expect( svgBox.childAt( 0 ).type() ).toEqual( "svg" );

        const input = component.find( "input" );
        expect( input ).toHaveLength( 1 );
        const checked = props.selectedCardsList.indexOf( props.element.id ) !== -1;
        expect( input.props().checked ).toEqual( checked );
    } );
} );
