import { connect } from "react-redux";
import { Header as component } from "./Header";
import {
    loadingBrandSelector,
    brandsSelector,
    brandsFavoriteListSelector,
    activeBrandSelector,
    isSwitchingBrandSelector
} from "services/Brand/selectors";
import { activeModuleStateSelector, activeModuleSelector } from "services/UserView/selectors";
import { addBrandToFavoritesAction, saveActiveBrandAction } from "services/Brand/actions";

const mapDispatchToProps = {
    addBrandToFavoritesAction,
    saveActiveBrandAction
};

const mapStateToProps = state => {
    const loadingBrandState = loadingBrandSelector( state );
    const brandsState = brandsSelector( state );
    const brandsFavoriteListState = brandsFavoriteListSelector( state );
    const activeBrandState = activeBrandSelector( state );
    const activeModuleState = activeModuleStateSelector( state );
    const activeModule = activeModuleSelector( state );
    const isSwitchingBrand = isSwitchingBrandSelector( state );

    return {
        brandsState,
        loadingBrandState,
        isSwitchingBrand,
        brandsFavoriteListState,
        activeBrandState,
        activeModuleState,
        activeModule
    };
};

export const Header = connect(
    mapStateToProps,
    mapDispatchToProps,
)( component );
