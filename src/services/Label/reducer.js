import { handleActions } from "redux-actions";
import { isLoadingLabelListAction, setPostLabelListAction, setProfileMarkListAction } from "./actions";

const initialState = {
    labelListIsLoading: false,
    postLabelList: null,
    profileLabelList: null
};

export const LabelReducer = handleActions(
    {
        [isLoadingLabelListAction]: ( state, { payload } ) => ( {
            ...state,
            labelListIsLoading: payload
        } ),
        [setPostLabelListAction]: ( state, { payload } ) => ( {
            ...state,
            postLabelList: payload
        } ),
        [setProfileMarkListAction]: ( state, { payload } ) => ( {
            ...state,
            profileLabelList: payload
        } )
    },
    initialState
);
