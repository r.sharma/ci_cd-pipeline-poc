import { handleActions } from "redux-actions";

import {
    selectEnforcementStatusInTopBarAction,
    selectInfringementTypeInTopBarAction
} from "pages/SocialProtect/services/Options/actions";
import { loadStartEnforcementAction, selectedPostMarkIdAction } from "pages/SocialProtect/services/Enforcement/actions";
import { loadCardListAction, loadMoreLoaderAction } from "pages/SocialProtect/services/Card/actions";
import { saveFilterValueAction } from "pages/SocialProtect/services/Options/actions";

const initialState = {
    loadMoreLoader: false,
    cardListLoader: false,
    startEnforcementLoader: false,
    selectedInfringementTypeId: null,
    selectedEnforcementStatusId: null,
    selectedPostMarkId: null,
    filterValue: {
        posts: {
            page: 1,
            limit: "20",
            platform: null,
            postMark: "0",
            enforcement: null,
            orderDirection: "-createdAt",
            profileName: null,
            profileId: null,
            profileUrl: null,
            listingId: null,
            batchUploadId: null,
            createdAtFrom: null,
            createdAtTo: null
        },
        profiles: {
            page: 1,
            limit: "20",
            platform: null,
            postMark: "0",
            enforcement: null,
            orderDirection: "-createdAt",
            profileName: null,
            profileId: null,
            listingId: null,
            batchUploadId: null,
            createdAtFrom: null,
            createdAtTo: null
        }
    }
};

export const socialProtectOptionsReducer = handleActions(
    {
        [selectInfringementTypeInTopBarAction]: ( state, { payload } ) => {
            return {
                ...state,
                selectedInfringementTypeId: payload
            };
        },
        [selectedPostMarkIdAction]: ( state, { payload } ) => {
            return {
                ...state,
                selectedPostMarkId: payload
            };
        },
        [selectEnforcementStatusInTopBarAction]: ( state, { payload } ) => {
            return {
                ...state,
                selectedEnforcementStatusId: payload
            };
        },
        [loadStartEnforcementAction]: ( state, { payload } ) => {
            return {
                ...state,
                startEnforcementLoader: payload
            };
        },
        [loadMoreLoaderAction]: ( state, { payload } ) => {
            return {
                ...state,
                loadMoreLoader: payload
            };
        },
        [loadCardListAction]: ( state, { payload } ) => {
            return {
                ...state,
                cardListLoader: payload
            };
        },
        [saveFilterValueAction]: ( state, { payload, payload: { activeModuleState } } ) => {
            const newData = { ...state.filterValue };
            newData[activeModuleState] = payload.payload;
            return {
                ...state,
                filterValue: newData
            };
        }
    },
    initialState,
);
