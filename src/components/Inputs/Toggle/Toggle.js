import React, { PureComponent } from "react";
import Switch from "@material-ui/core/Switch";
import "./Toggle.scss";

export class Toggle extends PureComponent {
    state = {
        checked: false
    };

    static getDerivedStateFromProps( nextProps ) {
        if ( Object.prototype.hasOwnProperty.call( nextProps, "checked" ) ) {
            return {
                checked: nextProps.checked
            };
        }
        return null;
    }

    onChange = event => {
        const { handleChange = val => {} } = this.props;
        this.setState(
            { checked: event.target.checked },
            handleChange( event.target )
        );
    };

    render() {
        let labelClassName = "label-toggle-default";
        const { label } = this.props;
        const { checked } = this.state;
        return (
            <div className={labelClassName}>
                <Switch onChange={this.onChange} checked={checked} />
                {label ? <div className="title">{label}</div> : null}
            </div>
        );
    }
}
