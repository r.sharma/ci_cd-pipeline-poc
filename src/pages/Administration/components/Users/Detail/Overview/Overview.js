import React from "react";
import "./Overview.scss";

import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";

import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { columnsUsers } from "services/User/models";

function renderEntry( entry, data, handleChange ) {
    const { title, field, readOnly } = entry;
    const value = data[field];

    if ( typeof value !== "boolean" ) {
        return (
            <TextField
                className="w-100"
                id={field}
                label={title}
                value={value}
                InputProps={{ readOnly: readOnly || false }}
                onChange={e => handleChange( field, e )}
                margin="normal"
                key={`detailField__${field}`}
            />
        );
    }

    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={value}
                    onChange={e => handleChange( field, e )}
                    color="primary"
                />
            }
            label={title}
        />
    );
}

export const UsersDetailOverview = props => {
    const { data, handleChange } = props;

    const headerFields = [ "userId", "client", "firstName", "lastName", "email" ];
    const headerColumns = columnsUsers.filter( column =>
        headerFields.includes( column.field )
    );
    const checkBoxes = columnsUsers.filter(
        column => !headerFields.includes( column.field )
    );

    return (
        <div className="container p-0">
            <div className="row">
                {headerColumns.map( entry => (
                    <div className="col-xs-12 col-md-6">
                        {renderEntry( entry, data, handleChange )}
                    </div>
                ) )}
            </div>
            <div className="row mt-5">
                <FormGroup row>
                    {checkBoxes.map( entry => (
                        <div className="col-xs-12 col-md-6">
                            {renderEntry( entry, data, handleChange )}
                        </div>
                    ) )}
                </FormGroup>
            </div>
        </div>
    );
};

UsersDetailOverview.propTypes = {
    data: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired
};
