import React from "react";
import "./Roles.scss";

import PropTypes from "prop-types";
import { Table } from "components";

const columnsUserRoles = [
    {
        title: "ID",
        field: "id",
        readOnly: true
    },
    {
        title: "Name",
        field: "name",
        readOnly: true
    }
];

export const UsersDetailRoles = props => {
    const { data } = props;

    return (
        <div className="container p-0">
            <div className="row">
                <div className="col">
                    <Table
                        hideBorder={true}
                        rows={data}
                        columns={columnsUserRoles}
                        //onEdit={this.onEdit}
                        onDelete={() => {}}
                        rowRepresents="roles"
                    />
                </div>
            </div>
        </div>
    );
};

UsersDetailRoles.propTypes = {
    data: PropTypes.array.isRequired
};
