import React from "react";
import { shallow } from "enzyme";
import { SpHeader } from "./Header";
import { brandsData } from "../../../../brands_json";

describe( "SpPage", () => {
    it( "renders 1 <Header /> component", () => {
        const component = shallow( <SpHeader /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const props = {
            title: "Social Protect",
            brandsState: brandsData,
            brandsFavoriteListState: [],
            activeBrandListState: null
        };

        const component = shallow( <SpHeader {...props} /> );
        expect( component.find( "CompanySelect" ) ).toHaveLength( 1 );
        expect( component.find( ".title" ) ).toHaveLength( 1 );
        expect( component.find( ".title" ).text() ).toEqual( props.title );
    } );
} );
