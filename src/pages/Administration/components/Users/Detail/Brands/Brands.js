import React from "react";
import "./Brands.scss";

import PropTypes from "prop-types";
import { Table } from "components";

const columnsUserBrands = [
    {
        title: "ID",
        field: "id",
        readOnly: true
    },
    {
        title: "Name",
        field: "name",
        readOnly: true
    },
    /*
    {
        title: 'Demo',
        field: 'isDemo',
    },
    {
        title: 'Remote',
        field: 'isRemote',
    },
    */
    {
        title: "Enabled",
        field: "isEnabled"
    }
];

export const UsersDetailBrands = props => {
    const { data } = props;

    return (
        <div className="container p-0">
            <div className="row">
                <div className="col">
                    <Table
                        hideBorder={true}
                        rows={data}
                        columns={columnsUserBrands}
                        //onEdit={this.onEdit}
                        onDelete={() => {}}
                        rowRepresents="brands"
                    />
                </div>
            </div>
        </div>
    );
};

UsersDetailBrands.propTypes = {
    data: PropTypes.array.isRequired
};
