import React from "react";
import moment from "moment";
import { dataFormat } from "config";
import { CardActions } from "../";
import {
    Checkbox,
    CollapsibleParagraph,
    SocialTitle,
    StatusIcons,
    maximizeCard,
    externalLinkCard
} from "components";
import "./Item.scss";

export const CardItem = ( {
    img = null,
    imgCardAlt = "",
    titleHeader = "",
    cardURL = null,
    name = "",
    date = "",
    text = "",
    profile = "",
    posts = "",
    views = "",
    status = "",
    children,
    imgHeader = null,
    selectedCardsList = [],
    chooseCardToSelect = () => {},
    deleteOneCardsAction = () => {},
    showViewDetailsPanelAction = () => {},
    userPushToRouteAction = () => {},
    element = { id: null }
} ) => {
    return (
        <div className="card-item">
            <div className="card-item-header">
                <Checkbox
                    handleChange={( { value } ) => {
                        chooseCardToSelect( { value: value, id: element.id } );
                    }}
                    defaultValue={selectedCardsList.indexOf( element.id ) !== -1}
                />
                <SocialTitle code={imgHeader} title={titleHeader} />
                <CardActions
                    id={element.id}
                    deleteOneCardsAction={deleteOneCardsAction}
                    editBtn={false}
                />
            </div>
            <div className="card-item-body">
                <div className="img-box">
                    {img ? <img src={img} alt={imgCardAlt} /> : null}
                    {children ? <div className="children-box">{children}</div> : null}
                    <div className="card-item-body-footer">
                        <StatusIcons status={status} text={true} type="card-status" />
                    </div>
                </div>
                <div className="description">
                    <div className="title-description-card">
                        <div>{name || "No title"}</div>
                        <span>{moment( date ).format( dataFormat )}</span>
                    </div>
                    <CollapsibleParagraph text={text} />
                    <div className="detail">
                        <div>
              Profile: <span>{profile}</span>
                        </div>
                        <div className="views-card">
                            <div>
                Posts: <span>{posts || 0}</span>
                            </div>
                            <div>
                Views: <span>{views || 0}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-item-footer">
                <div>
                    <a
                        href="/"
                        onClick={e => {
                            e.preventDefault();
                            if ( cardURL )
                                userPushToRouteAction( { profileURL: cardURL, module: "posts" } );
                        }}
                    >
                        {maximizeCard()}
                    </a>
                </div>
                <button
                    type={"button"}
                    onClick={() => showViewDetailsPanelAction( element.id )}
                >
          View details
                </button>
                <div>
                    {cardURL ? (
                        <a href={cardURL} rel="noopener noreferrer" target="_blank">
                            {externalLinkCard()}
                        </a>
                    ) : null}
                </div>
            </div>
        </div>
    );
};
