import React from "react";
import { storiesOf } from "@storybook/react";
import { StatusDropdown } from "./StatusDropdown";

storiesOf( "Status Dropdown", module ).add( "normal state", () => (
    <StatusDropdown
        wrapperClassName="status-dropdown"
        data={[ { id: 1, name: "example1" }, { id: 2, name: "example2" } ]}
    />
) );
