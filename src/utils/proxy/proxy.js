// Inject our overrides
require( "dotenv" ).config( { path: ".env.local" } );
const express = require( "express" );
const proxy = require( "http-proxy-middleware" );

const { REACT_APP_REVLECT_BASE_URL, HOST, PORT } = process.env;
const app = express();

// This configuration is for when we want to send responses through express itself.
app.use( ( req, res, next ) => {
    res.header( "Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT" );
    res.header( "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization" );
    res.header( "Access-Control-Allow-Origin", `http://${HOST}:${PORT}` );
    res.header( "Access-Control-Allow-Credentials", "true" );

    next();
} );

app.use(
    "/rest",
    proxy( {
        target: REACT_APP_REVLECT_BASE_URL,
        onProxyRes,
        onError,
        changeOrigin: true
    } )
);

function onProxyRes( proxyRes, req, res ) {
    // This is to fix an issue with Revlect's server configuration.
    proxyRes.headers["Access-Control-Allow-Origin"] = req.header( "origin" );

    /*
        If Revlect is rejecting us with a redirect on the basis of authorization,
        then reject the request so that the front-end can try to log back in.

        If we do not do this ourselves, we run into cors issues.
    */
    const location = proxyRes.headers.location;
    const isUnauthorized = location && location.includes( "login" );

    if ( isUnauthorized ) res.status( 401 ).end();
}

function onError( err, req, res ) {
    console.warn( err );

    res.writeHead( 500, {
        "Content-Type": "text/plain"
    } );
    res.end(
        "Something went wrong. And we are reporting a custom error message."
    );
}

app.listen( 4000 );
