/*
import * as API from 'api';

import {getRolesListAction, saveRolesListAction, setLoadingRolesList} from './actions';

import {call, put, select, takeLatest} from 'redux-saga/effects';

function* getRolesListWorker({payload}) {

    yield put(setLoadingRolesList(true));

    const activeBrand = yield select(activeBrandSelector);
    const response = yield call(API.getRolesList, payload);

    if (response) {
        yield put(saveRolesListAction({brand: activeBrand, ...response}));
    }

    yield put(setLoadingRolesList(false));

}

function* activeBrandSwitchedWorker( id ) {
    const activeModule = getModuleFromUrl();
    const activeModuleState = yield select( activeModuleStateSelector );

    if ( activeModule !== "admin" && activeModuleState !== "roles" ) return;

    put( getRolesListAction() )
}

// WATCHERS
export function* getRolesListWatcher() {
    yield takeLatest(getRolesListAction, getRolesListWorker);
}

export function* adminRolesActiveBrandSwitchedWatcher() {
    yield takeLatest(activeBrandSwitchedAction, activeBrandSwitchedWorker);
}
*/
