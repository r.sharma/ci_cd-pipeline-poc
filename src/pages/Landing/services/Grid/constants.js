import { serviceModule } from "pages/Landing/constants";

export const serviceModuleState = null;
export const serviceDefinition = {
    serviceModule,
    serviceModuleState
};