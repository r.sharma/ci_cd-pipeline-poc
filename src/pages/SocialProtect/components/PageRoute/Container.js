import { connect } from "react-redux";
import { PageRoute as component } from "./PageRoute";

import {
    filteredCardListDataSelector,
    selectedCardsList,
    selectedAllCardsStateSelector,
    cardListLoaderSelector
} from "pages/SocialProtect/services/Card/selectors";

import {
    chooseCardToSelectAction,
    chooseAllCardAction,
    deleteOneCardsAction,
    loadMoreAction,
    changeOrderDirectionAction
} from "pages/SocialProtect/services/Card/actions";
import {
    pageViewStateSelector,
    customizeTableViewSelector,
    pageCardDisplayViewSelector
} from "pages/SocialProtect/services/UserView/selectors";
import { pageListViewChangeAction } from "pages/SocialProtect/services/UserView/actions";
import {
    getLoadMoreLoaderSelector,
    activeFilterValueSelector
} from "pages/SocialProtect/services/Options/selectors";
import { searchByFilterInTimeAction } from "pages/SocialProtect/services/Options/actions";
import { userPushToRouteAction, removeChipsAction } from "pages/SocialProtect/components/PageRoute/services/actions";
import { activeBrandSelector } from "services/Brand/selectors";
import {
    platformListLoaderSelector,
    platformListSelector
} from "services/Platform/selectors";
import {
    statusListSelector,
    statusListLoaderSelector
} from "services/Status/selectors";
import {
    labelListSelector
} from "services/Label/selectors";
import { activeModuleStateSelector } from "services/UserView/selectors";
import { showViewDetailsPanelAction } from "services/UserView/actions";

const mapDispatchToProps = {
    chooseCardToSelectAction,
    chooseAllCardAction,
    deleteOneCardsAction,
    pageListViewChangeAction,
    showViewDetailsPanelAction,
    loadMoreAction,
    changeOrderDirectionAction,
    searchByFilterInTimeAction,
    userPushToRouteAction,
    removeChipsAction
};

const mapStateToProps = state => {
    const activeModuleState = activeModuleStateSelector( state ) || null;
    const cardListState = filteredCardListDataSelector( state ) || null;
    const pageViewState = pageViewStateSelector( state );
    const tableViewState = customizeTableViewSelector( state );
    const selectedCardsListState = selectedCardsList( state );
    const selectedAllCardsState = selectedAllCardsStateSelector( state );
    const pageCardDisplayViewState = pageCardDisplayViewSelector( state );
    const cardListLoaderState = cardListLoaderSelector( state ) || false;
    const getLoadMoreLoaderState = getLoadMoreLoaderSelector( state ) || false;
    const activeFilterValueState = activeFilterValueSelector( state ) || false;
    const platformListLoaderState = platformListLoaderSelector( state );
    const platformListState = platformListSelector( state );
    const statusListState = statusListSelector( state );
    const statusListLoaderState = statusListLoaderSelector( state );
    const activeBrandState = activeBrandSelector( state );
    const labelListState = labelListSelector( state );

    return {
        activeModuleState,
        cardListState,
        pageViewState,
        tableViewState,
        activeBrandState,
        selectedCardsListState,
        selectedAllCardsState,
        pageCardDisplayViewState,
        cardListLoaderState,
        getLoadMoreLoaderState,
        activeFilterValueState,
        platformListLoaderState,
        statusListLoaderState,
        statusListState,
        platformListState,
        labelListState
    };
};

export const PageRoute = connect(
    mapStateToProps,
    mapDispatchToProps,
)( component );
