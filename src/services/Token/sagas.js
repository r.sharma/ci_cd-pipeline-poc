import { takeLatest, call } from "redux-saga/effects";

import * as TokenAPI from "./api";
import { getNewToken } from "./actions";

//WORKERS
function* getNewTokenWorker() {
    yield call( TokenAPI.getNewToken );
}

// WATCHERS
export function* getNewTokenWatcher() {
    yield takeLatest( getNewToken, getNewTokenWorker );
}
