import React from "react";
import { storiesOf } from "@storybook/react";
import { Datepicker } from "./Datepicker";

storiesOf( "Datepicker", module ).add( "normal state", () => (
    <div className="row">
        <div className="col-xs-12 col-md-12 example-col">
            <p>Controlled DatePicker</p>
            <Datepicker value={null} handleChange={null} />
        </div>
    </div>
) );
