import { connect } from "react-redux";
import { PageRoute as component } from "./PageRoute";
import { showViewDetailsPanelAction } from "services/UserView/actions";
import { usersListSelector } from "pages/Administration/services/User/selectors";
import { getUsersListAction } from "pages/Administration/services/User/actions";
import { rolesListSelector } from "pages/Administration/services/Role/selectors";
import { getRolesListAction } from "pages/Administration/services/Role/actions";
import { activeModuleStateSelector } from "services/UserView/selectors";

const mapDispatchToProps = {
    getUsersListAction,
    getRolesListAction,
    showViewDetailsPanelAction
};

const mapStateToProps = state => {
    const activeModuleState = activeModuleStateSelector( state ) || null;
    const users = usersListSelector( state ) || null;
    const roles = rolesListSelector( state ) || null;

    return {
        activeModuleState,
        users,
        roles
    };
};

export const PageRoute = connect(
    mapStateToProps,
    mapDispatchToProps,
)( component );
