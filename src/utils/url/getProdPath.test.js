import { BASE_URL } from "config";
import { getProdPath } from "./getProdPath";

describe( "getProdPath returns the correct urls", () => {
    const OLD_ENV = process.env;

    beforeEach( () => {
        jest.resetModules(); // Clears cache
        process.env = { BASE_URL, ...OLD_ENV };
        delete process.env.NODE_ENV;
    } );

    afterEach( () => {
        process.env = OLD_ENV;
    } );

    it( "Properly returns the social-protect profiles url", () => {
        const url = getProdPath( "social-protect", "profiles" );

        expect( url ).toEqual(
            `${BASE_URL}/socialmedia/generic/storage/#/enforcement/profile`
        );
    } );

    it( "Properly returns the social-protect posts url", () => {
        const url = getProdPath( "social-protect", "posts" );

        expect( url ).toEqual(
            `${BASE_URL}/socialmedia/generic/storage/#/enforcement/post`
        );
    } );

    it( "Properly returns the market-protect listings url", () => {
        const url = getProdPath( "market-protect", "listings" );

        expect( url ).toEqual( `${BASE_URL}/enforcement` );
    } );

    it( "Properly returns the market-protect seller url", () => {
        const url = getProdPath( "market-protect", "sellers" );

        expect( url ).toEqual( `${BASE_URL}/seller` );
    } );

    it( "Properly returns the domain-protect websites url", () => {
        const url = getProdPath( "domain-protect", "websites" );

        expect( url ).toEqual( `${BASE_URL}/domains/website` );
    } );

    it( "Properly returns the app-protect url", () => {
        const url = getProdPath( "app-protect", "apps" );
        expect( url ).toEqual(
            `${BASE_URL}/socialmedia/app-generic/storage/#/enforcement/app`
        );
    } );
    it( "Properly returns the admin users url", () => {
        const url = getProdPath( "admin", "users" );
        expect( url ).toEqual( `${BASE_URL}/users` );
    } );
} );
