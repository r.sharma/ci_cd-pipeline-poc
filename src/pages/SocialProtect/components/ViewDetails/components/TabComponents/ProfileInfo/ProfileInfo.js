import React from "react";
import { urlUtil } from "utils";
import { RowLinkView } from "components";
const { extractHostname } = urlUtil;

export const ProfileInfo = ( { data } ) => {
    const connectedTo = data.connectedTo || null;

    return (
        <div className="info-wrapper">
            <div className="tab-title">Profile information</div>
            <ul className="profile-info">
                <li>
                    <span>Views:</span>{" "}
                    {data.profileViews ||
            ( data.profile && data.profile.profileViews ) ||
            "0"}
                </li>
                <li>
                    <span>Likes:</span>{" "}
                    {data.profileLikes ||
            ( data.profile && data.profile.profileLikes ) ||
            "0"}
                </li>
                <li>
                    <span>Posts:</span>
                    {data.profilePosts ||
            ( data.profile && data.profile.profilePosts ) ||
            "0"}
                </li>
                <li>
                    <span>Followers:</span>{" "}
                    {data.profileFollowers ||
            ( data.profile && data.profile.profileFollowers ) ||
            "0"}
                </li>
            </ul>
            <ul className="list-info">
                <li>
                    <span className="list-title">Profile picture</span>
                    <span>
                        <img
                            src={
                                data.profileImage || ( data.profile && data.profile.profileImage )
                            }
                            alt=""
                        />
                    </span>
                </li>
                <li>
                    <span className="list-title">Profile name</span>
                    <span>
                        {data.profileName ||
              ( data.profile && data.profile.profileName ) ||
              "No Data"}
                    </span>
                </li>
                <li>
                    <span className="list-title">Profile connected to</span>
                    <span>
                        {connectedTo ? (
                            <RowLinkView
                                label={extractHostname( connectedTo )}
                                iconLink={connectedTo}
                                targetBlank={true}
                                handleClickIcon={() => {
                                    window.open( connectedTo, "_blank" );
                                }}
                            />
                        ) : (
                            "No Data"
                        )}
                    </span>
                </li>
                <li>
                    <span className="list-title">Profile submitted by</span>
                    <span>
                        {data.submittedBy ||
              ( data.profile && data.profile.submittedBy ) ||
              "No Data"}
                    </span>
                </li>
                <li>
                    <span className="list-title">Country</span>
                    <span>
                        {data.countryName ||
              ( data.profile && data.profile.countryName ) ||
              "No Data"}
                    </span>
                </li>
                <li>
                    <span className="list-title">Profile URL</span>
                    <span>
                        {data.profileURL || ( data.profile && data.profile.profileURL ) ? (
                            <RowLinkView
                                label={
                                    data.profileURL || ( data.profile && data.platform.profileURL )
                                }
                                iconLink={
                                    data.profileURL || ( data.profile && data.profile.profileURL )
                                }
                                targetBlank={true}
                                handleClickIcon={() => {
                                    window.open(
                                        data.profileURL ||
                      ( data.profile && data.profile.profileURL ),
                                        "_blank"
                                    );
                                }}
                            />
                        ) : (
                            "No Data"
                        )}
                    </span>
                </li>
                <li>
                    <span className="list-title">Profile ID</span>
                    <span>
                        {data.profileID ||
              ( data.profile && data.profile.profileID ) ||
              "No Data"}
                    </span>
                </li>
            </ul>
        </div>
    );
};
