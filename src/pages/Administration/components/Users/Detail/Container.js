import { connect } from "react-redux";
import { UsersDetail as component } from "./Detail";

const mapDispatchToProps = {};

const mapStateToProps = state => {
    return {};
};

export const UsersDetail = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
