import React from "react";
import { shallow, mount } from "enzyme";
import { StatusDropdown } from "./StatusDropdown";
import { statusVariables } from "services/Status/fixtures";

describe( "StatusDropdown", () => {
    it( "renders 1 <StatusDropdown /> component", () => {
        const component = shallow( <StatusDropdown /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = mount(
            <StatusDropdown wrapperClassName="status-dropdown" />
        );
        const props = component.instance().props;
        expect( props.wrapperClassName ).toEqual( "status-dropdown" );

        expect( component.children().length ).toEqual( 1 );
        it( "chack component", () => {
            expect( component.childAt( 0 ).hasClass( "dropdown status-dropdown" ) ).toEqual(
                true
            );
            expect( component.state().open ).toEqual( false );
            expect( component.state().selected ).toEqual( null );
            expect(
                component
                    .find( ".btn-status" )
                    .children()
                    .type()
            ).toBe( "svg" );
            component.find( ".wrapper-button" ).simulate( "click" );
            expect( component.find( ".wrapper-button" ).hasClass( "show" ) ).toBeTruthy();
            expect( component.find( ".dropdown-list" ) ).toHaveLength( 1 );
            component
                .find( ".dropdown-list" )
                .find( "li" )
                .at( 0 )
                .simulate( "click" );

            expect( component.state().open ).toEqual( false );
            expect( component.state().selected ).not.toEqual( null );
            expect( component.state().selected ).toEqual( statusVariables[0] );

            component.find( ".wrapper-button" ).simulate( "click" );
            expect( component.state().open ).toEqual( true );

            const outerNode = document.createElement( "div" );
            outerNode.className = "outerDiv";
            document.body.appendChild( outerNode );

            outerNode.dispatchEvent( new Event( "click", { bubbles: true } ) );

            expect( component.state().open ).toEqual( false );
        } );
    } );
} );
