import React from "react";
import "../InputStyle.scss";

export const RenderFieldTextArea = ( {
    input,
    label,
    placeholder,
    id,
    heightArea = "120px",
    keyPress = () => {},
    meta: { touched, error, warning }
} ) => (
    <label
        className={
            touched && error ? "block-textarea error_border" : "block-textarea"
        }
    >
        {label ? <span className="title">{label}</span> : null}
        <textarea
            {...input}
            onKeyPress={keyPress}
            placeholder={placeholder}
            id={id}
            style={{ height: heightArea }}
            autoComplete="off"
        />
        {touched && error ? <span className="error">{error}</span> : null}
    </label>
);
