import {
    saveUsersDetailIdentifier,
    saveUsersDetailItem,
    saveUsersList,
    setLoadingUsersList
} from "./actions";

import { handleActions } from "redux-actions";

const initialState = {
    data: null,
    item_view_detail_value: null,
    item_view_detail_key: null,
    isLoadingUsers: false,
    detail: null
};

export const adminUserReducer = handleActions(
    {
        [saveUsersDetailIdentifier]: ( state, { payload } ) => {
            return {
                ...state,
                item_view_detail_value: payload ? payload.value : null,
                item_view_detail_key: payload ? payload.key : null
            };
        },

        [saveUsersDetailItem]: ( state, { payload } ) => {
            return {
                ...state,
                detail: payload ? payload.data : null
            };
        },

        [saveUsersList]: ( state, { payload } ) => {
            return {
                ...state,
                data: payload.data
            };
        },

        [setLoadingUsersList]: ( state, { payload } ) => {
            return {
                ...state,
                isLoadingUsers: payload
            };
        }
    },
    initialState
);
