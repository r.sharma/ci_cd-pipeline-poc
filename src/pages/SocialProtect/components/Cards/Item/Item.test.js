import React from "react";
import { shallow } from "enzyme";
import { CardItem } from "./Item";

describe( "CardItem", () => {
    it( "renders 1 <CardItem /> component", () => {
        const component = shallow( <CardItem /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <CardItem /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "CardItem img", () => {
        const component = shallow( <CardItem img={"http://cache.jpg"} /> );

        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
        ).toHaveLength( 1 );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().src
        ).toEqual( "http://cache.jpg" );
    } );

    describe( "CardItem img 2", () => {
        const component = shallow(
            <CardItem img={"http://cache.jpg"} imgCardAlt="test" />
        );

        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
        ).toHaveLength( 1 );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().src
        ).toEqual( "http://cache.jpg" );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().alt
        ).toEqual( "test" );
    } );

    describe( "CardItem imgHeader", () => {
        const props = "test";
        const component = shallow( <CardItem imgHeader={props} /> );

        const SocialTitle = component.find( "SocialTitle" );
        expect( SocialTitle ).toHaveLength( 1 );
        expect( SocialTitle.props().code ).toEqual( props );
    } );

    describe( "CardItem titleHeader", () => {
        const props = "Instagram";
        const component = shallow( <CardItem titleHeader={props} /> );

        const SocialTitle = component.find( "SocialTitle" );
        expect( SocialTitle ).toHaveLength( 1 );
        expect( SocialTitle.props().title ).toEqual( props );
    } );

    describe( "CardItem name", () => {
        const props = "Tremblant In Canada";
        const component = shallow( <CardItem name={props} /> );

        const title = component
            .find( ".title-description-card" )
            .children()
            .find( "div" );

        expect( title ).toHaveLength( 1 );
        expect( title.text() ).toEqual( props );
    } );

    describe( "CardItem date", () => {
        const props = "2019-04-08T07:32:33.278Z";
        const component = shallow( <CardItem date={props} /> );

        const title = component
            .find( ".title-description-card" )
            .children()
            .find( "span" );

        jest.mock( "moment", () => () => ( { format: () => "08 Apr 2019" } ) );

        expect( title ).toHaveLength( 1 );
        expect( title.text() ).toEqual( "08 Apr 2019" );
    } );

    describe( "CardItem text", () => {
        const props = "Can I buy a replica Nike Cal rugby jersey online?";
        const component = shallow( <CardItem text={props} /> );

        const CollapsibleParagraph = component.find( "CollapsibleParagraph" );
        expect( CollapsibleParagraph ).toHaveLength( 1 );
        expect( CollapsibleParagraph.props().text ).toEqual( props );
    } );

    describe( "CardItem channel", () => {
        const props = "Fátima Cambeiro";
        const component = shallow( <CardItem profile={props} /> );

        const wrapper = component
            .find( ".detail" )
            .children()
            .find( "div" )
            .first();
        expect( wrapper ).toHaveLength( 1 );
        expect( wrapper.find( "span" ).text() ).toEqual( props );
    } );

    describe( "CardItem Posts", () => {
        const props = "23";
        const component = shallow( <CardItem posts={props} /> );

        const wrapper = component
            .find( ".views-card" )
            .children()
            .find( "div" )
            .first();
        expect( wrapper ).toHaveLength( 1 );
        expect( wrapper.find( "span" ).text() ).toEqual( props );
    } );

    describe( "CardItem Posts 0", () => {
        const component = shallow( <CardItem /> );

        const wrapper = component
            .find( ".views-card" )
            .children()
            .find( "div" )
            .first();
        expect( wrapper ).toHaveLength( 1 );
        expect( wrapper.find( "span" ).text() ).toEqual( "0" );
    } );

    describe( "CardItem views", () => {
        const props = "23";
        const component = shallow( <CardItem views={props} /> );

        const wrapper = component
            .find( ".views-card" )
            .children()
            .find( "div" )
            .at( 1 );
        expect( wrapper ).toHaveLength( 1 );
        expect( wrapper.find( "span" ).text() ).toEqual( props );
    } );

    describe( "CardItem views 0", () => {
        const component = shallow( <CardItem /> );

        const wrapper = component
            .find( ".views-card" )
            .children()
            .find( "div" )
            .at( 1 );
        expect( wrapper ).toHaveLength( 1 );
        expect( wrapper.find( "span" ).text() ).toEqual( "0" );
    } );

    describe( "CardItem status", () => {
        const props = {
            img: null,
            imgCardAlt: "",
            titleHeader: "titleHeader",
            name: "",
            date: "",
            text: "",
            channel: "",
            posts: "",
            views: "",
            status: "",
            imgHeader: "imgHeader",
            selectedCardsList: [ "342" ],
            element: { id: "123" }
        };
        const component = shallow( <CardItem {...props} /> );

        const StatusIcons = component.find( "Checkbox" );
        const defaultValue =
      props.selectedCardsList.indexOf( props.element.id ) !== -1;
        expect( StatusIcons.props().defaultValue ).toEqual( defaultValue );

        const CardActions = component.find( "CardActions" );
        expect( CardActions ).toHaveLength( 1 );
        expect( CardActions.props().id ).toEqual( props.element.id );

        const SocialTitle = component.find( "SocialTitle" );
        expect( SocialTitle ).toHaveLength( 1 );
        expect( SocialTitle.props().code ).toEqual( props.imgHeader );
        expect( SocialTitle.props().title ).toEqual( props.titleHeader );
    } );
} );
