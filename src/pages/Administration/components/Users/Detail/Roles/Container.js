import { connect } from "react-redux";
import { UsersDetailRoles as component } from "./Roles";

const mapDispatchToProps = {};

const mapStateToProps = state => {
    return {};
};

export const UsersDetailRoles = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
