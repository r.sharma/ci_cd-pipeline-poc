import React from "react";
import "./Detail.scss";
import PropTypes from "prop-types";
import colors from "styles/variables.scss";

import { ButtonClose, Snackbar } from "components";
import Button from "@material-ui/core/Button";

import { UsersDetailBrands } from "./Brands";
import { UsersDetailOverview } from "./Overview";
import { UsersDetailRoles } from "./Roles";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const getDefaultSnackbarState = variant => ( {
    message: "",
    variant,
    isOpen: false
} );

export class UsersDetail extends React.Component {
    initialState = {
        snackbars: {
            info: getDefaultSnackbarState( "info" ),
            success: getDefaultSnackbarState( "success" ),
            warning: getDefaultSnackbarState( "warning" ),
            error: getDefaultSnackbarState( "error" )
        }
    };

    state = this.initialState;

    /*
    handleChange = (field, e) => {
        const data = e.target.value;

        e.preventDefault();
    };

    onDelete = data => {
        console.log(data);
    };
    */

    renderSnackbars = () => {
        const variants = [ "info", "success", "warning", "error" ];

        return (
            <div className="flexed-bottom-anchor">
                {variants.map( ( variant, index ) => {
                    const isOpen = this.state.snackbars[variant].isOpen;
                    const message = this.state.snackbars[variant].message;

                    return (
                        <Snackbar
                            variant={variant}
                            message={message}
                            isOpen={isOpen}
                            handleClose={this.closeSnackbar}
                        />
                    );
                } )}
            </div>
        );
    };

    openSnackbar = ( variant, message ) => {
        this.setState( state => ( {
            ...state,
            snackbars: {
                ...state.snackbars,
                [variant]: { variant, message, isOpen: true }
            }
        } ) );
    };

    closeSnackbar = variant => {
        this.setState( state => ( {
            ...state,
            snackbars: {
                ...state.snackbars,
                [variant]: getDefaultSnackbarState( variant )
            }
        } ) );
    };

    saveDetails = () => {
        this.openSnackbar( "error", "Test - No connection with the server!" );
        this.openSnackbar( "warning", "Test - No connection with the server!" );
    };

    handleChange = ( field, e ) => {};

    render() {
        const {
            userOverview,
            userBrands,
            userRoles,
            className,
            closePanel
        } = this.props;

        return (
            <div className={className}>
                <div className="mb-5">
                    <ButtonClose onClick={closePanel} color="#979797" />

                    <Button
                        onClick={this.saveDetails}
                        className="float-right"
                        variant="contained"
                        color="primary"
                    >
                        Save
                    </Button>
                </div>

                <div>
                    <ExpansionPanel defaultExpanded={true}>
                        <ExpansionPanelSummary
                            style={{ background: colors.primaryLighter }}
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panelOverview-content"
                            id="panelOverview-header"
                        >
                            <h5>Overview</h5>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <UsersDetailOverview
                                data={userOverview}
                                handleChange={this.handleChange}
                            />
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel defaultExpanded={true}>
                        <ExpansionPanelSummary
                            style={{ background: colors.primaryLight }}
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panelBrands-content"
                            id="panelBrands-header"
                        >
                            <h5>Brands</h5>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <UsersDetailBrands data={userBrands} />
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel defaultExpanded={true}>
                        <ExpansionPanelSummary
                            style={{ background: colors.primaryLighter }}
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panelRoles-content"
                            id="panelRoles-header"
                        >
                            <h5>Roles</h5>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <UsersDetailRoles data={userRoles} />
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>

                {this.renderSnackbars()}
            </div>
        );
    }
}

UsersDetail.propTypes = {
    userOverview: PropTypes.object.isRequired,
    userRoles: PropTypes.array.isRequired,
    userBrands: PropTypes.array.isRequired,
    closePanel: PropTypes.func.isRequired,
    className: PropTypes.string
};
