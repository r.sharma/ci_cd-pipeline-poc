import React from "react";
import { shallow } from "enzyme";
import { Tooltip } from "./Tooltip";

describe( "Tooltip", () => {
    it( "renders 1 <Tooltip /> component", () => {
        const component = shallow( <Tooltip /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper Tooltip", () => {
        const props = {
            title: "title tooltip"
        };
        const component = shallow(
            <Tooltip {...props}>
                <div>test</div>
            </Tooltip>
        );
        const instance = component.instance();

        expect( instance ).toEqual( null );
        expect( component.props().className ).toEqual( "tooltip-box" );
        expect( component.find( ".tooltip-body" ).text() ).toEqual( props.title );

        expect( component.find( ".tooltip-box" ).childAt( 1 ) ).toHaveLength( 1 );
        expect(
            component
                .find( ".tooltip-box" )
                .childAt( 1 )
                .text()
        ).toEqual( "test" );
    } );
} );
