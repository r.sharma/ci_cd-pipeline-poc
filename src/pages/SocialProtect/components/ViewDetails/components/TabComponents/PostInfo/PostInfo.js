import React from "react";
import { urlUtil } from "utils";
import { RowLinkView } from "components";
const { extractHostname } = urlUtil;

export const PostInfo = ( { data } ) => {
    return (
        <div className="info-wrapper">
            <div className="tab-title">Post information</div>
            <ul className="profile-info">
                <li>
                    <span>Views:</span> {data.listingViews || "0"}
                </li>
                <li>
                    <span>Likes:</span> {data.listingLikes || "0"}
                </li>
                <li>
                    <span>Shares:</span> {data.listingShares || "0"}
                </li>
                <li>
                    <span>Followers:</span> {data.listingFollowers || "0"}
                </li>
            </ul>
            <ul className="list-info">
                <li>
                    <span className="list-title">Platform</span>
                    <span>{data.platform.name || "No Data"}</span>
                </li>
                <li>
                    <span className="list-title">Country</span>
                    <span>{data.countryName || "No Data"}</span>
                </li>
                <li>
                    <span className="list-title">Post URL</span>
                    <span>
                        {data.listingURL ? (
                            <RowLinkView
                                label={data.platform.name}
                                iconLink={data.listingURL}
                                targetBlank={true}
                                handleClickIcon={() => {
                                    window.open( data.listingURL, "_blank" );
                                }}
                            />
                        ) : (
                            "No Data"
                        )}
                    </span>
                </li>
                <li>
                    <span className="list-title">Post ID</span>
                    <span>{data.id || "No Data"}</span>
                </li>
                <li>
                    <span className="list-title">Evidence URL</span>
                    <span>
                        {data.evidenceURL ? (
                            <RowLinkView
                                label={data.evidenceURL}
                                iconLink={data.evidenceURL}
                                targetBlank={true}
                                handleClickIcon={() => {
                                    window.open( data.evidenceURL, "_blank" );
                                }}
                            />
                        ) : (
                            "No Data"
                        )}
                    </span>
                </li>
                <li>
                    <span className="list-title">Submitted by</span>
                    <span>{data.submittedBy || "No Data"}</span>
                </li>
                <li>
                    <span className="list-title">Connected to</span>
                    <span>
                        {data.connectedTo ? (
                            <RowLinkView
                                label={extractHostname( data.connectedTo )}
                                iconLink={data.connectedTo}
                                targetBlank={true}
                                handleClickIcon={() => {
                                    window.open( data.connectedTo, "_blank" );
                                }}
                            />
                        ) : (
                            "No Data"
                        )}
                    </span>
                </li>
                <li>
                    <span className="list-title">Batch upload ID</span>
                    <span>{data.batchUploadID || "No Data"}</span>
                </li>
            </ul>
        </div>
    );
};
