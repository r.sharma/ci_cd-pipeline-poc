import { createAction } from "redux-actions";

export const userPushToRouteAction = createAction( "userPushToRouteAction" );
export const removeChipsAction = createAction( "removeChipsAction" );
