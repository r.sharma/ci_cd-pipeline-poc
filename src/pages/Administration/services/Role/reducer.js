import {
    itemViewDetailsRolesSaveAction,
    saveRolesListAction,
    setLoadingRolesList
} from "./actions";
import { handleActions } from "redux-actions";

const initialState = {
    data: null,
    item_view_detail_value: null,
    item_view_detail_key: null,
    isLoadingRoles: false
};

export const adminRoleReducer = handleActions(
    {
        [itemViewDetailsRolesSaveAction]: ( state, { payload } ) => {
            return {
                ...state,
                item_view_detail_value: payload ? payload.value : null,
                item_view_detail_key: payload ? payload.key : null
            };
        },

        [saveRolesListAction]: ( state, { payload } ) => {
            return {
                ...state,
                data: payload.data
            };
        },

        [setLoadingRolesList]: ( state, { payload } ) => {
            return {
                ...state,
                isLoadingRoles: payload
            };
        }
    },
    initialState
);
