import React from "react";
import "./TabBar.scss";

export const TabBar = ( { children } ) => {
    return <div className="tab-bar-box">{children}</div>;
};
