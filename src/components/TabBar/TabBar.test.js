import React from "react";
import { shallow } from "enzyme";
import { TabBar } from "./TabBar";

describe( "TabBar", () => {
    it( "renders 1 <TabBar /> component", () => {
        const component = shallow( <TabBar /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const children = <div>test</div>;
        const component = shallow( <TabBar>{children}</TabBar> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual( "tab-bar-box" );
        expect( component.children.length ).toEqual( 1 );
        expect(
            component
                .children()
                .at( 0 )
                .type()
        ).toEqual( "div" );
        expect( component.children().text() ).toBe( "test" );
    } );
} );
