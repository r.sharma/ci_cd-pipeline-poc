import React from "react";
import { storiesOf } from "@storybook/react";
import { ToggleView } from "./ToggleView";

storiesOf( "Toggle view", module ).add( "normal state", () => (
    <ToggleView label="Customize:" active="web" />
) );
