import React from "react";
import { shallow } from "enzyme";
import { SpSideBar } from "./SideBar";

describe( "SideBar", () => {
    it( "renders 1 <SideBar /> component", () => {
        const component = shallow( <SpSideBar /> );
        expect( component ).toHaveLength( 1 );
    } );

    // describe('shallow wrapper instance should be null', () => {
    //     const component = shallow(<SideBar size="35" />);
    //     const instance = component.instance();
    //
    //     expect(instance).toEqual(null);
    // });

    describe( "check closed component", () => {
    /*         const props = {
            sideBarStateChange : jest.fn(),
            sideBarIsOpen : false,
        };
        const component = shallow( <SpSideBar {...props} /> );

        expect(
            component.childAt(0).hasClass('side-bar side-bar-flex'),
        ).toBeTruthy();
        const wrapper = component
            .childAt(0)
            .find('div')
            .at(0);
        expect(
            wrapper
                .childAt(0)
                .find('ul')
                .hasClass('side-bar-list-icon'),
        ).toBeTruthy();
        expect(
            wrapper
                .childAt(0)
                .find('ul')
                .children(),
        ).toHaveLength(4);
        expect(
            wrapper
                .childAt(0)
                .find('ul')
                .childAt(0)
                .type(),
        ).toEqual('li');
        wrapper
            .childAt(0)
            .find('ul')
            .childAt(0)
            .simulate('click');
        expect(props.sideBarStateChange.mock.calls.length).toBe(1);

        const wrapper2 = component
            .childAt(0)
            .find('div')
            .at(2);
        expect(wrapper2.find('.icon-wrapper-circle')).toHaveLength(1);

        expect(component.find('.icon-wrapper-circle')).toHaveLength(5);
        component.find('.icon-wrapper-circle').forEach(e => {
            expect(e.children().type()).toEqual('svg');
        }); */
    } );
} );
