import React from "react";
import "./ViewDetails.scss";

export const ViewDetailsWrapper = ( props ) => {
    return (
        <div ref={props.myRef} className="view-details-panel scroll-m">
            {props.children}
        </div>
    );
};