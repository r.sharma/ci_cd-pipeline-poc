import { takeLatest, put, select, call } from "redux-saga/effects";

import { getLabelListAction } from "services/Label/actions";

import {
    sideBarStateChangeAction,
    sideBarStateSaveAction,
    setActiveModuleStateAction,
    setActiveModuleAction,
    storeActiveModuleStateAction,
    storeActiveModuleAction
} from "./actions";
import { checkFieldAndSave } from "utils";

import {
    activeModuleSelector,
    activeModuleStateSelector,
    sideBarStateSelector
} from "./selectors";


// Workers
// Wraps actions and prevents non-active (sub)modules from triggering from shared actions
export function* activeServiceValidationWrapper( { worker, serviceModule, serviceModuleState }, payload  ) {
    const isActive = yield call( serviceIsActive, serviceModule, serviceModuleState );

    if ( isActive ) yield call( worker, payload );
}

function* serviceIsActive( expectedModule, expectedModuleState ) {
    const activeModule = yield select( activeModuleSelector );
    const activeModuleState = yield select( activeModuleStateSelector );

    if (
        activeModule === expectedModule &&
        ( !expectedModuleState || activeModuleState === expectedModuleState )
    ) {
        return true;
    }

    return false;
}

function* sideBarStateChangeWorker() {
    const sideBarCurrentState = yield select( sideBarStateSelector );
    yield put( sideBarStateSaveAction( !sideBarCurrentState ) );
    checkFieldAndSave( "side_bar_open", !sideBarCurrentState, "sideBar" );
}

function* setActiveModuleStateWorker( { payload: activeModuleState } ) {
    yield put( storeActiveModuleStateAction( activeModuleState ) );
    //make a call to get Labels by current activeModuleState
    if ( activeModuleState ) yield put( getLabelListAction( activeModuleState ) );
}

function* setActiveModuleWorker( { payload: activeModule } ) {
    yield put( storeActiveModuleAction( activeModule ) );
}


// WATCHERS
export function* sideBarStateChangeWatcher() {
    yield takeLatest( sideBarStateChangeAction, sideBarStateChangeWorker );
}

export function* setActiveModuleStateUserViewWatcher() {
    yield takeLatest( setActiveModuleStateAction, setActiveModuleStateWorker );
}

export function* setActiveModuleUserViewWatcher() {
    yield takeLatest( setActiveModuleAction, setActiveModuleWorker );
}