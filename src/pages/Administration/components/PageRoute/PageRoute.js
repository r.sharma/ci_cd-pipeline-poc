import React, { PureComponent } from "react";
import { Table, Loader } from "components";
import colors from "styles/variables.scss";

import { columnsUsers } from "services/User/models";

export class PageRoute extends PureComponent {
    onEdit = ( key, data ) => {
        const { showViewDetailsPanelAction } = this.props;

        showViewDetailsPanelAction( {
            key,
            value: data[key]
        } );
    };

    onDelete = data => {
        console.log( data );
    };

    getRoute = () => {
        const { activeModuleState, users } = this.props;

        switch ( activeModuleState ) {
            case "users":
                if ( !users ) return <Loader style={{ margin: "20% auto" }} />;

                return (
                    <Table
                        //title="Users"
                        rows={users}
                        columns={columnsUsers}
                        onEdit={this.onEdit}
                        onDelete={this.onDelete}
                        rowRepresents="users"
                        headercolor={colors.primaryLighter}
                    />
                );

            default:
                return <p>No information for module {activeModuleState}!</p>;
        }
    };

    render() {
        return (
            <div className="row">
                <div className="col-lg-12">{this.getRoute()}</div>
            </div>
        );
    }
}
