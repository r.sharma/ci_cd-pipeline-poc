import {
    isLoadingBrandsListAction,
    saveBrandsListAction,
    saveBrandsFavoriteListAction,
    storeActiveBrandAction,
    isSwitchingBrandAction
} from "./actions";
import { handleActions } from "redux-actions";

const initialState = {
    brandList: null,
    isLoadingBrandsList: false,
    activeBrand: null,
    isSwitchingBrand: false,
    favorite: []
};

export const BrandReducer = handleActions(
    {
        [isSwitchingBrandAction]: ( state, { payload } ) => ( {
            ...state,
            isSwitchingBrand: payload
        } ),
        [isLoadingBrandsListAction]: ( state, { payload } ) => ( {
            ...state,
            isLoadingBrandsList: payload
        } ),
        [saveBrandsListAction]: ( state, { payload } ) => ( {
            ...state,
            brandList: payload
        } ),
        [saveBrandsFavoriteListAction]: ( state, { payload } ) => ( {
            ...state,
            favorite: payload
        } ),
        [storeActiveBrandAction]: ( state, { payload } ) => ( {
            ...state,
            activeBrand: payload
        } )
    },
    initialState
);
