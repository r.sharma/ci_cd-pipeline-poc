import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import readXlsxFile from "read-excel-file";
import Papa from "papaparse";
import {
    settingsSmall,
    searchSmall,
    addSmall,
    uploadSmall,
    downloadSmall,
    arrow,
    userD,
    exportSvg
} from "components";
import { HomeBadge, TextInput, SideBarWrapper } from "components";
import { CollapsibleDropDownBlock } from "./components/CollapsibleDropDownBlock";
import { CollapsibleInputBlock } from "./components/CollapsibleInputBlock";
import { CollapsibleDatePickerBlock } from "./components/CollapsibleDatePickerBlock";
import { ImportFileModalAddFile } from "./components/importFileModalAddFile";
import { ImportFileModalError } from "./components/importFileModalError";
import { ImportFileModalSuccess } from "./components/importFileModalSuccess";
import moment from "moment";
import { dataFormat } from "config";
import Modal from "@material-ui/core/Modal";
import { ButtonClose } from "components";

export class SideBar extends PureComponent {
    state = {
        file: null,
        error: null,
        loading: false,
        results: null
    };

    changeInputTitle = e => {
        const { value } = e.target;
        const {
            changeSearchWordTitleAction,
            changeSearchWordProfileAction
        } = this.props;
        changeSearchWordProfileAction( null );
        changeSearchWordTitleAction( value );
    };

    changeInputProfileName = e => {
        const { value } = e.target;
        const {
            changeSearchWordProfileAction,
            changeSearchWordTitleAction
        } = this.props;
        changeSearchWordTitleAction( null );
        changeSearchWordProfileAction( value );
    };

    checkFilter = ( label, value ) => {
        const { searchByFilterInTimeAction, activeFilterValueState } = this.props;
        if ( activeFilterValueState[label] === value ) return;
        const newFilter = { ...activeFilterValueState };
        newFilter[label] = value;
        searchByFilterInTimeAction( newFilter );
    };

    checkInputFilter = ( label, value ) => {
        const {
            filterValuesInObjectSaveAction,
            activeFilterValueState
        } = this.props;
        if ( activeFilterValueState[label] === value ) return;
        const newFilter = { ...activeFilterValueState };
        newFilter[label] = value;
        filterValuesInObjectSaveAction( newFilter );
    };

    checkDateFilter = ( label, value ) => {
        if ( value ) value = moment( value ).format( dataFormat );
        this.checkInputFilter( label, value );
    };

    toggleDialog = () => {
        this.props.toggleExportDataModalIsOpenAction();
    };

    toggleDialogImport = () => {
        const {
            importModalState,
            toggleImportModalStateAction,
            clearImportStoreAction
        } = this.props;
        if ( importModalState ) clearImportStoreAction();
        toggleImportModalStateAction();
        this.setState( {
            file: null
        } );
    };

    getFileData = file => {
        const { importDataAction } = this.props;
        const that = this;
        const newPromises = data => {
            that.setState( { loading: true } );
            return new Promise( ( resolve, reject ) => {
                importDataAction( {
                    data: data,
                    resolve,
                    reject
                } );
            } );
        };
        this.setState( {
            loading: true
        } );
        const fileType = file[0].name.split( "." )[1];
        if ( fileType === "xls" || fileType === "xlsx" ) {
            readXlsxFile( file[0] ).then( rows => {
                newPromises( rows )
                    .then(
                        that.setState( {
                            file: file[0].name,
                            loading: false
                        } )
                    )
                    .catch( err => {
                        that.removeLoader();
                        that.setState( { error: err.error } );
                    } );
            } );
        } else if ( fileType === "csv" ) {
            Papa.parse( file[0], {
                complete: function( results ) {
                    newPromises( results.data )
                        .then(
                            that.setState( {
                                file: file[0].name,
                                loading: false
                            } )
                        )
                        .catch( err => {
                            that.removeLoader();
                            that.setState( { error: err.error } );
                        } );
                }
            } );
        } else {
            this.setState( {
                error: "Wrong file type!",
                loading: false
            } );
            return false;
        }
    };

    removeError = () => {
        const { clearImportStoreAction } = this.props;
        this.setState(
            {
                error: null,
                file: null
            },
            () => clearImportStoreAction()
        );
    };

    removeLoader = () => {
        this.setState( {
            loading: false
        } );
    };

    sendImportData = ( onlySuccess = false ) => {
        const {
            sendOnlySuccessfullyImportDataAction,
            sendImportFileAction
        } = this.props;
        const activeAction = onlySuccess
            ? sendOnlySuccessfullyImportDataAction
            : sendImportFileAction;
        this.setState( {
            loading: true
        } );
        new Promise( ( resolve, reject ) => {
            activeAction( {
                resolve,
                reject
            } );
        } )
            .then( res => {
                this.setState( { results: res } );
                this.removeLoader();
            } )
            .catch( err => {
                if ( err && err.error ) this.setState( { error: err.error } );
                this.removeLoader();
            } );
    };

    goToPosts = () => {
        const {
            toggleImportModalStateAction,
            goToThePostsAfterImportAction
        } = this.props;
        const { results } = this.state;
        this.removeError();
        toggleImportModalStateAction();
        goToThePostsAfterImportAction( results.batchUploadID );
    };

    render() {
        const {
            sideBarStateChangeAction,
            sideBarIsOpen,
            activeFilterValueState,
            searchWordTitleState,
            searchWordProfileState,
            searchByFilterValueAction,
            statusListState,
            statusListLoaderState,
            labelListIsLoadingState,
            labelListState,
            platformListLoaderState,
            platformListState,
            activeModuleState,
            exportDataAction,
            exportModalState,
            importModalState,
            importFileState,
            dataAfterImportState,
            copyErrorLinksImportAction,
            exportErrorLinksToCsvAction
        } = this.props;
        const { error, file, loading, results } = this.state;
        let linkTo =
            activeModuleState === "posts"
                ? "/social-protect-add/posts"
                : "/social-protect-add/profiles";
        let sideBarClassName = "side-bar side-bar-flex";
        if ( sideBarIsOpen ) sideBarClassName += " open";
        const successLength =
            (
                dataAfterImportState &&
                dataAfterImportState
                    .filter( e => e.errorsArr && !e.errorsArr.length )
                    .length
            ) || false;
        return (
            <SideBarWrapper
                sideBarClassName={sideBarClassName}
                top={(
                    <>
                        <HomeBadge />

                        <ul className="side-bar-list-icon">
                            <li role="presentation" onClick={sideBarStateChangeAction} className="filters">
                                <div className="icon-wrapper-circle" title="Filters">
                                    {searchSmall()}
                                </div>
                            </li>
                            <li>
                                <Link
                                    to={linkTo}
                                    className="icon-wrapper-circle"
                                    title="Add Item"
                                >
                                    {addSmall()}
                                </Link>
                            </li>
                            <li>
                                <div
                                    className="icon-wrapper-circle"
                                    title="Import"
                                    onClick={this.toggleDialogImport}
                                    role="button"
                                    tabIndex={0}
                                    onKeyPress={e => e.keyCode === 13 ? this.toggleDialogImport() : false}
                                >
                                    {uploadSmall()}
                                </div>
                            </li>
                            <li>
                                <div
                                    className="icon-wrapper-circle"
                                    title="Export"
                                    onClick={this.toggleDialog}
                                    role="button"
                                    tabIndex={0}
                                    onKeyPress={e => e.keyCode === 13 ? this.toggleDialog() : false}
                                >
                                    {downloadSmall()}
                                </div>
                            </li>
                        </ul>
                    </>
                )}
                bottom={( <div className="icon-wrapper-circle">{settingsSmall()}</div> )}
                slideBar={sideBarIsOpen ? (
                    <div className="side-bar-right">
                        <div className="top-panel">
                            <div className="logo-btn-box noselect">
                                <span>Search</span>
                                <div
                                    role="button"
                                    tabIndex={0}
                                    className="side-bar-hide"
                                    onClick={sideBarStateChangeAction}
                                    onKeyPress={e => e.keyCode === 13 ? sideBarStateChangeAction() : false}
                                >
                                    {arrow()}
                                </div>
                            </div>
                            <div className="input-panel noselect">
                                <TextInput
                                    placeholder={"e.g. title, criteria..."}
                                    childrenStyle={{
                                        marginRight: "10px"
                                    }}
                                    defaultValue={searchWordTitleState || ""}
                                    handleChange={this.changeInputTitle}
                                >
                                    {searchSmall()}
                                </TextInput>
                                <TextInput
                                    placeholder={"Profile name"}
                                    childrenStyle={{
                                        marginRight: "10px"
                                    }}
                                    defaultValue={searchWordProfileState || ""}
                                    handleChange={this.changeInputProfileName}
                                >
                                    {userD()}
                                </TextInput>
                            </div>
                            <div className="filter-panel scroll-m noselect">
                                <div>
                                    <CollapsibleDropDownBlock
                                        title="Platform"
                                        name="platform"
                                        clearable={true}
                                        loader={platformListLoaderState}
                                        data={platformListState}
                                        activeFilterData={activeFilterValueState}
                                        handleChange={this.checkFilter}
                                    />
                                    <CollapsibleDropDownBlock
                                        title="Label"
                                        name="postMark"
                                        clearable={true}
                                        loader={labelListIsLoadingState}
                                        data={labelListState}
                                        activeFilterData={activeFilterValueState}
                                        handleChange={this.checkFilter}
                                    />
                                    <CollapsibleDropDownBlock
                                        title="Enforcement status"
                                        name="enforcement"
                                        clearable={true}
                                        loader={statusListLoaderState}
                                        data={statusListState}
                                        activeFilterData={activeFilterValueState}
                                        handleChange={this.checkFilter}
                                    />
                                    <CollapsibleInputBlock
                                        title="Profile name"
                                        name="profileName"
                                        defaultValue={activeFilterValueState.profileName}
                                        handleChange={this.checkInputFilter}
                                    />
                                    <CollapsibleInputBlock
                                        title="Profile ID"
                                        name="profileId"
                                        defaultValue={activeFilterValueState.profileId}
                                        handleChange={this.checkInputFilter}
                                    />
                                    {activeModuleState === "posts" ? (
                                        <CollapsibleInputBlock
                                            title="Profile URL"
                                            name="profileUrl"
                                            defaultValue={activeFilterValueState.profileUrl}
                                            handleChange={this.checkInputFilter}
                                        />
                                    ) : null}
                                    <CollapsibleInputBlock
                                        title="Listing ID"
                                        name="listingId"
                                        defaultValue={activeFilterValueState.listingId}
                                        handleChange={this.checkInputFilter}
                                    />
                                    <CollapsibleInputBlock
                                        title="Batch upload ID"
                                        name="batchUploadId"
                                        defaultValue={activeFilterValueState.batchUploadId}
                                        handleChange={this.checkInputFilter}
                                    />
                                    <CollapsibleDatePickerBlock
                                        title="Date"
                                        nameFrom="createdAtFrom"
                                        nameTo="createdAtTo"
                                        defaultValueFrom={activeFilterValueState.createdAtFrom}
                                        defaultValueTo={activeFilterValueState.createdAtTo}
                                        handleChange={this.checkDateFilter}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="button-panel">
                            <button
                                type="button"
                                className="btn btn-blue"
                                onClick={searchByFilterValueAction}
                            >
                                Apply
                            </button>
                            {/*<button type="button" className="btn">*/}
                            {/*{starBtn()}Save*/}
                            {/*</button>*/}
                        </div>
                    </div>
                ) :   null  }
                modals={(
                    <>
                        <Modal
                            open={exportModalState}
                            disablePortal={true}
                            onClose={this.toggleDialog}
                        >
                            <div className={"main-modal export"}>
                                <ButtonClose
                                    color="#979797"
                                    className="close-modal"
                                    onClick={this.toggleDialog}
                                />
                                <div className="title-slider">Export Data</div>
                                <div className="export-blocks">
                                    <div
                                        onClick={() => exportDataAction( "export" )}
                                        role="presentation"
                                    >
                                        <div>{exportSvg( "#809096" )}</div>
                                        <div>Export for CSV</div>
                                    </div>
                                    <div
                                        onClick={() => exportDataAction( "exportExcel" )}
                                        role="presentation"
                                    >
                                        <div>{exportSvg( "#809096" )}</div>
                                        <div>Export for Excel</div>
                                    </div>
                                </div>
                            </div>
                        </Modal>

                        <Modal
                            open={importModalState}
                            disablePortal={true}
                            onClose={this.toggleDialogImport}
                        >
                            <div className={"main-modal import"}>
                                <ButtonClose
                                    color="#979797"
                                    className="close-modal"
                                    onClick={this.toggleDialogImport}
                                />
                                <div className="title-slider">Import data</div>
                                {!importFileState ? (
                                    <ImportFileModalAddFile
                                        activeModuleState={activeModuleState}
                                        error={error}
                                        loading={loading}
                                        file={file}
                                        sendImportData={this.sendImportData}
                                        toggleDialogImport={this.toggleDialogImport}
                                        getFileData={this.getFileData}
                                        removeError={this.removeError}
                                    />
                                ) : importFileState === "error" ? (
                                    <ImportFileModalError
                                        exportErrorLinksToCsvAction={exportErrorLinksToCsvAction}
                                        error={error}
                                        loading={loading}
                                        dataAfterImportState={dataAfterImportState}
                                        successLength={successLength}
                                        copyErrorLinksImportAction={copyErrorLinksImportAction}
                                        sendImportData={this.sendImportData}
                                        removeError={this.removeError}
                                        toggleDialogImport={this.toggleDialogImport}
                                    />
                                ) : importFileState === "success" ? (
                                    <ImportFileModalSuccess
                                        results={results}
                                        goToPosts={this.goToPosts}
                                        toggleDialogImport={this.toggleDialogImport}
                                    />
                                ) : null}
                            </div>
                        </Modal>
                    </>
                )}
            />
        );
    }
}
