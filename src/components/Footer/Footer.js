import React from "react";
import "./Footer.scss";

export const FooterWrapper = ( { left, center, right } ) => {
    return (
        <footer className="page-footer page-footer__social-protect">
            <div>{left}</div>
            <div>{center}</div>
            <div>{right}</div>
        </footer>
    );
};
