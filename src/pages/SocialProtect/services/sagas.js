import * as CardSagas from "./Card/sagas";
import * as EnforcementSagas from "./Enforcement/sagas";
import * as OptionsSagas from "./Options/sagas";
import * as UserViewSagas from "./UserView/sagas";
import * as PageRouteSagas from "pages/SocialProtect/components/PageRoute/services/sagas";
import * as SideBarExportDataSagas from "pages/SocialProtect/components/SideBar/services/ExportData/sagas";
import * as SideBarImportDataSagas from "pages/SocialProtect/components/SideBar/services/ImportData/sagas";

export const socialProtectSagas = {
    CardSagas,
    EnforcementSagas,
    OptionsSagas,
    UserViewSagas,
    PageRouteSagas,
    SideBarExportDataSagas,
    SideBarImportDataSagas
};
