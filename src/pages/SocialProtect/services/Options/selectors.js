import { createSelector } from "reselect";

export const getSearchWordTitleSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        return state.socialProtect[activeModuleState].search_title;
    },
    state => state
);

export const getSearchWordProfileNameSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        return state.socialProtect[activeModuleState].search_profile_name;
    },
    state => state
);

export const activeFilterMainObjectSelector = createSelector(
    state => {
        return state.socialProtect.options.filterValue;
    },
    state => state
);

export const activeFilterValueSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        return state.socialProtect.options.filterValue[activeModuleState];
    },
    state => state
);

export const getLoadMoreLoaderSelector = createSelector(
    state => {
        return state.socialProtect.options.loadMoreLoader;
    },
    state => state
);

export const selectedPostMarkIdSelector = createSelector(
    state => {
        return state.socialProtect.options.selectedPostMarkId;
    },
    state => state
);

export const selectedInfringementTypeIdSelector = createSelector(
    state => {
        return state.socialProtect.options.selectedInfringementTypeId;
    },
    state => state
);

export const selectedEnforcementStatusIdSelector = createSelector(
    state => {
        return state.socialProtect.options.selectedEnforcementStatusId;
    },
    state => state
);

export const startEnforcementLoaderSelector = createSelector(
    state => state.socialProtect.options.startEnforcementLoader,
    state => state
);
