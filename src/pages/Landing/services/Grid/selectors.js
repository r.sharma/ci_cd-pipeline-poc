import { createSelector } from "reselect";

export const activeGridViewSelector = createSelector(
    state => {
        return state.landing.gridView;
    },
    state => state
);
