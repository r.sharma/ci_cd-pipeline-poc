import React from "react";
import { mount } from "enzyme";
import { CollapsibleParagraph } from "./CollapsibleParagraph";

describe( "CollapsibleParagraph", () => {
    it( "renders 1 <CollapsibleParagraph /> component", () => {
        const component = mount( <CollapsibleParagraph /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const component = mount( <CollapsibleParagraph text={"texttext"} /> );
        const props = component.instance().props;

        expect( props.text ).toEqual( "texttext" );
        expect(
            component
                .find( ".all-text" )
                .children()
                .find( "span" )
                .at( 0 )
                .text()
        ).toEqual( "texttext" );
        expect( component.find( ".show-text" ).text() ).toEqual( "Less" );
    } );

    describe( "it change state on Read more…", () => {
        const component = mount( <CollapsibleParagraph text={"texttext"} /> );
        expect( component.state().open ).toBe( false );
        const spanBtn = component
            .find( ".row-btn-text" )
            .children()
            .find( ".show-text" );
        expect( component.state().hideBtn ).toBe( false );
        expect( spanBtn ).toHaveLength( 0 );
    } );

    describe( "it change state on Read more….", () => {
        const props =
      "That feeling when you don't see people for years(9, to be exact) hometown 😎🌞🥂 Do That feeling when you don't see people for years(9, to be exact) hometown 😎🌞🥂 Do";
        const component = mount( <CollapsibleParagraph text={props} /> );
        expect( component.state().open ).toBe( false );

        expect( component.state().hideBtn ).toBe( false );
        component.setState( { hideBtn: true } );
        expect( component.state().hideBtn ).toBe( true );
        const spanBtn = component
            .find( ".row-btn-text" )
            .children()
            .find( "span" );
        expect( spanBtn ).toHaveLength( 1 );

        spanBtn.simulate( "click" );
        expect( component.state().open ).toBe( true );
    } );

    describe( "check className", () => {
        const props =
      "That feeling when you don't see people for years(9, to be exact) hometown 😎🌞🥂 Do That feeling when you don't see people for years(9, to be exact) hometown 😎🌞🥂 Do";
        const component = mount( <CollapsibleParagraph text={props} /> );

        expect(
            component
                .children()
                .find( "div" )
                .at( 0 )
                .hasClass( "text-box-card" )
        ).toBe( true );
        component.setState( { open: true } );
        expect(
            component
                .children()
                .find( "div" )
                .at( 0 )
                .hasClass( "text-box-card show" )
        ).toBe( true );
    } );
} );
