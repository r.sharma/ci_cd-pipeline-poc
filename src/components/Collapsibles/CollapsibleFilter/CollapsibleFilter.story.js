import React from "react";
import { storiesOf } from "@storybook/react";
import { Checkbox } from "components";
import { CollapsibleFilter } from "./CollapsibleFilter";

storiesOf( "Collapsible Filter", module ).add( "normal state", () => (
    <div className="row">
        <div className="col">
            <CollapsibleFilter title="Hockey">
                <div>
                    <Checkbox label="label 1" /> <span>245</span>
                </div>
                <div>
                    <Checkbox label="label 2" value={true} /> <span>12</span>
                </div>
                <div>
                    <Checkbox label="label 3" value={true} /> <span>56</span>
                </div>
                <div className="check-all-items">+ All statuses</div>
            </CollapsibleFilter>
        </div>
        <div className="col" />
        <div className="col" />
    </div>
) );
