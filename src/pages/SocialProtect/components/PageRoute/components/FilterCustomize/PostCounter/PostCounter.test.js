import React from "react";
import { shallow } from "enzyme";
import { PostCounter } from "./PostCounter";

describe( "PostCounter", () => {
    it( "renders 1 <PostCounter /> component", () => {
        const component = shallow( <PostCounter /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const props = {
            postCount: "35"
        };
        const component = shallow( <PostCounter {...props} /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );

        it( "chack component", () => {
            expect( component.find( ".post-counter" ) ).toHaveLength( 1 );
            let textItem = Number( props.postCount ) === 1 ? "Item" : "Items";
            expect( component.find( ".post-counter" ).text() ).toEqual(
                `${props.postCount} ${textItem}`
            );
        } );
    } );
} );
