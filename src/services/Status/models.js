export const statusVariables = [
    "Infringing",
    "Suspicious",
    "Genuine",
    "Irrelevant",
    "Authorized",
    "Unable to determine"
];
