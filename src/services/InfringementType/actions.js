import { createAction } from "redux-actions";

export const loadInfringementTypeListAction = createAction(
    "loadInfringementTypeListAction"
);
export const saveInfringementTypeListAction = createAction(
    "saveInfringementTypeListAction"
);
