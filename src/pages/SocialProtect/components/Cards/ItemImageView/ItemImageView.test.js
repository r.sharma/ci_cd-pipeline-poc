import React from "react";
import { shallow } from "enzyme";
import { CardItemImageView } from "./ItemImageView";

describe( "CardItemImageView", () => {
    it( "renders 1 <CardItemImageView /> component", () => {
        const component = shallow( <CardItemImageView /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <CardItemImageView /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "CardItem status", () => {
        const props = {
            img: "http://cache.jpg",
            imgCardAlt: "imgCardAlt",
            titleHeader: "titleHeader",
            name: "name",
            date: "2019-04-08T07:32:33.278Z",
            text: "text",
            channel: "channel",
            posts: "0",
            views: "0",
            status: "channel",
            imgHeader: "imgHeader",
            selectedCardsList: [ "342" ],
            element: { id: "123" }
        };
        const component = shallow( <CardItemImageView {...props} /> );

        const title = component
            .find( ".title-description-card" )
            .children()
            .find( "div" );

        expect( title ).toHaveLength( 1 );
        expect( title.text() ).toEqual( props.name );

        const title2 = component
            .find( ".title-description-card" )
            .children()
            .find( "span" );

        jest.mock( "moment", () => () => ( { format: () => "08 Apr 2019" } ) );

        expect( title2 ).toHaveLength( 1 );
        expect( title2.text() ).toEqual( "08 Apr 2019" );

        const CollapsibleParagraph = component.find( "CollapsibleParagraph" );
        expect( CollapsibleParagraph ).toHaveLength( 0 );

        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
        ).toHaveLength( 1 );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().src
        ).toEqual( "http://cache.jpg" );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().alt
        ).toEqual( props.imgCardAlt );

        const wrapper = component.find( ".views" );
        expect( wrapper ).toHaveLength( 1 );
        expect(
            wrapper
                .find( "span" )
                .childAt( 1 )
                .text()
        ).toEqual( props.views );

        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
        ).toHaveLength( 1 );
        expect(
            component
                .find( ".img-box" )
                .children()
                .find( "img" )
                .props().src
        ).toEqual( "http://cache.jpg" );

        const StatusIcons = component.find( "Checkbox" );
        const defaultValue =
      props.selectedCardsList.indexOf( props.element.id ) !== -1;
        expect( StatusIcons.props().defaultValue ).toEqual( defaultValue );

        const CardActions = component.find( "CardActions" );
        expect( CardActions ).toHaveLength( 1 );
        expect( CardActions.props().id ).toEqual( props.element.id );

        const SocialTitle = component.find( "SocialTitle" );
        expect( SocialTitle ).toHaveLength( 1 );
        expect( SocialTitle.props().code ).toEqual( props.imgHeader );
        expect( SocialTitle.props().title ).toEqual( props.titleHeader );
    } );
} );
