import React from "react";
import { storiesOf } from "@storybook/react";
import { Dropdown } from "./Dropdown";

const sports = [
    "Baseball",
    "Basketball",
    "Cricket",
    "Field Hockey",
    "Football",
    "Table Tennis",
    "Tennis",
    "Volleyball"
];

const page = [ "10", "20", "30", "50", "100" ];

storiesOf( "Dropdowns", module )
    .add( "dropdown list", () => (
        <Dropdown
            data={sports}
            name="default"
            wrapperClassName="form-dropdown"
            label="Default"
        />
    ) )
    .add( "dropdown list disabled", () => (
        <Dropdown
            data={sports}
            name="default2"
            wrapperClassName="form-dropdown"
            label="Default"
            disabled={true}
        />
    ) )
    .add( "filter dropdown", () => (
        <Dropdown
            data={page}
            defaultValue="20"
            name="default2"
            wrapperClassName="filter-dropdown"
            label="Rows per page:"
        />
    ) );
