import { configure } from "@storybook/react";

import "components/App/Reset.scss";
import "components/App/App.scss";
import "./storybook.scss";

// automatically import all files ending in *.stories.js
const req = require.context("../src", true, /\.story.js?$/);
function loadStories() {
  req
    .keys()
    .sort()
    .forEach(filename => req(filename));
}

configure(loadStories, module);
