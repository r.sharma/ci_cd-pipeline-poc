import enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
enzyme.configure( { adapter: new Adapter() } );

//Jest testing with react: matchMedia not present, legacy browsers require a polyfill
//Slick slider
window.matchMedia =
  window.matchMedia ||
  function() {
      return {
          matches: false,
          addListener: function() {},
          removeListener: function() {}
      };
  };
