import React from "react";
import { shallow, mount } from "enzyme";
import { Checkbox } from "./Checkbox";

describe( "Checkbox", () => {
    it( "renders 1 <Checkbox /> component", () => {
        const component = shallow( <Checkbox /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const component = shallow(
            <Checkbox name="checkbox" label="Label-1" value={false} />
        );
        const props = component.instance().props;

        expect( props.name ).toEqual( "checkbox" );
        expect( props.label ).toEqual( "Label-1" );
        expect( props.value ).toEqual( false );
    } );

    describe( "check className", () => {
        const component = shallow( <Checkbox /> );

        expect( component.props().className ).toEqual( "label-checkbox-default " );
    } );

    describe( "check className labelClassName", () => {
        const component = shallow( <Checkbox labelClassName="label-test" /> );

        expect( component.props().className ).toEqual(
            "label-checkbox-default label-test"
        );
    } );

    describe( "check name input", () => {
        const component = shallow( <Checkbox name="checkbox" /> );

        expect( component.find( "input" ).prop( "name" ) ).toEqual( "checkbox" );
    } );

    describe( "check value input true", () => {
        const component = shallow( <Checkbox value={true} /> );
        expect( component.find( "input" ).props().checked ).toEqual( true );
    } );

    describe( "check value input false", () => {
        const component = shallow( <Checkbox value={false} /> );
        expect( component.find( "input" ).props().checked ).toEqual( false );
    } );

    describe( "check svg after input", () => {
        const component = shallow( <Checkbox value={true} /> );

        expect(
            component
                .children()
                .find( "span" )
                .children()
                .type()
        ).toEqual( "svg" );
    } );

    describe( "check without label", () => {
        const component = shallow( <Checkbox /> );

        expect( component.children().find( ".title" ) ).toHaveLength( 0 );
    } );

    describe( "check label", () => {
        const component = shallow( <Checkbox label={"labelTest"} /> );

        expect(
            component
                .children()
                .find( ".title" )
                .text()
        ).toEqual( "labelTest" );
    } );

    describe( "testing component functions", () => {
        const component = mount( <Checkbox /> );
        expect( component.state().value ).toBe( false );

        const input = component.find( "input" );
        input.simulate( "change", { target: { checked: true } } );
        expect( component.state().value ).toBe( true );
    } );
} );
