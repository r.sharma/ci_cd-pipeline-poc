import React, { PureComponent } from "react";
import { AddPostForm } from "./components";
import { Header } from "pages/SocialProtect/components";
import "pages/SocialProtect/styles/AddItem.scss";

export class AddPost extends PureComponent {
    render() {
        return (
            <div className="bg-primary-transparent page-container page-container__full-width scroll-m">
                {/*Header start*/}
                <Header title="Social Protect" />
                {/*Header end*/}

                {/*Form Add Post start*/}
                <AddPostForm />
                {/*Form Add Post end*/}
            </div>
        );
    }
}
