import { createAction } from "redux-actions";

//Get a data
export const getLabelListAction = createAction( "getLabelListAction" );
//Loading
export const isLoadingLabelListAction = createAction( "isLoadingLabelListAction" );
//save Data
export const setPostLabelListAction = createAction( "setPostLabelListAction" );
export const setProfileMarkListAction = createAction( "setProfileMarkListAction" );