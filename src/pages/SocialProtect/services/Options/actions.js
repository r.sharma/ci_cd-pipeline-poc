import { createAction } from "redux-actions";

//Save filter values
export const filterValuesInObjectSaveAction = createAction(
    "filterValuesInObjectSaveAction"
);
export const searchByFilterValueAction = createAction(
    "searchByFilterValueAction"
);
export const searchByFilterInTimeAction = createAction(
    "searchByFilterInTimeAction"
);
export const saveFilterValueAction = createAction( "saveFilterValueAction" );

export const changeSearchWordTitleAction = createAction(
    "changeSearchWordTitleAction"
);

export const saveSearchWordTitlePostAction = createAction(
    "saveSearchWordTitlePostAction"
);
export const saveSearchWordTitleProfileAction = createAction(
    "saveSearchWordTitleProfileAction"
);

export const changeSearchWordProfileAction = createAction(
    "changeSearchWordProfileAction"
);
export const saveSearchWordProfileNameInPostAction = createAction(
    "saveSearchWordProfileNameInPostAction"
);
export const saveSearchWordProfileNameInProfileAction = createAction(
    "saveSearchWordProfileNameInProfileAction"
);

export const selectInfringementTypeInTopBarAction = createAction(
    "selectInfringementTypeInTopBarAction"
);
export const selectEnforcementStatusInTopBarAction = createAction(
    "selectEnforcementStatusInTopBarAction"
);
