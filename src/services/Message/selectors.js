import { createSelector } from "reselect";

export const globalMessageSelector = createSelector(
    state => state.messages.messageBody,
    state => state
);
