import React from "react";
import { storiesOf } from "@storybook/react";
import { Tooltip } from "./Tooltip";

storiesOf( "Tooltip", module ).add( "normal state", () => (
    <div style={{ margin: "100px" }}>
        <Tooltip title="title tooltip">Tooltip text</Tooltip>
        <br />
        <br />
        <br />
        <br />
        <br />
        <Tooltip title="Show all posst from this channel ">Tooltip text</Tooltip>
    </div>
) );
