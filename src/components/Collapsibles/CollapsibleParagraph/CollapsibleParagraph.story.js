import React from "react";
import { storiesOf } from "@storybook/react";
import { CollapsibleParagraph } from "./CollapsibleParagraph";

storiesOf( "Collapsible Paragraph", module ).add( "normal state", () => (
    <div className="row">
        <div className="col">
            <CollapsibleParagraph
                text="That feeling when you don't see people for years(9, to be exact) hometown 😎🌞🥂 Do That feeling when you don't see people for years(9, to be exact)"
            />
        </div>
        <div className="col">
            <CollapsibleParagraph text="(9, to be exact) hometown 😎🌞🥂 Do " />
        </div>
        <div className="col" />
    </div>
) );
