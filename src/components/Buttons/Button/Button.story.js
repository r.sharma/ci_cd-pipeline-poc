import React from "react";
import { storiesOf } from "@storybook/react";
import { Button } from "./Button";

storiesOf( "Button", module ).add( "Buttons", () => (
    <div className="row">
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" size="small" />
            </div>
        </div>
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" color="secondary" />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" color="secondary" size="small" />
            </div>
        </div>
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" color="secondary" disabled={true} />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button text="default" color="secondary" size="small" disabled={true} />
            </div>
        </div>
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button type="circle" add={true} />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button type="circle" add={true} size="small" />
            </div>
        </div>
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button type="circle" color="secondary" add={true} />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button type="circle" color="secondary" add={true} size="small" />
            </div>
        </div>
        <div className="col">
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button type="circle" color="secondary" add={true} disabled={true} />
            </div>
            <div
                style={{
                    marginBottom: "20px",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button
                    type="circle"
                    color="secondary"
                    add={true}
                    size="small"
                    disabled={true}
                />
            </div>
        </div>
    </div>
) );
