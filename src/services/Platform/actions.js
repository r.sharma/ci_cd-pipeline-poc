import { createAction } from "redux-actions";

export const loadPlatformListAction = createAction( "loadPlatformListAction" );
export const savePlatformListAction = createAction( "savePlatformListAction" );
