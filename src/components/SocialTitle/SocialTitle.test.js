import React from "react";
import { shallow } from "enzyme";
import { SocialTitle } from "./SocialTitle";

describe( "SocialTitle", () => {
    it( "renders 1 <SocialTitle /> component", () => {
        const component = shallow( <SocialTitle /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const props = {
            code: null,
            title: "title"
        };
        const component = shallow( <SocialTitle {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual( "title" );
        expect( component.text() ).toEqual( props.title );
    } );
} );
