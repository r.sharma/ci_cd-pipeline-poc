import React from "react";
import { shallow } from "enzyme";
import { GridTableView } from "./GridTableView";
import { listEnabledItem } from "services/Post";
import { fakeData } from "../../fakeDataCardList";
import moment from "moment";
import { dataFormat } from "config";

describe( "Avatar", () => {
    it( "renders 1 <GridTableView /> component", () => {
        const component = shallow( <GridTableView /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        let props = {
            data: fakeData.results,
            tableView: "large",
            columnSelected: listEnabledItem,
            selectedAllCardsState: false
        };
        const component = shallow( <GridTableView {...props} /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );

        it( "chack component", () => {
            expect(
                component
                    .find( "div" )
                    .at( 0 )
                    .hasClass( "large" )
            ).toBeTruthy();
            expect( component.find( "table thead tr" ).find( "Checkbox" ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead" )
                    .find( "Checkbox" )
                    .props().defaultValue
            ).toEqual( props.selectedAllCardsState );

            expect( component.find( "table thead tr" ).childAt( 2 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 2 )
                    .text()
            ).toBe( "Chanel" );
            expect( component.find( "table thead tr" ).childAt( 3 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 3 )
                    .text()
            ).toBe( "Platform" );
            expect( component.find( "table thead tr" ).childAt( 4 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 4 )
                    .text()
            ).toBe( "Posts" );
            expect( component.find( "table thead tr" ).childAt( 5 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 5 )
                    .text()
            ).toBe( "Views" );
            expect( component.find( "table thead tr" ).childAt( 6 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 6 )
                    .text()
            ).toBe( "Date" );
            expect( component.find( "table thead tr" ).childAt( 7 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 7 )
                    .text()
            ).toBe( "Description" );
            expect( component.find( "table thead tr" ).childAt( 8 ) ).toHaveLength( 1 );
            expect(
                component
                    .find( "table thead tr" )
                    .childAt( 8 )
                    .text()
            ).toBe( "Status" );

            const trFirst = component.find( "table tbody tr" );
            expect(
                trFirst
                    .find( "td" )
                    .at( 1 )
                    .find( "RowLinkView" ).props.label
            ).toEqual( fakeData.results[0].profile.name );
            expect(
                trFirst
                    .find( "td" )
                    .at( 3 )
                    .find( "SocialTitle" )
            ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 3 )
                    .find( "SocialTitle" )
                    .props().code
            ).toEqual( fakeData.results[0].platform.code );
            expect(
                trFirst
                    .find( "td" )
                    .at( 3 )
                    .find( "SocialTitle" )
                    .props().title
            ).toEqual( fakeData.results[0].platform.name );
            expect(
                trFirst
                    .find( "td" )
                    .at( 2 )
                    .find( "RowLinkView" )
            ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 2 )
                    .find( "RowLinkView" )
                    .props().label
            ).toEqual( fakeData.results[0].brandName );
            expect(
                trFirst
                    .find( "td" )
                    .at( 4 )
                    .text()
            ).toBe( fakeData.results[0].profile.profilePosts.toString() );
            expect(
                trFirst
                    .find( "td" )
                    .at( 5 )
                    .text()
            ).toBe( fakeData.results[0].profile.profileViews.toString() );
            expect(
                trFirst
                    .find( "td" )
                    .at( 6 )
                    .text()
            ).toBe( moment( fakeData.results[0].createdAt ).format( dataFormat ) );
            expect(
                trFirst
                    .find( "td" )
                    .at( 7 )
                    .text()
            ).toBe( fakeData.results[0].listingDescription.toString() );
            expect(
                trFirst
                    .find( "td" )
                    .at( 8 )
                    .find( "StatusIcons" )
            ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 9 )
                    .find( "CardActions" )
            ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 9 )
                    .find( "CardActions" )
                    .props().horizontally
            ).toBeTruthy();
        } );
    } );
} );
