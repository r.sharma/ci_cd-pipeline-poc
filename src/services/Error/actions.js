import { createAction } from "redux-actions";

export const globalError = createAction( "globalError" );
