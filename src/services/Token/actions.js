import { createAction } from "redux-actions";

export const getNewToken = createAction( "getNewToken" );
