import React from "react";
import { shallow, mount } from "enzyme";
import { Dropdown } from "./Dropdown";

const sports = [
    "Baseball",
    "Basketball",
    "Cricket",
    "Field Hockey",
    "Football",
    "Table Tennis",
    "Tennis",
    "Volleyball"
];

describe( "Dropdown", () => {
    it( "renders 1 <Dropdown /> component", () => {
        const component = shallow( <Dropdown /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = mount(
            <Dropdown
                data={sports}
                name="default"
                wrapperClassName="form-dropdown"
                label="Default"
            />
        );
        const props = component.instance().props;

        expect( props.data ).toEqual( sports );
        expect( props.name ).toEqual( "default" );
        expect( props.wrapperClassName ).toEqual( "form-dropdown" );
        expect( props.label ).toEqual( "Default" );

        expect( component.children().length ).toEqual( 1 );
        it( "chack component", () => {
            expect( component.state().selected ).toEqual( null );
            expect( component.childAt( 0 ).hasClass( "dropdown form-dropdown" ) ).toEqual(
                true
            );
            expect(
                component
                    .children()
                    .find( ".title" )
                    .text()
            ).toEqual( "Default" );
            expect( component.state().open ).toEqual( false );
            expect(
                component
                    .children()
                    .find( ".dropdown-body" )
                    .childAt( 0 )
                    .props().className
            ).toEqual( "wrapper-button" );
            expect(
                component
                    .children()
                    .find( ".dropdown-body" )
                    .childAt( 1 )
                    .props().className
            ).toEqual( "dropdown-menu" );
            component.find( ".wrapper-button" ).simulate( "click" );
            expect( component.state().open ).toEqual( true );
            expect(
                component
                    .children()
                    .find( ".dropdown-body" )
                    .childAt( 0 )
                    .props().className
            ).toEqual( "wrapper-button show" );
            expect(
                component
                    .children()
                    .find( ".dropdown-body" )
                    .childAt( 1 )
                    .props().className
            ).toEqual( "dropdown-list show" );

            expect(
                component
                    .children()
                    .find( ".dropdown-list" )
                    .childAt( 0 )
                    .type()
            ).toEqual( "ul" );

            component
                .children()
                .find( ".dropdown-list" )
                .childAt( 0 )
                .childAt( 0 )
                .simulate( "click" );

            expect( component.state().open ).toEqual( false );
            expect( component.state().selected ).not.toEqual( null );
            expect( component.state().selected ).toEqual( props.data[0] );

            component.find( ".wrapper-button" ).simulate( "click" );
            expect( component.state().open ).toEqual( true );

            const outerNode = document.createElement( "div" );
            outerNode.className = "outerDiv";
            document.body.appendChild( outerNode );

            outerNode.dispatchEvent( new Event( "click", { bubbles: true } ) );

            expect( component.state().open ).toEqual( false );
        } );
    } );
} );
