import { combineReducers } from "redux";

import { adminUserReducer } from "./User/reducer";
import { adminRoleReducer } from "./Role/reducer";

export const administrationReducer = combineReducers( {
    users: adminUserReducer,
    roles: adminRoleReducer
} );
