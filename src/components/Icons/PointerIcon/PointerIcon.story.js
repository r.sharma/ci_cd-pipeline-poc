import React from "react";
import { storiesOf } from "@storybook/react";
import { PointerIcon } from "./PointerIcon";

storiesOf( "Pointer Icon", module ).add( "normal state", () => <PointerIcon /> );
