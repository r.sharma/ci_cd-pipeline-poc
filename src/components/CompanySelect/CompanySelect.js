import React, { PureComponent } from "react";
import {
    Avatar,
    TabBar,
    TextInput,
    Loader,
    ButtonClose,
    arrow,
    starSvg,
    starActiveSvg,
    searchSmall
} from "components";
import { logoImage } from "./LogoImage";
import { genCharArray } from "utils";
import "./CompanySelect.scss";

export class CompanySelect extends PureComponent {
    constructor( props ) {
        super( props );
        this.myRef = React.createRef();
        this.topDrop = null;
        this.timeout = null;
        this.state = {
            open: false,
            starredDisabled: false,
            mode: "brands",
            searchValue: "",
            brandsList: null,
            filterValue: null,
            scrollTop: 0,
            availableHeight: 661
        };
    }

    static getDerivedStateFromProps( nextProps, prevState ) {
        if ( nextProps.brandsState && !prevState.brandsList ) {
            return {
                brandsList: nextProps.brandsState
            };
        }
        return null;
    }

    componentDidMount() {
        if ( localStorage.brandTab ) {
            this.setState( {
                mode: localStorage.brandTab
            } );
        }
        if (
            localStorage.brandsFavorite &&
      JSON.parse( localStorage.brandsFavorite ).length
        ) {
            this.setState( {
                starredDisabled: false
            } );
        }
        document.addEventListener( "click", this.documentClick, { capture: true } );
    }

    componentDidUpdate() {
        const { brandsFavoriteListState } = this.props;
        const { brandsList, mode } = this.state;
        if ( mode !== "starred" || !brandsList ) return;
        const newArr = brandsList.filter(
            e => brandsFavoriteListState.indexOf( e.id ) !== -1
        );
        if ( !newArr.length ) {
            this.setState( { mode: "brands", starredDisabled: true } );
        }
    }

    componentWillUnmount() {
        document.removeEventListener( "click", this.documentClick, {
            capture: true
        } );
    }

    documentClick = e => {
        const node = this.myRef.current;
        if ( e.target.closest( ".company-select" ) !== node ) this.closeDropDown();
    };

    changeStateCardActions = () => {
        if ( !this.topDrop ) {
            const rect = document
                .getElementById( "company-select" )
                .getBoundingClientRect();
            this.topDrop = 20 + rect.height + rect.top;
        }
        this.setState(
            prevState => ( { open: !prevState.open } ),
            () => {
                this.changeScrollBody( this.state.open );
            }
        );
    };

    closeDropDown = () => {
        this.setState( { open: false } );
        this.changeScrollBody( false );
    };

    changeScrollBody = index => {
        if ( index ) {
            document.documentElement.style.overflow = "hidden"; // firefox, chrome
            document.body.scroll = "no";
        } else {
            document.documentElement.style.overflow = "auto"; // firefox, chrome
            document.body.scroll = "yes"; // ie only
        }
    };

    changeMode = mode => {
        localStorage.brandTab = mode;
        this.setState( { mode } );
    };

    addBrandToFavorite = e => {
        const { addBrandToFavoritesAction } = this.props;
        const { starredDisabled } = this.state;
        if ( starredDisabled ) this.setState( { starredDisabled: false } );
        addBrandToFavoritesAction( e.id );
    };

    setBrandAsActive = ( e, el ) => {
        if ( el.target.tagName === "BUTTON" || el.target.closest( "svg" ) ) return;
        const { saveActiveBrandAction } = this.props;

        this.setState(
            prevState => ( { open: !prevState.open } ),
            () => {
                saveActiveBrandAction( { id: e.id, shouldUpdateSession: true } );
                this.changeScrollBody( this.state.open );
            }
        );
    };

    scrollListBoxToTop = () => {
        const elem = document.getElementsByClassName( "list-company-brands" )[0];
        if ( elem.scrollTop > 0 ) {
            elem.scrollTop = 0;
        }
    };

    filterAlphabet = e => {
        const val = e.target.value;
        const { filterValue } = this.state;
        const { brandsState } = this.props;
        if ( filterValue === val ) {
            this.setState( {
                brandsList: brandsState,
                searchValue: "",
                filterValue: null
            } );
        } else {
            const newArr = brandsState.filter( e => {
                if ( val.indexOf( "9" ) > -1 && Number.isInteger( Number( e.name.charAt( 0 ) ) ) )
                    return e;
                if ( e.name.charAt( 0 ).toLowerCase() === val.toLowerCase() ) return e;
                return false;
            } );
            this.setState( {
                brandsList: newArr,
                searchValue: "",
                filterValue: val
            } );
            this.scrollListBoxToTop();
        }
    };

    searchChange = e => {
        const { value } = e.target;
        const { brandsState } = this.props;
        const that = this;
        if ( this.timeout ) {
            clearTimeout( this.timeout );
        }
        this.setState( {
            searchValue: value
        } );
        this.timeout = setTimeout( () => {
            if ( value === "" ) {
                that.setState( {
                    filterValue: null,
                    brandsList: brandsState
                } );
            } else {
                const newArr = brandsState.filter( e => {
                    if ( e.name.toLowerCase().search( value.toLowerCase() ) !== -1 ) return e;
                    return false;
                } );
                that.setState( {
                    filterValue: null,
                    brandsList: newArr
                } );
                this.scrollListBoxToTop();
            }
        }, 800 );
    };

    handleScroll = event => {
        this.setState( {
            scrollTop: event.target.scrollTop
        } );
    };

    genCharArray( charA, charZ ) {
        var a = [],
            i = charA.charCodeAt( 0 ),
            j = charZ.charCodeAt( 0 );
        for ( ; i <= j; ++i ) {
            a.push( String.fromCharCode( i ) );
        }
        return a;
    }

    render() {
        const {
            brandsFavoriteListState = [],
            loadingBrandState = false,
            activeBrandState,
            isSwitchingBrand,
            brandsState
        } = this.props;
        const {
            open,
            mode,
            brandsList,
            filterValue,
            searchValue,
            starredDisabled
        } = this.state;
        let companySelectClassName = "company-select";
        if ( open ) companySelectClassName += " show";
        if ( !activeBrandState || loadingBrandState || isSwitchingBrand )
            return <Loader style={{ marginLeft: "50px", marginRight: "50px" }} />;
        let activeBrandData = null;
        if ( brandsState && brandsState.length ) {
            activeBrandData = brandsState.filter(
                e => e.id === Number( activeBrandState )
            )[0];
        }

        let listOfBrands = brandsList || null;
        if ( mode === "starred" && listOfBrands ) {
            listOfBrands = listOfBrands.filter(
                e => brandsFavoriteListState.indexOf( e.id ) !== -1
            );
        }

        //render list of Brands
        const marginBot = 30;
        const itemHeight = 185 + marginBot;
        const itemInRow = 6;
        const numLength = listOfBrands && listOfBrands.length;
        const numRows = Math.ceil( numLength / itemInRow );

        const totalHeight = itemHeight * numRows;
        const { availableHeight, scrollTop } = this.state;
        const scrollBottom = scrollTop + availableHeight;

        const startIndex = Math.max(
            0,
            Math.floor( scrollTop / itemHeight ) * itemInRow - 10
        );
        const endIndex = Math.min(
            numRows * itemInRow,
            Math.ceil( scrollBottom / itemHeight ) * itemInRow + 10
        );

        const items = [];

        listOfBrands &&
        listOfBrands.filter( ( elem, index ) => {
            if ( index < endIndex && index >= startIndex ) {
                items.push(
                    <li
                        className="company-brands__item"
                        key={index}
                        onClick={el => this.setBrandAsActive( elem, el )}
                        style={{ marginBottom: marginBot }}
                        role="presentation"
                    >
                        <button
                            className="select-brand"
                            type="button"
                            onClick={() => this.addBrandToFavorite( elem )}
                        >
                            {brandsFavoriteListState.indexOf( elem.id ) === -1
                                ? starSvg( "#cbdae0" )
                                : starActiveSvg()}
                        </button>
                        <div>
                            <Avatar
                                data={[
                                    !elem.logo || elem.logo === "" ? logoImage() : elem.logo
                                ]}
                                size={"65"}
                            />
                            <span>{elem.name}</span>
                        </div>
                    </li>
                );
            }
            return null;
        } );
        return (
            <div
                ref={this.myRef}
                className={companySelectClassName}
                id="company-select"
            >
                <div
                    className="front-box"
                    onClick={this.changeStateCardActions}
                    role="presentation"
                >
                    <Avatar data={[ activeBrandData && activeBrandData.logo ]} />
                    <div className="company-select__box">
                        <div className="company-select__description">
                            <span>Brand</span>
                            <div className="company-select__name">
                                {activeBrandData ? activeBrandData.name : "Not selected"}
                            </div>
                        </div>
                        <button type="button">{arrow()}</button>
                    </div>
                </div>
                <div
                    className="company-select__drop-down scroll-m"
                    style={{
                        top: this.topDrop,
                        height: `calc(100vh - ${this.topDrop}px)`
                    }}
                >
                    <ButtonClose
                        className="close-btn"
                        color="#979797"
                        onClick={this.changeStateCardActions}
                    />
                    <div className="search-panel">
                        <TextInput
                            placeholder={"Search…"}
                            wrapperStyle={{ flexDirection: "row-reverse" }}
                            childrenStyle={{
                                marginRight: "10px"
                            }}
                            handleChange={this.searchChange}
                            defaultValue={searchValue}
                        >
                            {searchSmall()}
                        </TextInput>
                    </div>
                    <div className="filter-box">
                        <TabBar>
                            <button
                                type="button"
                                className={mode === "brands" ? "active" : ""}
                                onClick={() => this.changeMode( "brands" )}
                            >
                                All brands
                            </button>
                            <button
                                type="button"
                                className={mode === "starred" ? "active" : ""}
                                onClick={() => this.changeMode( "starred" )}
                                disabled={starredDisabled}
                            >
                                Starred
                            </button>
                        </TabBar>
                        <div className="alphabet-filter">
                            <ul>
                                <li>
                                    <button
                                        type="button"
                                        onClick={this.filterAlphabet}
                                        value="9"
                                        className={filterValue === "9" ? "active" : ""}
                                    >
                                        0-9
                                    </button>
                                </li>
                                {genCharArray( "A", "Z" ).map( letter => (
                                    <li key={letter}>
                                        <button
                                            type="button"
                                            onClick={this.filterAlphabet}
                                            value={letter}
                                            className={filterValue === letter ? "active" : ""}
                                        >
                                            {letter}
                                        </button>
                                    </li>
                                ) )}
                            </ul>
                        </div>
                    </div>

                    <div
                        className="list-company-brands"
                        style={{
                            height: "100%",
                            overflowY: "scroll"
                        }}
                        onScroll={this.handleScroll}
                    >
                        {brandsList && brandsList.length && open ? (
                            <ul
                                style={{
                                    paddingTop: startIndex * ( itemHeight / itemInRow ),
                                    height: totalHeight
                                }}
                            >
                                {items}
                            </ul>
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}
