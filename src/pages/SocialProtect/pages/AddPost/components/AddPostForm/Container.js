import { connect } from "react-redux";
import { AddPostForm as component } from "./AddPostForm";
import { countryListSelector } from "services/Country/selectors";
import { platformListSelector } from "services/Platform/selectors";
import { statusListSelector } from "services/Status/selectors";
import { labelListSelector } from "services/Label/selectors";
import { infringementTypeListSelector } from "services/InfringementType/selectors";
import { addCardItemAction } from "pages/SocialProtect/services/Card/actions";

const mapDispatchToProps = {
    addCardItemAction
};

const mapStateToProps = state => {
    const countryListState = countryListSelector( state );
    const platformListState = platformListSelector( state );
    const statusListState = statusListSelector( state );
    const labelListState = labelListSelector( state );
    const infringementTypeListState = infringementTypeListSelector( state );
    const formFieldsValues =
    ( state.form.AddPostForm && state.form.AddPostForm.values ) || {};

    return {
        countryListState,
        platformListState,
        statusListState,
        labelListState,
        infringementTypeListState,
        formFieldsValues
    };
};

export const AddPostForm = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
