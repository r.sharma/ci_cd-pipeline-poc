import React from "react";
import moment from "moment";
import {
    Checkbox,
    SocialTitle,
    StatusIcons,
    maximizeCard,
    externalLinkCard,
    viewsSvg
} from "components";
import { dataFormat } from "config";
import { CardActions } from "../";
import "./ItemImageView.scss";

export const CardItemImageView = ( {
    img = null,
    imgCardAlt = "",
    cardURL = null,
    titleHeader = "",
    name = "",
    date = "",
    addClassName = "",
    views = "",
    status = "",
    children,
    imgHeader = null,
    titleHide = false,
    selectedCardsList = [],
    chooseCardToSelect = () => {},
    deleteOneCardsAction = () => {},
    showViewDetailsPanelAction = () => {},
    userPushToRouteAction = () => {},
    element = { id: null }
} ) => {
    const checked = selectedCardsList.indexOf( element.id ) !== -1;
    return (
        <div
            className={`card-item-image-view noselect ${addClassName} ${
                checked ? "checked" : ""
            }`}
        >
            <div className="card-item-header">
                <Checkbox
                    handleChange={( { value } ) => {
                        chooseCardToSelect( { value: value, id: element.id } );
                    }}
                    color={"#fff"}
                    defaultValue={checked}
                />
                <SocialTitle code={imgHeader} title={titleHeader} />
                <CardActions
                    color={"#fff"}
                    id={element.id}
                    deleteOneCardsAction={deleteOneCardsAction}
                    editBtn={false}
                />
            </div>
            <div className="card-item-body">
                <div className="img-box">
                    {img ? <img src={img} alt={imgCardAlt} /> : null}
                    {children ? <div className="children-box">{children}</div> : null}
                </div>
            </div>
            <div className="footer-card">
                <div className="description">
                    <div className="title-box">
                        {!titleHide ? (
                            <div className="title-description-card">
                                <span>{moment( date ).format( dataFormat )}</span>
                                <div>{name || "No title"}</div>
                            </div>
                        ) : null}
                    </div>
                    <div className="detail">
                        <div>
                            <StatusIcons status={status} tooltip={true} />
                        </div>
                        <span className="views">
                            {viewsSvg()}
                            {views || 0}
                        </span>
                    </div>
                </div>
                <div className="card-item-footer">
                    <div>
                        <a
                            href="/"
                            onClick={e => {
                                e.preventDefault();
                                if ( cardURL )
                                    userPushToRouteAction( {
                                        profileURL: cardURL,
                                        module: "posts"
                                    } );
                            }}
                        >
                            {maximizeCard()}
                        </a>
                    </div>
                    <button
                        type={"button"}
                        onClick={() => showViewDetailsPanelAction( element.id )}
                    >
            View details
                    </button>
                    <div>
                        {cardURL ? (
                            <a href={cardURL} rel="noopener noreferrer" target="_blank">
                                {externalLinkCard()}
                            </a>
                        ) : null}
                    </div>
                </div>
            </div>
        </div>
    );
};
