import React from "react";
import { shallow } from "enzyme";
import { ProfileCardItem } from "./ProfileItem";

describe( "ProfileCardItem", () => {
    it( "renders 1 <ProfileCardItem /> component", () => {
        const component = shallow( <ProfileCardItem /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <ProfileCardItem /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "CardItem status", () => {
        const props = {
            img: "img",
            imgCardAlt: "imgCardAlt",
            titleHeader: "titleHeader",
            name: "name",
            date: "",
            text: "text",
            channel: "channel",
            posts: "posts",
            views: "views",
            status: "status",
            imgHeader: null,
            selectedCardsList: [ "342" ],
            element: { id: "123" }
        };
        const component = shallow( <ProfileCardItem {...props} /> );

        const StatusIcons = component.find( "Checkbox" );
        const defaultValue =
      props.selectedCardsList.indexOf( props.element.id ) !== -1;
        expect( StatusIcons.props().defaultValue ).toEqual( defaultValue );

        expect( component.find( "Checkbox" ) ).toHaveLength( 1 );
        expect( component.find( "Checkbox" ).props().defaultValue ).toEqual(
            defaultValue
        );

        expect( component.find( "SocialTitle" ) ).toHaveLength( 1 );
        expect( component.find( "SocialTitle" ).props().code ).toEqual( props.imgHeader );
        expect( component.find( "SocialTitle" ).props().title ).toEqual(
            props.titleHeader
        );

        expect( component.find( "CardActions" ) ).toHaveLength( 1 );
        expect( component.find( "Avatar" ) ).toHaveLength( 1 );
        expect( component.find( "Avatar" ).props().size ).toEqual( "65" );
        expect( component.find( "Avatar" ).props().data ).toEqual( [ props.img ] );

        expect( component.find( "StatusIcons" ) ).toHaveLength( 1 );
        expect( component.find( "StatusIcons" ).props().status ).toEqual( props.status );
        expect( component.find( "StatusIcons" ).props().text ).toEqual( true );

        expect( component.find( ".title-description-card" ) ).toHaveLength( 1 );
        const name = props.name || "No title";
        const followers = props.followers || "0";
        const posts = props.posts || "0";
        const views = props.views || "0";
        expect( component.find( ".description-name" ).text() ).toEqual( name );
        expect(
            component
                .find( ".views-card" )
                .childAt( 0 )
                .find( "span" )
                .text()
        ).toEqual( followers );
        expect(
            component
                .find( ".views-card" )
                .childAt( 1 )
                .find( "span" )
                .text()
        ).toEqual( posts );
        expect(
            component
                .find( ".views-card" )
                .childAt( 2 )
                .find( "span" )
                .text()
        ).toEqual( views );
    } );
} );
