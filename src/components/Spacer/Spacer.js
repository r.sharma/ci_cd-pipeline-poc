import React from "react";

import "./Spacer.scss";

export const Spacer = () => (
    <p className="spacer" />
);