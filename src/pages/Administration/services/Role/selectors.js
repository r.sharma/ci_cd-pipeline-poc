import { createSelector } from "reselect";

export const rolesListSelector = createSelector(
    state => {
        return state.admin.roles.data;
    },
    state => state
);
