import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import { AddPostContainer } from "./Container";
import { fakeDataProfile, fakeData } from "../../../../../fakeDataCardList";
import { brandsData } from "../../../../../brands_json";
import { listEnabledItem, columnsPosts } from "services/Post";
import { columnsProfiles } from "services/Profile";

const socialProtect = {
    profiles: {
        profile_list_loader: false,
        profile_list_data: fakeDataProfile.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    },
    posts: {
        card_list_loader: false,
        card_list_data: fakeData.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    }
};
const socialProtectLoader = {
    profiles: {
        profile_list_loader: false,
        profile_list_data: fakeDataProfile.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    },
    posts: {
        card_list_loader: true,
        card_list_data: fakeData.results,
        item_view_detail: null,
        search_title: null,
        search_profile_name: null,
        selected_card: {
            selectAll: false,
            arrID: []
        }
    }
};

describe( "PostsOverviewDisplay", () => {
    describe( "check loader", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();

            // creates the store with any initial state or middleware needed
            store = mockStore( {
                match: { url: "/social-protect" },
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "row",
                        page_card_display_view: "block",
                        item_view_detail: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "row",
                        page_card_display_view: "x1",
                        item_view_detail: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                },
                socialProtect: socialProtectLoader,
                brands: brandsData.payload
            } );
        } );

        it( "Should render the component", () => {
            const component = shallow(
                <AddPostContainer
                    store={store}
                    match={{ isExact: true, path: "", url: "/social-protect-add-post" }}
                />
            ).dive( {
                context: { store: store }
            } );
            expect( component ).not.toBe( null );

            //const props = component.instance().props;
            // expect(component.find('.loader')).toHaveLength(1);
        } );
    } );

    describe( "check props in component block view", () => {
        let store;

        beforeEach( () => {
            const mockStore = configureStore();

            // creates the store with any initial state or middleware needed
            store = mockStore( {
                match: { url: "/social-protect" },
                userView: {
                    side_bar_open: false,
                    activeModuleState: "posts",
                    profiles: {
                        page_list_view: "row",
                        page_card_display_view: "block",
                        item_view_detail: null,
                        search_title: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsProfiles
                        }
                    },
                    posts: {
                        page_list_view: "row",
                        page_card_display_view: "x1",
                        item_view_detail: null,
                        search_title: null,
                        customize_table_view: {
                            view: "large",
                            list_enabled_item: listEnabledItem,
                            columns: columnsPosts
                        }
                    }
                },
                socialProtect: socialProtect,
                brands: brandsData.payload
            } );
        } );

        it( "Should render the component", () => {
            const state = {
                radioValue: "Instagram",
                postMore: false,
                tabActive: "manually"
            };
            const component = shallow(
                <AddPostContainer
                    store={store}
                    match={{ isExact: true, path: "", url: "/social-protect-add-post" }}
                />
            ).dive( {
                context: { store: store }
            } );
            expect( component ).not.toBe( null );

            const props = component.instance().props;

            expect( component.at( 0 ).children() ).toHaveLength( 2 );

            expect( component.find( "Header" ) ).toHaveLength( 1 );
            expect( component.find( "Header" ).props().title ).toEqual( "Social Protect" );
            expect( component.find( "Header" ).props().brandsState ).toEqual(
                props.brandsState
            );
            expect( component.find( "Header" ).props().brandsFavoriteListState ).toEqual(
                props.brandsFavoriteListState
            );
            expect( component.find( "Header" ).props().activeBrandListState ).toEqual(
                props.activeBrandListState
            );

            expect( component.find( "ReduxForm" ) ).toHaveLength( 1 );
            expect( component.find( "ReduxForm" ).props().radioValue ).toEqual(
                state.radioValue
            );
            expect( component.find( "ReduxForm" ).props().postMore ).toEqual(
                state.postMore
            );
            expect( component.find( "ReduxForm" ).props().tabActive ).toEqual(
                state.tabActive
            );
        } );
    } );
} );
