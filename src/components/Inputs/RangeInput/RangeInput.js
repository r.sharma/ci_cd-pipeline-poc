import React, { PureComponent } from "react";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import "./RangeInput.scss";

export class RangeInput extends PureComponent {
    state = {
        value: { min: 0, max: 1 }
    };

    handleChange = value => {
        const { onChange } = this.props;
        this.setState( { value }, onChange( value ) );
    };

    componentDidMount() {
        const { min, max } = this.props;
        let value = { min: min || 0, max: max || 0 };
        this.setState( {
            value
        } );
    }

    render() {
        const { value } = this.state;
        const { minRange = 0, maxRange = 1 } = this.props;
        return (
            <InputRange
                maxValue={maxRange}
                minValue={minRange}
                value={value}
                onChange={this.handleChange}
            />
        );
    }
}
