import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";

storiesOf( "Typography", module ).add( "Text", () => (
    <Fragment>
        <pre className="mt-3 border-bottom">Hero</pre>
        <p className="hero">Steps In Installing Rack Mount Lcd Monitors</p>

        <pre className="mt-5 border-bottom">H1</pre>
        <h1>Steps In Installing Rack Mount Lcd Monitors</h1>

        <pre className="mt-5 border-bottom">H2</pre>
        <h2>Steps In Installing Rack Mount Lcd Monitors</h2>

        <pre className="mt-5 border-bottom">H3</pre>
        <h3>Steps In Installing Rack Mount Lcd Monitors</h3>

        <pre className="mt-5 border-bottom">H4</pre>
        <h4>Steps In Installing Rack Mount Lcd Monitors</h4>

        <pre className="mt-5 border-bottom">H5</pre>
        <h5>Steps In Installing Rack Mount Lcd Monitors</h5>

        <pre className="mt-5 border-bottom">Subtitle 1</pre>
        <p className="subtitle-1">Steps In Installing Rack Mount Lcd Monitors</p>

        <pre className="mt-5 border-bottom">Subtitle 2</pre>
        <p className="subtitle-2">Steps In Installing Rack Mount Lcd Monitors</p>

        <pre className="mt-5 border-bottom">Body 1</pre>
        <p className="body-1">Steps In Installing Rack Mount Lcd Monitors</p>

        <pre className="mt-5 border-bottom">Body 2</pre>
        <p className="body-2">Steps In Installing Rack Mount Lcd Monitors</p>

        <pre className="mt-5 border-bottom">Caption</pre>
        <p className="caption">Steps In Installing Rack Mount Lcd Monitors</p>
    </Fragment>
) );
