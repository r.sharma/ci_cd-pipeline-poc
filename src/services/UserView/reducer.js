import { sideBarStateSaveAction, storeActiveModuleStateAction, storeActiveModuleAction } from "./actions";
import { handleActions } from "redux-actions";

const initialState = {
    side_bar_open: false,
    activeModule: null
};

export const SideBarUserViewStateReducer = handleActions(
    {
        [sideBarStateSaveAction]: ( state, { payload } ) => ( {
            ...state,
            side_bar_open: payload
        } ),
        [storeActiveModuleStateAction]: ( state, { payload } ) => {
            return {
                ...state,
                activeModuleState: payload
            };
        },
        [storeActiveModuleAction]: ( state, { payload } ) => {
            return {
                ...state,
                activeModule: payload
            };
        }
    },
    initialState
);
