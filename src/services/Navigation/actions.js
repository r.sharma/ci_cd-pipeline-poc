import { createAction } from "redux-actions";

export const handleNavigationAction = createAction( "handleNavigationAction" );
export const onNavigationAction = createAction( "onNavigationAction" );