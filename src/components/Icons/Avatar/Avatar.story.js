import React from "react";
import { storiesOf } from "@storybook/react";
import { Avatar } from "./Avatar";

storiesOf( "Avatar", module )
    .add( "header state", () => (
        <Avatar
            data={[ "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg" ]}
        />
    ) )
    .add( "card state", () => (
        <Avatar
            size="65"
            data={[ "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg" ]}
        />
    ) )
    .add( "multiple allowed", () => (
        <Avatar
            size="80"
            data={[
                "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg",
                "https://xboxavatargear.files.wordpress.com/2016/09/jordan-air-vector-logo.png?w=450",
                "http://www.userlogos.org/files/logos/L0mass/Puma.white.png"
            ]}
        />
    ) );
