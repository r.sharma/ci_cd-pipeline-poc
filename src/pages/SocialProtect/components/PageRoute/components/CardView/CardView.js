import React from "react";
import {
    CardItem,
    GridTableView,
    ProfileCardItem
} from "pages/SocialProtect/components";

import {
    CardItemImageView,
    CardItemImageViewSmall
} from "pages/SocialProtect/components/Cards";

export const CardView = ( {
    cardListState,
    pageViewState,
    tableViewState,
    chooseCardToSelectAction,
    selectedCardsListState,
    chooseAllCardAction,
    selectedAllCardsState,
    pageCardDisplayViewState,
    deleteOneCardsAction,
    activeModuleState,
    showViewDetailsPanelAction,
    activeFilterValueState,
    changeOrderDirectionAction,
    userPushToRouteAction
} ) => {
    if ( !cardListState || ( cardListState && !cardListState.results ) ) return null;
    if ( cardListState && cardListState.results && !cardListState.results.length )
        return <div className="col-lg-12">No Items...</div>;
    if ( pageViewState === "row" ) {
        return (
            <div className="col-lg-12 col-md-12">
                <GridTableView
                    activeModuleState={activeModuleState}
                    data={cardListState.results}
                    tableView={tableViewState.view}
                    columnSelected={tableViewState.list_enabled_item}
                    columnOrderList={tableViewState.columns.taskIds}
                    chooseCardToSelect={chooseCardToSelectAction}
                    selectedCardsList={selectedCardsListState}
                    chooseAllCardAction={chooseAllCardAction}
                    selectedAllCardsState={selectedAllCardsState}
                    deleteOneCardsAction={deleteOneCardsAction}
                    showViewDetailsPanelAction={showViewDetailsPanelAction}
                    activeFilterValueState={activeFilterValueState}
                    changeOrderDirectionAction={changeOrderDirectionAction}
                    userPushToRouteAction={userPushToRouteAction}
                />
            </div>
        );
    } else {
        return cardListState.results.map( ( e, i ) => {
            const props = {
                element: e,
                img: e.listingImage || e.profileImage,
                cardURL: e.listingURL || e.profileURL,
                titleHeader: e.platform.name,
                name:
          e.listingTitle ||
          ( e.profileName || ( e.profile && e.profile.profileName ) ) ||
          "",
                date: e.createdAt,
                text: e.profileDescription || e.listingDescription || "No description",
                profile: e.profileName || ( e.profile && e.profile.profileName ) || "",
                posts: e.profilePosts || ( e.profile && e.profile.profilePosts ) || "0",
                views: e.profilePosts || ( e.profile && e.profile.profileViews ) || "0",
                imgHeader: e.platform.code,
                status: {
                    mark: ( e.mark && e.mark.name ) || null,
                    infringementType:
            ( e.infringementType && e.infringementType.name ) || null,
                    enforcementStatus:
            ( e.enforcementStatus && e.enforcementStatus.name ) || null
                },
                chooseCardToSelect: chooseCardToSelectAction,
                selectedCardsList: selectedCardsListState,
                deleteOneCardsAction: deleteOneCardsAction,
                showViewDetailsPanelAction: showViewDetailsPanelAction,
                userPushToRouteAction: userPushToRouteAction,
                followers: e.profileFollowers || "0"
            };
            if ( activeModuleState === "profiles" ) {
                return (
                    <div className="col-lg-4 col-md-4" key={i}>
                        <ProfileCardItem {...props} />
                    </div>
                );
            }
            if ( pageCardDisplayViewState === "block-description-x1" ) {
                return (
                    <div className="col-lg-4 col-md-4" key={i}>
                        <CardItem {...props} />
                    </div>
                );
            }
            if ( pageCardDisplayViewState === "block-description-x2" ) {
                return (
                    <div className="col-lg-3 col-md-3" key={i}>
                        <CardItem {...props} />
                    </div>
                );
            }
            if ( pageCardDisplayViewState === "block-img-x1" ) {
                return (
                    <div className="col-lg-4 col-md-3" key={i}>
                        <CardItemImageView {...props} />
                    </div>
                );
            }
            if ( pageCardDisplayViewState === "block-img-x2" ) {
                return (
                    <div className="col-lg-2 col-md-3" key={i}>
                        <CardItemImageView
                            addClassName={"small"}
                            {...props}
                            titleHide={true}
                        />
                    </div>
                );
            }
            if ( pageCardDisplayViewState === "block-img-x3" ) {
                return (
                    <div className="card-image-view-small-wrapper" key={i}>
                        <CardItemImageViewSmall {...props} />
                    </div>
                );
            }
            return (
                <div className="col-lg-4 col-md-4" key={i}>
                    <CardItem {...props} />
                </div>
            );
        } );
    }
};
