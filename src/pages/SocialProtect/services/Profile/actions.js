import { createAction } from "redux-actions";

export const chooseAllProfileSaveAction = createAction(
    "chooseAllProfileSaveAction"
);
export const saveProfileListAction = createAction( "saveProfileListAction" );
export const selectedProfileSaveAction = createAction(
    "selectedProfileSaveAction"
);
