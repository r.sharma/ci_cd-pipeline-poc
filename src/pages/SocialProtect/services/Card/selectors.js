import { createSelector } from "reselect";

export const cardListLoaderSelector = createSelector(
    state => state.socialProtect.options.cardListLoader,
    state => state
);

// This does not do any filtering or mutations, unlike filteredCardListDataSelector itself
export const cardListDataSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;

        return state.socialProtect[activeModuleState].card_list_data;
    },
    state => state
);

export const filteredCardListDataSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        const {
            search_title = null,
            search_profile_name = null
        } = state.socialProtect[activeModuleState];
        if ( search_title ) {
            const newList = [];
            state.socialProtect[activeModuleState].card_list_data.results.forEach( el => {
                if (
                    ( el.listingTitle &&
            el.listingTitle.toLowerCase().search( search_title.toLowerCase() ) !==
              -1 ) ||
          ( el.listingDescription &&
            el.listingDescription
                .toLowerCase()
                .search( search_title.toLowerCase() ) !== -1 )
                ) {
                    newList.push( el );
                }
            } );
            return { totalCount: newList.length, results: newList };
        } else if ( search_profile_name ) {
            const newList = [];
            state.socialProtect[activeModuleState].card_list_data.results.forEach( el => {
                if (
                    ( el.profileName &&
            el.profileName
                .toLowerCase()
                .search( search_profile_name.toLowerCase() ) !== -1 ) ||
          ( el.profileDescription &&
            el.profileDescription
                .toLowerCase()
                .search( search_profile_name.toLowerCase() ) !== -1 )
                ) {
                    newList.push( el );
                }
            } );
            return { totalCount: newList.length, results: newList };
        } else {
            return state.socialProtect[activeModuleState].card_list_data;
        }
    },
    state => state
);

export const selectedCardsList = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        return state.socialProtect[activeModuleState].selected_card.arrID;
    },
    state => state
);

export const selectedAllCardsStateSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        return state.socialProtect[activeModuleState].selected_card.selectAll;
    },
    state => state
);
