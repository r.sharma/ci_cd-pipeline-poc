import { connect } from "react-redux";
import { SideBar as component } from "./SideBar";

import {
    activeModuleStateSelector,
    sideBarStateSelector
} from "services/UserView/selectors";

import { sideBarStateChangeAction } from "services/UserView/actions";

import {
    changeSearchWordTitleAction,
    changeSearchWordProfileAction,
    filterValuesInObjectSaveAction,
    searchByFilterValueAction,
    searchByFilterInTimeAction
} from "pages/SocialProtect/services/Options/actions";

import {
    getSearchWordTitleSelector,
    getSearchWordProfileNameSelector,
    activeFilterValueSelector
} from "pages/SocialProtect/services/Options/selectors";

import {
    toggleExportDataModalIsOpenAction,
    toggleImportModalStateAction
} from "services/Modal/actions";

import {
    exportModalSelector,
    importModalSelector
} from "services/Modal/selectors";

import { exportDataAction } from "./services/ExportData/actions";

import {
    exportErrorLinksToCsvAction,
    importDataAction,
    sendImportFileAction,
    importDataSaveAction,
    clearImportStoreAction,
    copyErrorLinksImportAction,
    sendOnlySuccessfullyImportDataAction,
    goToThePostsAfterImportAction
} from "./services/ImportData/actions";

import {
    importFileStateSelector,
    dataAfterImportSelector
} from "./services/ImportData/selectors";

import {
    statusListSelector,
    statusListLoaderSelector
} from "services/Status/selectors";

import {
    platformListLoaderSelector,
    platformListSelector
} from "services/Platform/selectors";

import {
    labelListIsLoadingSelector,
    labelListSelector
} from "services/Label/selectors";

const mapDispatchToProps = {
    sideBarStateChangeAction,
    changeSearchWordProfileAction,
    filterValuesInObjectSaveAction,
    changeSearchWordTitleAction,
    searchByFilterValueAction,
    searchByFilterInTimeAction,
    toggleExportDataModalIsOpenAction,
    exportDataAction,
    toggleImportModalStateAction,
    importDataAction,
    sendImportFileAction,
    importDataSaveAction,
    clearImportStoreAction,
    copyErrorLinksImportAction,
    sendOnlySuccessfullyImportDataAction,
    exportErrorLinksToCsvAction,
    goToThePostsAfterImportAction
};

const mapStateToProps = state => {
    const activeModuleState = activeModuleStateSelector( state );
    const sideBarIsOpen = sideBarStateSelector( state ) || false;
    const searchWordTitleState = getSearchWordTitleSelector( state );
    const searchWordProfileState = getSearchWordProfileNameSelector( state );
    const statusListState = statusListSelector( state );
    const statusListLoaderState = statusListLoaderSelector( state );
    const platformListLoaderState = platformListLoaderSelector( state );
    const platformListState = platformListSelector( state );
    const activeFilterValueState = activeFilterValueSelector( state );
    const labelListIsLoadingState = labelListIsLoadingSelector( state );
    const labelListState = labelListSelector( state );
    const exportModalState = exportModalSelector( state );
    const importModalState = importModalSelector( state );
    const importFileState = importFileStateSelector( state );
    const dataAfterImportState = dataAfterImportSelector( state );
    return {
        activeModuleState,
        sideBarIsOpen,
        searchWordTitleState,
        searchWordProfileState,
        statusListState,
        statusListLoaderState,
        platformListLoaderState,
        platformListState,
        activeFilterValueState,
        labelListIsLoadingState,
        labelListState,
        exportModalState,
        importModalState,
        importFileState,
        dataAfterImportState
    };
};

export const SideBar = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
