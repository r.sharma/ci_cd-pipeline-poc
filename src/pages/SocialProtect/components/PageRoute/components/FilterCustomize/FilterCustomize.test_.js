import React from "react";
import { shallow } from "enzyme";
import { SpFilterCustomize } from "./";
import { statusVariables } from "services/Status";

describe( "SpFilterCustomize", () => {
    it( "renders 1 <FilterCustomize /> component", () => {
        const component = shallow( <SpFilterCustomize /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const props = {
            defaultStatus: "Unhandled",
            postCount: null,
            activeView: "activeView",
            pageViewChange: "pageViewChange"
        };
        const component = shallow( <SpFilterCustomize {...props} /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
        expect( component.props().className ).toEqual( "filter-customize" );

        const customizeFilter = component.find( ".filter-customize__filter" );

        expect( customizeFilter.childAt( 0 ).props().data ).toEqual( statusVariables );
        expect( customizeFilter.childAt( 0 ).props().defaultValue ).toEqual(
            props.defaultStatus
        );
        expect( customizeFilter.childAt( 0 ).props().wrapperClassName ).toEqual(
            "filter-dropdown"
        );
        expect( customizeFilter.childAt( 0 ).props().widthBtn ).toEqual( "120px" );
        expect( customizeFilter.childAt( 0 ).props().label ).toEqual( "Status:" );

        expect( customizeFilter.childAt( 1 ).props().data ).toEqual( statusVariables );
        expect( customizeFilter.childAt( 1 ).props().defaultValue ).toEqual( "All" );
        expect( customizeFilter.childAt( 1 ).props().wrapperClassName ).toEqual(
            "filter-dropdown"
        );
        expect( customizeFilter.childAt( 1 ).props().widthBtn ).toEqual( undefined );
        expect( customizeFilter.childAt( 1 ).props().label ).toEqual( "Platform:" );

        expect( component.find( "PostCounter" ).props().postCount ).toEqual(
            props.postCount
        );
        expect(
            component.find( "filter-customize__customize" ).children.length
        ).toEqual( 1 );

        expect( component.find( "ToggleView" ) ).toHaveLength( 1 );
        expect( component.find( "ToggleView" ).props().active ).toEqual(
            props.activeView
        );
        expect( component.find( "ToggleView" ).props().pageViewChange ).toEqual(
            props.pageViewChange
        );
    } );
} );
