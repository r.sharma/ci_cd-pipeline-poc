import { serviceModule } from "pages/SocialProtect/constants";

export const serviceModuleState = null;
export const serviceDefinition = {
    serviceModule,
    serviceModuleState
};