import React, { PureComponent } from "react";
import { Loader, OutlinedChips } from "components";
import Chip from "@material-ui/core/Chip";
import { FilterCustomize } from "./components/";
import { CardView } from "./components/";
import { Footer } from "pages/SocialProtect/components";
import "./Page.scss";

export class PageRoute extends PureComponent {
    handleChangeLimitPage = limit => {
        const { cardListState, searchByFilterInTimeAction } = this.props;
        if ( cardListState.limit === Number( limit ) ) return;
        searchByFilterInTimeAction( { limit, page: 1 } );
    };

    handlePageChange = ( { selected } ) => {
        const { cardListState } = this.props;
        if ( cardListState.page === selected + 1 ) return;
        let newPage = selected + 1;
        this.checkFilter( "page", newPage );
    };

    checkFilter = ( label, id ) => {
        const { searchByFilterInTimeAction, activeFilterValueState } = this.props;
        if ( activeFilterValueState[label] === id ) return;
        const newFilter = { ...activeFilterValueState };
        newFilter[label] = id;
        searchByFilterInTimeAction( newFilter );
    };

    render() {
        const {
            cardListState,
            activeModuleState,
            pageViewState,
            pageListViewChangeAction,
            cardListLoaderState,
            loadMoreAction,
            getLoadMoreLoaderState,
            activeFilterValueState,
            statusListLoaderState,
            platformListState,
            platformListLoaderState,
            activeBrandState,
            statusListState,
            labelListState,
            removeChipsAction
        } = this.props;
        const dataChips = {
            platform: platformListState,
            postMark: labelListState,
            enforcement: statusListState
        };
        const clearAll = Object.keys( activeFilterValueState ).filter(
            ( e ) => e !== "page" && e !== "limit" && e !== "orderDirection" && activeFilterValueState[e]
        ).length > 1;
        return (
            <div>
                {/*FilterCustomize start*/}
                <FilterCustomize
                    activeModuleState={activeModuleState}
                    cardListState={cardListState}
                    activeView={pageViewState}
                    pageViewChange={pageListViewChangeAction}
                    activeFilterValueState={activeFilterValueState}
                    platformListLoaderState={platformListLoaderState}
                    platformListState={platformListState}
                    statusListLoaderState={statusListLoaderState}
                    statusListState={statusListState}
                    handleChangeLimitPage={this.handleChangeLimitPage}
                    handleChangeFilter={this.checkFilter}
                />
                {/*FilterCustomize end*/}

                {/*OutlinedChips start*/}
                <OutlinedChips
                    onCloseAll={ () => removeChipsAction( null ) }
                    clearAll = {clearAll}
                >
                    {
                        Object.keys( activeFilterValueState ).map( ( e, index ) => {
                            let label = activeFilterValueState[e];
                            if( !activeFilterValueState[e] || e === "page" || e === "limit" || e === "orderDirection" ) return null;
                            if( e === "platform" || e === "postMark" || e === "enforcement" ){
                                if( !dataChips[e] ) return  null;
                                label = dataChips[e].filter( ( e ) => +e.id === +label )[0].name;
                            }
                            return (
                                <Chip
                                    key={index}
                                    label={label}
                                    onDelete={ ()=> removeChipsAction( e ) }
                                    variant="outlined"
                                />
                            );
                        } )
                    }
                </OutlinedChips>
                {/*OutlinedChips end*/}

                {/*Mapping Cards start*/}
                <div className="row">
                    {cardListLoaderState || !activeBrandState ? (
                        <div className="col-lg-12">
                            <Loader style={{ margin: "20% auto" }} />
                        </div>
                    ) : cardListState ? (
                        <CardView {...this.props} />
                    ) : null}
                </div>
                {/*Mapping Cards end*/}

                {/*Footer start*/}
                {!cardListLoaderState ? (
                    <Footer
                        cardListState={cardListState}
                        handlePageChange={this.handlePageChange}
                        handleLoadMore={loadMoreAction}
                        getLoadMoreLoaderState={getLoadMoreLoaderState}
                    />
                ) : null}
                {/*Footer end*/}
            </div>
        );
    }
}
