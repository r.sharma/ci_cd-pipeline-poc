import React from "react";
import { shallow } from "enzyme";
import { Button } from "./Button";

describe( "Button", () => {
    it( "renders 1 <Button /> component", () => {
        const component = shallow( <Button /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <Button /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check disabled", () => {
        const component = shallow( <Button disabled={true} /> );
        expect( component.props().disabled ).toBe( true );
    } );

    describe( "check className default", () => {
        const component = shallow( <Button /> );
        expect( component.props().className ).toEqual(
            "btn-type-default btn-color-primary btn-size-large"
        );
    } );

    describe( "check className circle", () => {
        const component = shallow( <Button type="circle" /> );
        expect( component.props().className ).toEqual(
            "btn-type-circle btn-color-primary btn-size-large"
        );
    } );

    describe( "check className secondary", () => {
        const component = shallow( <Button color="secondary" /> );
        expect( component.props().className ).toEqual(
            "btn-type-default btn-color-secondary btn-size-large"
        );
    } );

    describe( "check className small", () => {
        const component = shallow( <Button size="small" /> );
        expect( component.props().className ).toEqual(
            "btn-type-default btn-color-primary btn-size-small"
        );
    } );

    describe( "check text", () => {
        const component = shallow( <Button text="text" /> );
        expect( component.text() ).toEqual( "text" );
    } );

    describe( "check add Icon", () => {
        const component = shallow( <Button add={true} /> );
        expect( component.children().length ).toEqual( 1 );
        expect( component.children().type() ).toEqual( "svg" );
    } );
} );
