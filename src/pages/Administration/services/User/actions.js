import { createAction } from "redux-actions";

export const saveUsersDetailIdentifier = createAction(
    "saveUsersDetailIdentifier"
);
export const saveUsersDetailItem = createAction( "saveUsersDetailItem" );
export const getUsersListAction = createAction( "getUsersListAction" );
export const saveUsersList = createAction( "saveUsersList" );
export const setLoadingUsersList = createAction( "setLoadingUsersList" );
export const setLoadingUsersDetail = createAction( "setLoadingUsersDetail" );
