import React from "react";
import { storiesOf } from "@storybook/react";
import { RadioInput } from "./RadioInput";

storiesOf( "RadioInput", module )
    .add( "normal state", () => (
        <RadioInput name="checkbox" label="Label-1" value={false} />
    ) )
    .add( "active state", () => (
        <RadioInput name="checkbox" label="Label-1" value={true} />
    ) );
