import { createAction } from "redux-actions";

export const loadStatusListAction = createAction( "loadStatusListAction" );
export const saveStatusListAction = createAction( "saveStatusListAction" );
