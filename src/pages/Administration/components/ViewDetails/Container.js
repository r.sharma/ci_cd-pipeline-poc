import { connect } from "react-redux";
import { ViewDetails as component } from "./ViewDetails";

import { activeModuleStateSelector } from "services/UserView/selectors";
import { showViewDetailsPanelAction } from "services/UserView/actions";
import {
    userDetailOverviewSelector,
    userDetailRolesSelector,
    userDetailBrandsSelector
} from "pages/Administration/services/User/selectors";

const mapDispatchToProps = { showViewDetailsPanelAction };

const mapStateToProps = state => {
    const activeModuleState = activeModuleStateSelector( state );

    return {
        activeModuleState,
        userOverview: userDetailOverviewSelector( state ),
        userRoles: userDetailRolesSelector( state ),
        userBrands: userDetailBrandsSelector( state )
    };
};

export const ViewDetails = connect(
    mapStateToProps,
    mapDispatchToProps,
)( component );
