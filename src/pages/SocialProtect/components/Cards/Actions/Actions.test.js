import React from "react";
import { shallow, mount } from "enzyme";
import { CardActions } from "./Actions";

describe( "CardActions", () => {
    it( "renders 1 <CardActions /> component", () => {
        const component = shallow( <CardActions /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const component = shallow( <CardActions horizontally={true} /> );
        const props = component.instance().props;

        expect( props.horizontally ).toEqual( true );
    } );

    describe( "check add Icon", () => {
        const component = shallow( <CardActions /> );
        expect( component.children().length ).toEqual( 2 );
        expect(
            component
                .children()
                .find( "span" )
                .children()
                .type()
        ).toEqual( "svg" );
    } );

    describe( "check add Icon style default", () => {
        const component = shallow( <CardActions /> );
        expect(
            component
                .children()
                .find( "span" )
                .prop( "style" )
        ).toEqual( null );
    } );

    describe( "check add Icon style", () => {
        const component = shallow( <CardActions horizontally={true} /> );
        expect(
            component
                .children()
                .find( "span" )
                .prop( "style" )
        ).toHaveProperty( "transform", "rotate(90deg)" );
    } );

    describe( "it change state on click span", () => {
        const component = mount( <CardActions /> );
        expect( component.state().open ).toBe( false );
        const spanBtn = component.find( "span" );

        spanBtn.simulate( "click" );
        expect( component.state().open ).toBe( true );
        expect(
            component
                .find( "div" )
                .at( 0 )
                .hasClass( "show" )
        ).toBe( true );
        spanBtn.simulate( "click" );

        expect( component.state().open ).toBe( false );
        expect(
            component
                .find( "div" )
                .at( 0 )
                .hasClass( "show" )
        ).toBe( false );
    } );

    describe( "testing component functions", () => {
        const component = mount( <CardActions /> );
        expect( component.state().open ).toBe( false );

        const spanBtn = component.find( "span" );
        spanBtn.simulate( "click" );
        expect( component.state().open ).toBe( true );

        const outerNode = document.createElement( "div" );
        outerNode.className = "outerDiv";
        document.body.appendChild( outerNode );

        outerNode.dispatchEvent( new Event( "click", { bubbles: true } ) );
        expect( component.state().open ).toBe( false );
    } );
} );
