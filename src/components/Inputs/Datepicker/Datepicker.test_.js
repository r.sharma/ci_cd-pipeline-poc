import React from "react";
import { shallow, mount } from "enzyme";
import { Datepicker } from "./Datepicker";

describe( "Datepicker", () => {
    it( "renders 1 <Datepicker /> component", () => {
        const component = shallow( <Datepicker /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const date = new Date();
        const component = mount( <Datepicker value={date} /> );
        const props = component.instance().props;

        expect( props.value ).toEqual( date );
    } );
} );
