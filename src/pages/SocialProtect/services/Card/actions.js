import { createAction } from "redux-actions";

export const getCardListAction = createAction( "getCardListAction" );
export const saveCardListAction = createAction( "saveCardListAction" );
export const loadCardListAction = createAction( "loadCardListAction" );
export const chooseCardToSelectAction = createAction(
    "chooseCardToSelectAction"
);
export const selectedCardSaveAction = createAction( "selectedCardSaveAction" );
export const chooseAllCardAction = createAction( "chooseAllCardAction" );
export const chooseAllCardSaveAction = createAction( "chooseAllCardSaveAction" );
export const deleteOneCardsAction = createAction( "deleteOneCardsAction" );
export const addCardItemAction = createAction( "addCardItemAction" );
export const updateCardItemAction = createAction( "updateCardItemAction" );
export const deleteAllSelectedCardsAction = createAction(
    "deleteAllSelectedCardsAction"
);
export const loadMoreAction = createAction( "loadMoreAction" );
export const loadMoreLoaderAction = createAction( "loadMoreLoaderAction" );
export const changeOrderDirectionAction = createAction(
    "changeOrderDirectionAction"
);
