import { createSelector } from "reselect";

/*
export const adminViewDetailItemDataSelector = createSelector(
    state => {
        const {activeModuleState} = state.main.userView;
        const id = state.admin[activeModuleState].item_view_detail;

        if (!id) return null;

        const currentItem = state.admin[activeModuleState].card_list_data.results.filter(e => e.id === id)[0];

        return {
            data: currentItem,
            index: state.admin[activeModuleState].card_list_data.results.indexOf(currentItem),
            lengthList: state.admin[activeModuleState].card_list_data.results.length,
        };
    },
    state => state,
);
*/

export const userDetailOverviewSelector = createSelector(
    state => {
        if ( !state.admin.users.detail ) return null;

        return state.admin.users.detail.overview;
    },
    state => state
);

export const userDetailRolesSelector = createSelector(
    state => {
        if ( !state.admin.users.detail ) return null;

        return state.admin.users.detail.roles;
    },
    state => state
);

export const userDetailBrandsSelector = createSelector(
    state => {
        if ( !state.admin.users.detail ) return null;

        return state.admin.users.detail.brands;
    },
    state => state
);

export const usersListSelector = createSelector(
    state => {
        return state.admin.users.data;
    },
    state => state
);
