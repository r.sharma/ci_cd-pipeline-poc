import React from "react";
import { shallow } from "enzyme";
import { GridTableViewTopBar } from "./GridTableViewTopBar";
import { listEnabledItem } from "services/Post/models";
import { fakeData } from "../../fakeDataCardList";

describe( "GridTableViewTopBar", () => {
    it( "renders 1 <GridTableViewTopBar /> component", () => {
        const component = shallow( <GridTableViewTopBar /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        let props = {
            data: fakeData.results,
            tableView: "large",
            columnSelected: listEnabledItem,
            selectedAllCardsState: false
        };
        const component = shallow( <GridTableViewTopBar {...props} /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );

        it( "chack component", () => {
            expect(
                component
                    .find( "div" )
                    .at( 0 )
                    .hasClass( "large" )
            ).toBeTruthy();

            expect( component.find( "table thead" ) ).toHaveLength( 0 );
            const trFirst = component.find( "table tbody tr" ).at( 0 );
            const label =
        fakeData.results[0].profileName ||
        ( fakeData.results[0].profile && fakeData.results[0].profile.name ) ||
        "No title";
            expect(
                trFirst
                    .find( "RowLinkView" )
                    .at( 0 )
                    .props().label
            ).toEqual( label );
            const profilePosts =
        fakeData.results[0].profilePosts ||
        ( fakeData.results[0].profile &&
          fakeData.results[0].profile.profilePosts ) ||
        0;
            expect(
                trFirst
                    .find( "td" )
                    .at( 2 )
                    .text()
            ).toEqual( profilePosts.toString() );
            const profileViews =
        fakeData.results[0].profileViews ||
        ( fakeData.results[0].profile &&
          fakeData.results[0].profile.profileViews ) ||
        0;
            expect(
                trFirst
                    .find( "td" )
                    .at( 3 )
                    .text()
            ).toEqual( profileViews.toString() );
            const description =
        fakeData.results[0].listingDescription ||
        fakeData.results[0].profileDescription ||
        "No Description";
            expect(
                trFirst
                    .find( "td" )
                    .at( 4 )
                    .text()
            ).toEqual( description );
            expect( trFirst.find( "StatusIcons" ) ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 6 )
                    .children()
            ).toHaveLength( 1 );
            expect(
                trFirst
                    .find( "td" )
                    .at( 6 )
                    .childAt( 0 )
                    .type()
            ).toBe( "button" );
        } );
    } );
} );
