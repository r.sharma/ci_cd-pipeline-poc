import { createSelector } from "reselect";

export const globalErrorSelector = createSelector(
    state => state.errors.errorMessageGlobal,
    state => state
);
