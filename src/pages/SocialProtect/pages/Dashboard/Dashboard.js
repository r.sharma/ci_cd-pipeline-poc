import React, { PureComponent } from "react";
import {
    SideBar,
    Header,
    PageRoute,
    ViewDetails
} from "pages/SocialProtect/components";
import { OptionsTopBar } from "./components";
import { TabBar, NoMatch } from "components";
import { Route, Switch, Redirect, NavLink } from "react-router-dom";

export class Dashboard extends PureComponent {
    constructor( props ) {
        super( props );
        const { match } = this.props;
        this.baseUrl =
        match.url[match.url.length - 1] === "/" ? match.url : match.url + "/";
    }

    render() {
        const { sideBarIsOpen, activeModuleState } = this.props;
        let pageClassName = "page bg-primary-transparent";

        if ( sideBarIsOpen ) pageClassName += " with-open-side-bar";

        if ( !activeModuleState ) {
            return (
                <Switch>
                    <Route
                        exact
                        path={this.baseUrl}
                        render={() => <Redirect to={`${this.baseUrl}posts`} />}
                    />
                    <Route component={NoMatch} />
                </Switch>
            );
        }

        return (
            <div>
                {/*SideBar start*/}
                <SideBar />
                {/*SideBar end*/}

                {/*SpOptionsTopBar start*/}
                <OptionsTopBar />
                {/*SpOptionsTopBar end*/}

                <div className={pageClassName}>
                    <div className="page-container scroll-m">
                        {/*Header start*/}
                        <Header title="Social Protect">
                            <TabBar>
                                <NavLink to="/social-protect/posts" activeClassName="active">
                                    Posts
                                </NavLink>
                                <NavLink to="/social-protect/profiles" activeClassName="active">
                                    Profiles
                                </NavLink>
                            </TabBar>
                        </Header>
                        {/*Header end*/}

                        {/*View Details start*/}
                        <ViewDetails />
                        {/*View Details end*/}

                        <Switch>
                            <Route path={`${this.baseUrl}posts`} component={PageRoute} />
                            <Route path={`${this.baseUrl}profiles`} component={PageRoute} />
                            <Route component={NoMatch} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}
