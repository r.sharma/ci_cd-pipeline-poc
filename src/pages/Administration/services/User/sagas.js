import { takeLatest, call, put } from "redux-saga/effects";
import {
    saveUsersDetailIdentifier,
    saveUsersDetailItem,
    getUsersListAction,
    saveUsersList,
    setLoadingUsersList,
    setLoadingUsersDetail
} from "./actions";
import { activeServiceValidationWrapper } from "services/UserView/sagas";
import { activeBrandSwitchedAction } from "services/Brand/actions";
import { setActiveModuleStateAction, showViewDetailsPanelAction } from "services/UserView/actions";
import { serviceDefinition } from "./constants";

import * as API from "./fixtures";

// WORKERS
function* getUsersListWorker( { payload } ) {
    yield put( setLoadingUsersList( true ) );

    const response = yield call( API.getUsersList, payload );

    if ( response ) {
        yield put( saveUsersList( { ...response } ) );
    }

    yield put( setLoadingUsersList( false ) );
}

function* saveUserDetailWorker( { payload } ) {
    // If we are resetting it
    if ( !payload ) {
        yield put( saveUsersDetailItem( { data: null } ) );
        return;
    }

    const { value } = payload;
    yield put( setLoadingUsersDetail( true ) );

    const response = yield call( API.getUsersDetail, { id: value } );

    if ( response ) {
        yield put( saveUsersDetailItem( { ...response } ) );
    }

    yield put( setLoadingUsersDetail( false ) );
}

function* activeBrandSwitchedWorker( id ) {
    yield put( getUsersListAction() );
}

function* setActiveModuleWorker( activeModuleState ) {
    yield put( getUsersListAction() );
}

function* adminUsersShowViewDetailsPanelWorker( { payload } ) {
    yield put( saveUsersDetailIdentifier( payload ) );
}

// WATCHERS
export function* saveUsersDetailWatcher() {
    yield takeLatest( saveUsersDetailIdentifier, saveUserDetailWorker );
}
export function* getUsersListWatcher() {
    yield takeLatest( getUsersListAction, getUsersListWorker );
}
export function* adminUsersActiveBrandSwitchedWatcher() {
    yield takeLatest(
        activeBrandSwitchedAction,
        activeServiceValidationWrapper,
        {
            ...serviceDefinition,
            worker: activeBrandSwitchedWorker
        }
    );
}
export function* adminUsersSetActiveModuleWatcher() {
    yield takeLatest(
        setActiveModuleStateAction,
        activeServiceValidationWrapper,
        {
            ...serviceDefinition,
            worker: setActiveModuleWorker
        }
    );
}
export function* adminUsersShowViewDetailsPanelWatcher() {
    yield takeLatest(
        showViewDetailsPanelAction,
        activeServiceValidationWrapper,
        {
            ...serviceDefinition,
            worker: adminUsersShowViewDetailsPanelWorker
        }
    );
}