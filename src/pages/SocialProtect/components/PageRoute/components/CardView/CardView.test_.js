import React from "react";
import { shallow, mount } from "enzyme";
import { SpCardView } from "./";
import { listEnabledItem, columnsPosts } from "services/Post";
import { fakeData, fakeDataProfile } from "../../../../../fakeDataCardList";

describe( "Avatar", () => {
    it( "renders 1 <CardView /> component", () => {
        const component = shallow( <SpCardView /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <SpCardView size="35" /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check count children", () => {
        const props = {
            cardListState: null,
            pageViewState: "row",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-img-x2"
        };

        const component = shallow( <SpCardView {...props} /> );
        expect( component.children ).toHaveLength( 1 );
    } );

    describe( "check count children 2", () => {
        const props = {
            cardListState: fakeData,
            pageViewState: "row",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-img-x2"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "GridTableView" ) ).toHaveLength( 1 );
        expect( component.find( "GridTableView" ).props().data ).toEqual(
            props.cardListState.results
        );
        expect( component.find( "GridTableView" ).props().tableView ).toEqual(
            props.tableViewState.view
        );
        expect( component.find( "GridTableView" ).props().columnSelected ).toEqual(
            props.tableViewState.list_enabled_item
        );
        expect( component.find( "GridTableView" ).props().selectedCardsList ).toEqual(
            props.selectedCardsListState
        );
        expect(
            component.find( "GridTableView" ).props().selectedAllCardsState
        ).toEqual( props.selectedAllCardsState );
    } );

    describe( "check count children 3", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-img-x2",
            activeModuleState: "profiles"
        };
        const e = props.cardListState.results[0];

        const obj = {
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "ProfileCardItem" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "ProfileCardItem" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );

    describe( "check count children 4", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-description-x1",
            activeModuleState: "posts"
        };
        const e = props.cardListState.results[0];

        const obj = {
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "CardItem" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "CardItem" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );

    describe( "check count children 5", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-description-x2",
            activeModuleState: "posts"
        };
        const e = props.cardListState.results[0];

        const obj = {
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "CardItem" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "CardItem" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );

    describe( "check count children 6", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-img-x1",
            activeModuleState: "posts"
        };
        const e = props.cardListState.results[0];

        const obj = {
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "CardItemImageView" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "CardItemImageView" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );

    describe( "check count children 7", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: "block-img-x2",
            activeModuleState: "posts"
        };
        const e = props.cardListState.results[0];

        const obj = {
            addClassName: "small",
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0",
            titleHide: true
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "CardItemImageView" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "CardItemImageView" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );

    describe( "check count children 8", () => {
        const props = {
            cardListState: fakeDataProfile,
            pageViewState: "block",
            tableViewState: {
                columns: columnsPosts,
                list_enabled_item: listEnabledItem
            },
            selectedCardsListState: [],
            selectedAllCardsState: false,
            pageCardDisplayViewState: null,
            activeModuleState: "posts"
        };
        const e = props.cardListState.results[0];

        const obj = {
            element: e,
            img: e.listingImage || e.profileImage,
            titleHeader: e.platform.name,
            name: e.listingTitle,
            date: e.createdAt,
            text: e.profileDescription || "No description",
            channel: e.profileName || "",
            posts: e.profilePosts || "0",
            views: e.profilePosts || "0",
            imgHeader: e.platform.code,
            status: "Non Required",
            chooseCardToSelect: undefined,
            selectedCardsList: [],
            deleteOneCardsAction: undefined,
            followers: e.profileFollowers || "0"
        };
        const component = mount( <SpCardView {...props} /> );
        expect( component.find( "CardItem" ) ).toHaveLength( 8 );
        expect(
            component
                .find( "CardItem" )
                .at( 0 )
                .props()
        ).toEqual( obj );
    } );
} );
