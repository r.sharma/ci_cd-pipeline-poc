import { serviceModule } from "pages/Administration/constants";

export const serviceModuleState = "users";
export const serviceDefinition = {
    serviceModule,
    serviceModuleState
};