import { createAction } from "redux-actions";

export const toggleExportDataModalIsOpenAction = createAction(
    "toggleExportDataModalIsOpenAction"
);
export const toggleImportModalStateAction = createAction( "toggleImportModalStateAction" );
export const setAlertModalIsOpenAction = createAction( "setAlertModalIsOpenAction" );
export const setAlertModalConfigurationAction = createAction( "setAlertModalConfigurationAction" );
