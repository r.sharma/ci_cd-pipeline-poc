import React from "react";
import { shallow } from "enzyme";
import { SpPage } from "./PageRoute";
import { fakeData } from "../../../../fakeDataCardList";

describe( "SpPageRoute", () => {
    it( "renders 1 <SpPageRoute /> component", () => {
        const component = shallow( <SpPage /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const props = {
            cardListLoaderState: true,
            cardListState: fakeData,
            pageViewState: "row",
            pageListViewChangeAction: () => {}
        };

        const component = shallow( <SpPage {...props} /> );
        expect( component.find( ".loader" ) ).toHaveLength( 1 );
    } );

    describe( "check props in component 2", () => {
        const props = {
            cardListLoaderState: false,
            cardListState: fakeData,
            pageViewState: "row",
            pageListViewChangeAction: () => {}
        };

        const component = shallow( <SpPage {...props} /> );
        expect( component.find( "SpFilterCustomize" ) ).toHaveLength( 1 );
        const postCount = props.cardListState
            ? props.cardListState.totalCount
            : null;
        expect( component.find( "SpFilterCustomize" ).props().postCount ).toEqual(
            postCount
        );
        expect( component.find( "SpFilterCustomize" ).props().activeView ).toEqual(
            props.pageViewState
        );

        expect( component.find( "SpCardView" ) ).toHaveLength( 1 );

        expect( component.find( "SpFooter" ) ).toHaveLength( 1 );
    } );
} );
