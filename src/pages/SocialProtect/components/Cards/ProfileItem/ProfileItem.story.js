import React from "react";
import { storiesOf } from "@storybook/react";
import { ProfileCardItem } from "./ProfileItem";
import { Avatar } from "components";

storiesOf( "Card Item", module ).add( "normal state", () => (
    <div className="row">
        <div className="col">
            <ProfileCardItem
                img={
                    "http://cache.desktopnexus.com/thumbseg/1998/1998280-bigthumbnail.jpg"
                }
                imgHeader={{
                    src:
            "https://icon2.kisspng.com/20180410/rzw/kisspng-logo-computer-icons-youtube-symbol-instagram-logo-5acd6f97777305.1128861915234128874893.jpg", //eslint-disable-line
                    title: "test"
                }}
                titleHeader="Instagram"
                name="Tremblant In Canada"
                date="04-21-2018"
        text="Can I buy a replica Nike Cal rugby jersey online? Can I buy a replica Nike Cal rugby jersey online? Can I buy a replica Nike Cal rugby jersey online? Can I buy a replica Nike Cal rugby jersey online? " //eslint-disable-line
                channel="Fátima Cambeiro"
                posts="23"
                views="23"
                status="Unhandled"
            />
        </div>
        <div className="col">
            <ProfileCardItem
                titleHeader="Facebook"
                name="Subway Surfers"
                date="04-21-2018"
        text="I have found the Barbarian Rugby Sportswear shirt at the Cal student store, but I don't find the Nike one. Thanks for your help." //eslint-disable-line
                channel="Marama Petera"
                posts="23"
                views="23"
                status="Canceled"
            >
                <Avatar
                    size="80"
                    data={[
                        "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg",
                        "https://xboxavatargear.files.wordpress.com/2016/09/jordan-air-vector-logo.png?w=450",
                        "http://www.userlogos.org/files/logos/L0mass/Puma.white.png"
                    ]}
                />
            </ProfileCardItem>
        </div>
        <div className="col">
            <ProfileCardItem
                img={
                    "https://lh3.googleusercontent.com/Ax2wQYxjDITuZEpc6K9EDYPG7C839tb4PApia4Tmf18u8XehB-twqhVgDVPgxxExkr4=s180"
                }
                titleHeader="Youtube"
                name="Photos from Edward Ball post"
                date="04-21-2018"
        text="That feeling when you don't see people for years(9, to be exact)  hometown 😎🌞🥂 Do visit often, friends. It's such a blast! 💋" //eslint-disable-line
                channel="Ashish Asharaful"
                posts="23"
                views="23"
                status="Non Required"
            />
        </div>
        <div className="col">
            <ProfileCardItem
                img={
                    "http://cache.desktopnexus.com/thumbseg/1998/1998280-bigthumbnail.jpg"
                }
                imgHeader={{
                    src:
            "https://icon2.kisspng.com/20180410/rzw/kisspng-logo-computer-icons-youtube-symbol-instagram-logo-5acd6f97777305.1128861915234128874893.jpg", //eslint-disable-line
                    title: "test"
                }}
                titleHeader="Instagram"
                name="Tremblant In Canada"
                date="04-21-2018"
                text="Can I buy a replica Nike Cal rugby jersey online? "
                channel="Fátima Cambeiro"
                posts="23"
                views="23"
                status="Unhandled"
            />
        </div>
        <div className="col">
            <ProfileCardItem
                titleHeader="Facebook"
                name="Subway Surfers"
                date="04-21-2018"
        text="I have found the Barbarian Rugby Sportswear shirt at the Cal student store, but I don't find the Nike one. Thanks for your help." //eslint-disable-line
                channel="Marama Petera"
                posts="23"
                views="23"
                status="Canceled"
            >
                <Avatar
                    size="80"
                    data={[
                        "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg",
                        "https://xboxavatargear.files.wordpress.com/2016/09/jordan-air-vector-logo.png?w=450",
                        "http://www.userlogos.org/files/logos/L0mass/Puma.white.png"
                    ]}
                />
            </ProfileCardItem>
        </div>
        <div className="col">
            <ProfileCardItem
                img={
                    "https://lh3.googleusercontent.com/Ax2wQYxjDITuZEpc6K9EDYPG7C839tb4PApia4Tmf18u8XehB-twqhVgDVPgxxExkr4=s180"
                }
                titleHeader="Youtube"
                name="Photos from Edward Ball post"
                date="04-21-2018"
                text="That feeling when you don't see people for years(9, to be exact)  hometown 😎🌞🥂 Do visit often, friends. It's such a blast! 💋"
                channel="Ashish Asharaful"
                posts="23"
                views="23"
                status="Non Required"
            />
        </div>
    </div>
) );
