import React from "react";
import { shallow } from "enzyme";
import { PointerIcon } from "./PointerIcon";

describe( "PointerIcon", () => {
    it( "renders 1 <PointerIcon /> component", () => {
        const component = shallow( <PointerIcon /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "normal state", () => {
        const props = {
            wrapperClassName: "wrapperClassName",
            ImgClassName: "ImgClassName",
            borderRadius: "50%",
            imgAlt: "alt",
            size: "30px",
            img: "www.img.com/i.png",
            style: {
                border: "1px solid #ccc"
            }
        };
        const component = shallow( <PointerIcon {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        it( "chack component", () => {
            expect( component.props().className ).toBe( props.wrapperClassName );
            expect( component.props().style ).toBe( props.style );
            expect( component.find( "img" ).props().className ).toBe( props.ImgClassName );
            expect( component.find( "img" ).props().style ).toEqual( {
                height: props.size,
                width: props.size,
                borderRadius: props.borderRadius
            } );
            expect( component.find( "img" ).props().alt ).toBe( `icon-${props.imgAlt}` );
        } );
    } );
} );
