import React, { PureComponent } from "react";
import { ViewDetailsWrapper } from "components";
import { ViewDetailsPanel } from "./components";

const options = { capture: true };

export class ViewDetails extends PureComponent {
    constructor( props ) {
        super( props );
        this.myRef = React.createRef();
        this.state = {
            activeTab: null,
            module: null,
            optionValue: null
        };
    }

    static getDerivedStateFromProps( nextProps, prevState ) {
        if (
            nextProps.activeModuleState &&
            nextProps.activeModuleState !== prevState.module
        ) {
            if ( nextProps.activeModuleState === "posts" ) {
                return {
                    activeTab: "posts",
                    module: nextProps.activeModuleState
                };
            }
            return {
                activeTab: "profile",
                module: nextProps.activeModuleState
            };
        }
        return null;
    }

    componentDidMount() {
        document.addEventListener( "click", this.documentClick, options );
    }

    componentWillUnmount() {
        document.removeEventListener( "click", this.documentClick, options );
    }

    documentClick = e => {
        const { showViewDetailsPanelAction } = this.props;
        const node = this.myRef.current;
        if ( e.target.closest( ".view-details-panel" ) !== node ) {
            showViewDetailsPanelAction( null );
            this.closeTab();
        }
    };

    clickTab = activeTab => {
        this.setState( { activeTab } );
    };

    closePanel = () => {
        const { showViewDetailsPanelAction } = this.props;
        //close tab
        this.setOptions( null );
        //clear props
        showViewDetailsPanelAction( null );
    };

    setCurrentInfringementType = e => {
        //Save ID InfringementType in store
        const {
            selectInfringementTypeInTopBarAction,
            selectEnforcementStatusInTopBarAction
        } = this.props;
        const { optionValue } = this.state;
        if ( optionValue === "type" ) {
            selectInfringementTypeInTopBarAction( e );
        } else if ( optionValue === "status" ) {
            selectEnforcementStatusInTopBarAction( e );
        }
    };

    setOptions = optionValue => {
        this.setState( {
            optionValue: optionValue
        } );
    };

    closeList = () => {
        this.setCurrentInfringementType( null );
        this.setState( {
            optionValue: null
        } );
    };

    closeTab = () => {
        const { selectedPostMarkIdAction } = this.props;
        //Clear props
        selectedPostMarkIdAction( null );
        this.setCurrentInfringementType( null );
        //Clear state
        this.setState( { optionValue: null } );
    };

    setCurrentInfringementType = e => {
        //Save ID InfringementType in store
        const {
            selectInfringementTypeInTopBarAction,
            selectEnforcementStatusInTopBarAction
        } = this.props;
        const { optionValue } = this.state;
        if ( optionValue === "type" ) {
            selectInfringementTypeInTopBarAction( e );
        } else if ( optionValue === "status" ) {
            selectEnforcementStatusInTopBarAction( e );
        }
    };

    clickApplyInfringement = () => {
        const { optionValue } = this.state;
        const {
            selectedPostMarkIdAction,
            startEnforcementAction,
            viewDetailItemDataState
        } = this.props;
        selectedPostMarkIdAction( 1 );
        return new Promise( ( resolve, reject ) => {
            startEnforcementAction( {
                optionValue: optionValue,
                id: [ viewDetailItemDataState.data.id ],
                resolve,
                reject
            } );
        } ).then( () => {
            this.closePanel();
        } );
    };

    selectedPostMark = e => {
        const {
            selectedPostMarkIdAction,
            selectedNewPostMarkAction,
            viewDetailItemDataState
        } = this.props;
        //set value to store
        selectedPostMarkIdAction( e );
        if ( e !== 1 ) {
            //make a call to API with change values
            selectedNewPostMarkAction( {
                ids: [ viewDetailItemDataState.data.id ],
                mark_id: e
            } );
        }
    };

    render() {
        const {
            activeModuleState,
            viewDetailItemDataState,
            deleteOneCardsAction,
            goToNextViewDetailsAction,
            labelListState,
            selectedPostMarkIdState,
            startEnforcementLoaderState,
            updateCardItemAction,
            infringementTypeListState,
            infringementTypeIdState,
            statusListState,
            selectedEnforcementStatusIdState
        } = this.props;
        const { activeTab, selectInfringementType, optionValue } = this.state;
        if (
            !viewDetailItemDataState ||
            ( viewDetailItemDataState && !viewDetailItemDataState.data )
        )
            return null;
        let changeListValues = [];
        let valueList = null;
        if ( optionValue === "type" ) {
            changeListValues = infringementTypeListState;
            valueList = infringementTypeIdState;
        } else if ( optionValue === "status" ) {
            changeListValues = statusListState;
            valueList = selectedEnforcementStatusIdState;
        }
        return (
            <ViewDetailsWrapper myRef={ this.myRef } >
                <ViewDetailsPanel
                    activeModuleState={activeModuleState}
                    data={viewDetailItemDataState.data}
                    index={viewDetailItemDataState.index}
                    lengthList={viewDetailItemDataState.lengthList}
                    deleteOneCardsAction={deleteOneCardsAction}
                    goToNextViewDetailsAction={goToNextViewDetailsAction}
                    updateCardItemAction={updateCardItemAction}
                    activeTab={activeTab}
                    labelListState={labelListState}
                    selectedPostMarkIdState={selectedPostMarkIdState}
                    startEnforcementLoaderState={startEnforcementLoaderState}
                    selectInfringementType={selectInfringementType}
                    selectedPostMark={this.selectedPostMark}
                    setCurrentInfringementType={this.setCurrentInfringementType}
                    clickApplyInfringement={this.clickApplyInfringement}
                    closePanel={this.closePanel}
                    clickTab={this.clickTab}
                    closeList={this.closeList}
                    setOptions={this.setOptions}
                    changeListValues={changeListValues}
                    valueList={valueList}
                    optionValue={optionValue}
                />
            </ViewDetailsWrapper>
        );
    }
}
