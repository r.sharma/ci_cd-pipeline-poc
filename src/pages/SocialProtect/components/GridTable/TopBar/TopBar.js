import React from "react";
import { RowLinkView } from "components";
import moment from "moment";
import { dataFormat } from "config";

export const GridTableViewTopBar = ( {
    activeModuleState,
    data,
    tableView = "large",
    chooseCardToSelect = () => {}
} ) => {
    return (
        <div className={`table-main ${tableView}`}>
            <table align="center">
                <tbody>
                    {data && data.length
                        ? data.map( ( e, i ) => (
                            <tr key={i}>
                                <td>
                                    {e.listingImage || e.profileImage ? (
                                        <img
                                            className="img-profile"
                                            src={e.listingImage || e.profileImage}
                                            alt="logo"
                                        />
                                    ) : null}
                                </td>

                                <td>
                                    {e.platform && e.platform.name ? e.platform.name : null}
                                </td>

                                {e.profileURL || e.listingURL ? (
                                    <td>
                                        <RowLinkView
                                            label={e.profileURL || e.listingURL}
                                            iconLink={true}
                                            handleClickIcon={() => {
                                                if ( e.profileURL || e.listingURL )
                                                    window.open( e.profileURL || e.listingURL, "_blank" );
                                            }}
                                        />
                                    </td>
                                ) : null}

                                {activeModuleState === "posts" ? (
                                    <td
                                        style={{
                                            maxWidth: "300px",
                                            overflow: "hidden",
                                            textOverflow: "ellipsis",
                                            whiteSpace: "nowrap"
                                        }}
                                    >
                                        {e.listingTitle || "No Title"}
                                    </td>
                                ) : null}

                                <td>
                                    <RowLinkView
                                        label={
                                            e.profileName ||
                        ( e.profile && e.profile.profileName ) ||
                        null
                                        }
                                        channel={true}
                                    />
                                </td>

                                {activeModuleState === "posts" ? (
                                    <td>{moment( e.createdAt ).format( dataFormat )}</td>
                                ) : null}

                                {activeModuleState === "profiles" ? (
                                    <td>{e.countPostsInDB || 0}</td>
                                ) : null}
                            </tr>
                        ) )
                        : null}
                </tbody>
            </table>
        </div>
    );
};
