import React from "react";
import { storiesOf } from "@storybook/react";
import { StatusIcons } from "./StatusIcons";

storiesOf( "Status Icons", module ).add( "normal state", () => (
    <div style={{ padding: "40px", background: "#e8e8e8" }}>
        <div className="row">
            <div className="col">
                <StatusIcons status="Unhandled" text={true} />
                <br />
                <br />
                <StatusIcons status="Non Required" text={true} />
                <br />
                <br />
                <StatusIcons status="Enforced" text={true} />
                <br />
                <br />
                <StatusIcons status="Canceled" text={true} />
            </div>
            <div className="col">
                <StatusIcons status="Unhandled" text={true} type="card-status" />
                <br />
                <br />
                <StatusIcons status="Non Required" text={true} type="card-status" />
                <br />
                <br />
                <StatusIcons status="Enforced" text={true} type="card-status" />
                <br />
                <br />
                <StatusIcons status="Canceled" text={true} type="card-status" />
            </div>
        </div>
    </div>
) );
