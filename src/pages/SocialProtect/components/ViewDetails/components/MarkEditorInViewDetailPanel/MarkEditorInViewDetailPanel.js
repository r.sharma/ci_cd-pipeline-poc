import React from "react";
import {
    StatusDropdown,
    ButtonHoverSVG,
    Button,
    ChangeLabel
} from "components";
import "./MarkEditorInViewDetailPanel.scss";

export const MarkEditorInViewDetailPanel = ( {
    data,
    data: { mark } = {},
    labelListState,
    selectedPostMarkIdState,
    setOptions = () => {},
    setCurrentInfringementType = () => {},
    clickApplyInfringement = () => {},
    closeList = () => {},
    selectedPostMark = () => {},
    optionValue,
    valueList,
    changeListValues
} ) => {
    return (
        <div className="mark-editor-view-detail">
            <StatusDropdown
                wrapperClassName="status-dropdown"
                data={labelListState}
                defaultValue={
                    selectedPostMarkIdState
                        ? selectedPostMarkIdState
                        : mark && mark.id
                            ? mark.id
                            : ""
                }
                handleChange={e => {
                    selectedPostMark( e );
                }}
            />

            {( mark && mark.id === 1 ) ||
      ( selectedPostMarkIdState && selectedPostMarkIdState === 1 ) ? (
                    <div className="set-infringing-status-type">
                        <ButtonHoverSVG onClick={() => setOptions( "type" )}>
            Set Type
                        </ButtonHoverSVG>
                        <ButtonHoverSVG onClick={() => setOptions( "status" )}>
            Set Status
                        </ButtonHoverSVG>
                    </div>
                ) : null}

            <ChangeLabel
                open={!!optionValue}
                title={optionValue}
                defaultValue={valueList}
                data={changeListValues}
                handleChange={setCurrentInfringementType}
            >
                <Button text="Apply" handleClick={clickApplyInfringement} />
                <Button text="Cancel" color="secondary" handleClick={closeList} />
            </ChangeLabel>
        </div>
    );
};
