import { ApiCreator } from "utils/connected";
import { BASE_URL_API } from "config";
const api = ApiCreator( BASE_URL_API );

export const signInApi = email => api.get( `user?email=${email}` );

export const signUnApi = data =>
    api.post( "user", {
        body: JSON.stringify( data ),
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        }
    } );
