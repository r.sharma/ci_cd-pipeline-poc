import React from "react";
import { shallow } from "enzyme";
import { SpOptionsTopBar } from "./OptionsTopBar";
import { fakeData } from "../../../../fakeDataCardList";

describe( "OptionsTopBar", () => {
    it( "renders 1 <OptionsTopBar /> component", () => {
        const component = shallow( <SpOptionsTopBar /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check props in component", () => {
        const props = {
            cardListOnPageState: fakeData.results,
            selectedAllCardsState: false,
            selectedCardsListState: []
        };
        const component = shallow( <SpOptionsTopBar {...props} /> );
        expect( component.state().open ).toEqual( false );
    } );
} );
