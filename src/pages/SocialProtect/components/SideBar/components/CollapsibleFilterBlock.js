import React from "react";
import { CollapsibleFilter, RadioInput, Loader } from "components";

export const CollapsibleFilterBlock = ( {
    title = "",
    name = "",
    loader = false,
    data = null,
    activeFilterData = {},
    handleChange = () => {}
} ) => {
    let dataExist = !!( data && data.length );
    let btn = "";
    if ( !loader && dataExist ) {
        btn = (
            <div
                className="check-all-items"
                onClick={() => handleChange( name, null )}
                role="presentation"
            >
        Disable filter
            </div>
        );
    }
    return (
        <CollapsibleFilter
            title={title}
            defaultExpanded={!!activeFilterData[name] || activeFilterData[name] === 0}
        >
            {loader ? (
                <Loader style={{ margin: "20px auto" }} />
            ) : dataExist ? (
                data.map( ( e, i ) => (
                    <div key={i}>
                        <RadioInput
                            defaultValue={activeFilterData[name]}
                            label={e.name}
                            value={e.id}
                            onChange={el => handleChange( name, e.id )}
                        />{" "}
                        {/*<span>245</span>*/}
                    </div>
                ) )
            ) : (
                <div>No items</div>
            )}
            {btn}
        </CollapsibleFilter>
    );
};
