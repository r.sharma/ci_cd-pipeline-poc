import React, { PureComponent } from "react";
import Modal from "@material-ui/core/Modal";
import { sliderPrevSvg, sliderNextSvg } from "components/Icons/Svg";
import { SlickSlider, ButtonClose } from "components";
import "./ViewDetailsSlider.scss";

export class SpViewDetailsSlider extends PureComponent {
    constructor( props ) {
        super( props );
        this.state = {
            visible: false,
            slickGoTo: null
        };
    }

    toggleDialog = val => {
        this.setState( {
            visible: !this.state.visible,
            slickGoTo: Number( val )
        } );
    };

    render() {
        const { visible, slickGoTo } = this.state;
        const prevSvg = sliderPrevSvg();
        const nextSvg = sliderNextSvg();
        const PrevArrow = props => (
            <button
                onClick={props.onClick}
                type="button"
                className="slick-prev pull-left"
            >
                {prevSvg}
            </button>
        );
        const NextArrow = props => (
            <button
                onClick={props.onClick}
                type="button"
                className="slick-next pull-right"
            >
                {nextSvg}
            </button>
        );
        return (
            <div className="view-panel-slider-box">
                <ul>
                    <li onClick={() => this.toggleDialog( "0" )} role="presentation">
                        <img
                            src="https://media.gettyimages.com/photos/moraine-lake-in-banff-national-park-canada-picture-id500177214?s=612x612"
                            alt=""
                        />
                    </li>
                    <li onClick={() => this.toggleDialog( "1" )} role="presentation">
                        <img
                            src="https://cdn.pixabay.com/photo/2017/04/09/09/56/avenue-2215317__340.jpg"
                            alt=""
                        />
                    </li>
                    <li onClick={() => this.toggleDialog( "2" )} role="presentation">
                        <img
                            src="https://media.gettyimages.com/photos/spring-in-southern-woodland-garden-picture-id512037234?s=612x612"
                            alt=""
                        />
                    </li>
                    <li
                        className="show-all"
                        onClick={this.toggleDialog}
                        role="presentation"
                    >
                        + 24
                    </li>
                </ul>

                <Modal open={visible} disablePortal={true} onClose={this.toggleDialog}>
                    <div className={"main-modal"}>
                        <ButtonClose
                            color="#979797"
                            className="close-modal"
                            onClick={this.toggleDialog}
                        />
                        <div className="title-slider">Subway Surfers</div>
                        <SlickSlider
                            multiple={true}
                            cssClass={"view-panel-slider"}
                            cssClass2={"view-panel-slider-list"}
                            next={<NextArrow />}
                            prev={<PrevArrow />}
                            slickGoTo={slickGoTo}
                            props1={{
                                fade: true,
                                infinite: true,
                                speed: 500
                            }}
                            props2={{
                                slidesToShow: 10,
                                infinite: true,
                                swipeToSlide: true,
                                focusOnSelect: true
                            }}
                        >
                            <div>
                                <img
                                    src="https://media.gettyimages.com/photos/moraine-lake-in-banff-national-park-canada-picture-id500177214?s=612x612" //eslint-disable-line
                                    alt=""
                                />
                            </div>
                            <div>
                                <img
                                    src="https://cdn.pixabay.com/photo/2017/04/09/09/56/avenue-2215317__340.jpg"
                                    alt=""
                                />
                            </div>
                            <div>
                                <img
                                    src="https://media.gettyimages.com/photos/spring-in-southern-woodland-garden-picture-id512037234?s=612x612"
                                    alt=""
                                />
                            </div>
                        </SlickSlider>
                    </div>
                </Modal>
            </div>
        );
    }
}
