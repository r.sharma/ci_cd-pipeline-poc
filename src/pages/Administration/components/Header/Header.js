import React from "react";
import { CompanySelect, MenuModules, ProdSwitcher, HeaderWrapper } from "components";

export const Header = props => {
    return (
        <HeaderWrapper
            left={( <div className="title">{props.title}</div> )}
            center={props.children}
            right={(
                <>
                    <ProdSwitcher {...props} />
                    <MenuModules />
                    <CompanySelect {...props} />
                </>
            )}
        />
    );
};
