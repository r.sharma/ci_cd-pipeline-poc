import React from "react";
import { storiesOf } from "@storybook/react";
import { Checkbox } from "./Checkbox";

storiesOf( "Checkbox", module )
    .add( "normal state", () => (
        <Checkbox name="checkbox" label="Label-1" value={false} />
    ) )
    .add( "active state", () => (
        <Checkbox name="checkbox" label="Label-1" value={true} />
    ) );
