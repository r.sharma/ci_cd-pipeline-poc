export const getUsersList = () => {
    return generateUsersListFixture();
};

export const getUsersDetail = ({ id = null } = null) => {
    if (!id) throw new Error("No id for user specified");

    return generateUsersDetailFixture();
};

const generateUsersListFixture = () => {
    /*
        // EDIT: CAN BE REMOVED?
        @TODO: Move to a helper, not in Table b/c:
        a): when component updates state, it will re-render and regenerate the columns during that operation = crash
        b): other solution would be to put the generated list in state once and read from there, but then you need more logic in the lifecycles mount/update and comparing with already stored state
    */

    const tempUserFixture = [
        {
            userId: 1,
            client: "System",
            firstName: "András",
            lastName: "Békás",
            email: "bekas.adras@fantom.com",
            isDeveloper: false,
            isDemo: false,
            isRemote: false,
            isBeta: false,
            isEnabled: true,
            isSisense: false,
            isStatsV2: true
        },
        {
            userId: 2,
            client: "System",
            firstName: "Nándor",
            lastName: "Békás",
            email: "bekas.nandor@fantom.com",
            isDeveloper: true,
            isDemo: true,
            isRemote: false,
            isBeta: false,
            isEnabled: true,
            isSisense: false,
            isStatsV2: true
        }
    ];

    for (const i in Array.apply(null, { length: 100 }).map(Number.call, Number)) {
        if ([0, 1, 2].includes(Number(i))) continue;

        tempUserFixture.push(JSON.parse(JSON.stringify(tempUserFixture[0])));
        tempUserFixture[tempUserFixture.length - 1].userId = Number(i);
    }

    return {
        data: tempUserFixture
    };
};

const generateUsersDetailFixture = () => {
    const testBrands = [
        {
            id: 1,
            name: "Nike",
            code: "nike",
            clientID: 7,
            logo: "/uploads/customer/nike.png",
            status: {
                id: 3,
                name: "Operational",
                code: "operational"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: false,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 2,
            modifiedByUserID: 1
        },
        {
            id: 2,
            name: "TomTom",
            code: "tomtom",
            clientID: 8,
            logo: "/uploads/customer/tomtom.png",
            status: {
                id: 3,
                name: "Operational",
                code: "operational"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: false,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 1,
            modifiedByUserID: 1
        },
        {
            id: 3,
            name: "Converse",
            code: "converse",
            clientID: 7,
            logo:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAQAAADa613fAAAABGdBTUEAALGPC/xhBQAAAAJiS0dEAP+Hj8y/AAAACXBIWXMAAABIAAAASABGyWs+AAAAB3RJTUUH4wUOBy8b6Q3cywAABCRJREFUeNrtmm9olVUYwH/37rpd2dzc1nCiVs7JuksLA2tkSFENxIjpqMilUOQ3wy/9E4qoPoROJHRrZCv6R2BpooaiLlmGlE6WYf+QIEvGnNI1Nbe1+2d9cMy76733PQ/neN8vz+98vPc9z/M773vOfc65LyiKoiiKoiiKoiiKoiiK4hsBB30U0UQtScEV/7Kdfr/VMw3G3RxnVNS6me932pm5lS9JilR+5UG/k85MOZv5T6TSzyoKXIV31hHDHOIyDRQZX1HCwwToIe52RF0Q5AnOiO5KjK1U+Z12Zhbxo3Di76PW76QzU8d+oUovi/xOOjNVvE9cpPIXzXa/ae4meyqDdBFnIYXGV5TRyAi9JG5IPlaEeIYB0V0ZZhNT/Ut4OsuyPhSN/CZSSbKdWf5oVPA5nTk+v4NvhRP/CAvyr1FKJ0ney/mdmWwjIVL5nSX51SimjQSjHiJQSivDIpXzrCaUL40w64kxaiACk1hDVKQyyJuUuEl0Ho9RT3GWTwt5nZGxoN4iEKCJP0QqcT6i2oVIKW9zju/ZQgv1aaMT4gWGxkOaiADcI961dBFxoTKFzrFFMcpR2llBhBIgyBqupIQzFYE5fCXctZxksQuVcj5NCZzkb47SzltcnBDMXAQqeWf8kTRrfTxJ0F6lih2eobaKepzMy1wSqVzkJcL2KtPZ6xFob45FIRNBWugXqcTooMJeZRZf5wyTIMoxOljJPKYY9vkAP4lUkuxmtr1KDUeMwkXpoYOnmG8gFPEYnuvbMRbaq9zGCcHoXeAHOlnpUQBW85lwDTvN0mydma4HkwQlwygxhhhkyGN3ERCvRknRMWAG6ugxGK8E5zjMRpYzl8mefd5Ot/DR+o677DRme5TicQb4hlaaqTUQuMpD/CKc7Du5xU5jJl0eQXYzR7TOF7BKuHccYQvldhrV7PEMI/1BfIXLIo1/eN72B7GSLyY8QmfpZgOvppXkkhLlJt4dK/5N2xkety1Ryvg4ZQ5soIkawkCAZycUGeYic9knnOAnuM9OAspoo49u1rOM2rRbW8BzKfWvqci99Ao1DlBnqwERHskxiUOsG9++mm2smvlTJBHjA6bZa3hzbY/oLVLIWi6INK7wmqgQtSLMRuIGIlPZJDx8GODpG3QGmoVi2jyPg25mh7CiOkVjPiWuUsYnOUUWGNbN19ph7sy/BkAVS7MemS7hlEgiwTZm+KORnRCrOS/SGKaVUr/TTqeYNyactHi3KGsFf0PkiWl8KPyj5zRNTl5ecEqEg8IJfpwGv5O+nsWcFGrscXGs4JYgK+gTSYzQQaXfaacT5kXh0dsl1rk4enNLBe3CVzj6aMlvIWJCDbuEhcjP3O930ukEaBDvNA5R7zoJe4p4lBrhi2c7OetWRFEURVEURVEURVEURVEUH/kfKbGWY6+nrE0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMDUtMTRUMDc6NDc6MjcrMDA6MDAR+0AhAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTA1LTE0VDA3OjQ3OjI3KzAwOjAwYKb4nQAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=",
            status: {
                id: 3,
                name: "Operational",
                code: "operational"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: false,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 2,
            modifiedByUserID: 1
        },
        {
            id: 4,
            name: "Levis",
            code: "levis",
            clientID: 9,
            logo: "/uploads/customer/levis.png",
            status: {
                id: 1,
                name: "Demo",
                code: "demo"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: true,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 1,
            modifiedByUserID: 1
        },
        {
            id: 5,
            name: "Lipsy",
            code: "lipsy",
            clientID: 10,
            logo: "/uploads/customer/pointer.png",
            status: {
                id: 1,
                name: "Demo",
                code: "demo"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: true,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 1,
            modifiedByUserID: 1
        },
        {
            id: 6,
            name: "Vitra",
            code: "vitra",
            clientID: 11,
            logo: "/uploads/customer/pointer.png",
            status: {
                id: 1,
                name: "Demo",
                code: "demo"
            },
            createdAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            modifiedAt: {
                date: "2014-02-12 13:19:25.000000",
                timezone_type: 3,
                timezone: "UTC"
            },
            isEnabled: true,
            deleted: false,
            whiteLabel: false,
            countDaysToNextEnforcementEmail: 1,
            modifiedByUserID: 1
        }
    ];

    const testRoles = [
        {
            id: 1,
            name: "Basic user",
            code: "ROLE_USER",
            description: "Basic system functionality",
            pbpOnly: false
        },
        {
            id: 2,
            name: "Enforcer",
            code: "ROLE_ENFORCER",
            description: "User can enforce infringing matters",
            pbpOnly: false
        },
        {
            id: 3,
            name: "User manager",
            code: "ROLE_USER_MANAGER",
            description: "The user can create and manage users for the same client.",
            pbpOnly: false
        },
        {
            id: 4,
            name: "Admin",
            code: "ROLE_ADMIN",
            description: "The user can modify system settings, such as proxies",
            pbpOnly: true
        },
        {
            id: 6,
            name: "Pointer user",
            code: "ROLE_POINTER",
            description: "Special role for pointer users only.",
            pbpOnly: true
        },
        {
            id: 7,
            name: "Blogger",
            code: "ROLE_BLOGGER",
            description: "The user can blog",
            pbpOnly: false
        },
        {
            id: 16,
            name: "Domains enforcer user",
            code: "ROLE_DOMAINS_ENFORCER",
            description: "This user can enforce domains",
            pbpOnly: false
        },
        {
            id: 18,
            name: "MarketProtect viewer",
            code: "ROLE_MARKETPROTECT_VIEWER",
            description: "This use can see but not enforce MarketProtect",
            pbpOnly: false
        },
        {
            id: 19,
            name: "DomainProtect viewer",
            code: "ROLE_DOMAINSPROTECT_VIEWER",
            description: "This user can see but not enforce DomainProtect",
            pbpOnly: false
        },
        {
            id: 20,
            name: "SocialProtect viewer",
            code: "ROLE_SOCIALPROTECT_VIEWER",
            description: "This user can see but not enforce SocialProtect",
            pbpOnly: false
        },
        {
            id: 22,
            name: "Advert deleter",
            code: "ROLE_DELETE_ADVERT",
            description: "User can delete advert",
            pbpOnly: false
        }
    ];

    const testOverview = {
        userId: 1,
        client: "System",
        firstName: "András",
        lastName: "Békás",
        email: "bekas.adras@fantom.com",
        isDeveloper: false,
        isDemo: false,
        isRemote: false,
        isBeta: false,
        isEnabled: true,
        isSisense: false,
        isStatsV2: true
    };

    return {
        data: {
            overview: testOverview,
            brands: testBrands,
            roles: testRoles
        }
    };
};
