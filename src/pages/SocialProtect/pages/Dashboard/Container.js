import { connect } from "react-redux";
import { Dashboard } from "./Dashboard";
import { compose } from "redux";
import { errorsDecorator } from "components";

import { sideBarStateSelector, activeModuleStateSelector } from "services/UserView/selectors";
import { setActiveModuleStateAction } from "services/UserView/actions";

const mapStateToProps = state => {
    const sideBarIsOpen = sideBarStateSelector( state ) || false;
    const activeModuleState = activeModuleStateSelector( state ) || null;
    return {
        sideBarIsOpen,
        activeModuleState
    };
};

const mapDispatchToProps = {
    setActiveModuleStateAction
};

export const SocialProtectDashboard = compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    errorsDecorator(),
)( Dashboard );
