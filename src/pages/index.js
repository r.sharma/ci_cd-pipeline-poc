export { NoMatch } from "components";

// Landing
export { LandingDashboard } from "pages/Landing/pages";

// SOCIAL PROTECT
export {
    SocialProtectDashboard,
    SocialProtectAddPost,
    SocialProtectAddProfiles
} from "pages/SocialProtect/pages";

// ADMINISTRATION
export { AdminDashboard } from "pages/Administration/pages";
