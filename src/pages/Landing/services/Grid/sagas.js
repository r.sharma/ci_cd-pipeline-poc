import { takeLatest } from "redux-saga/effects";
import {
    saveGridView
} from "./actions";
import { activeServiceValidationWrapper } from "services/UserView/sagas";
import { activeBrandSwitchedAction } from "services/Brand/actions";
import { setActiveModuleStateAction } from "services/UserView/actions";
import { serviceDefinition } from "./constants";

// WORKERS
function* saveGridViewWorker( { payload } ) {
}


function* activeBrandSwitchedWorker( id ) {
    // Currently no action, to be implemented when we have scisense data on the page
}
function* setActiveModuleWorker( activeModuleState ) {
    // Currently no action, to be implemented when we have scisense data on the page
}


// WATCHERS
export function* landingUsersActiveBrandSwitchedWatcher() {
    yield takeLatest(
        activeBrandSwitchedAction,
        activeServiceValidationWrapper,
        {
            ...serviceDefinition,
            worker: activeBrandSwitchedWorker
        }
    );
}
export function* landingUsersSetActiveModuleWatcher() {
    yield takeLatest(
        setActiveModuleStateAction,
        activeServiceValidationWrapper,
        {
            ...serviceDefinition,
            worker: setActiveModuleWorker
        }
    );
}
export function* saveGridViewWatcher() {
    yield takeLatest( saveGridView, saveGridViewWorker );
}