import React from "react";
import { App, Redirecter } from "components";
import { Route, Switch } from "react-router-dom";
import {
    NoMatch,
    SocialProtectDashboard,
    AdminDashboard,
    LandingDashboard,
    SocialProtectAddPost,
    SocialProtectAddProfiles
} from "pages";
import { BASE_URL } from "config";

export const Routes = (
    <App>
        <Switch>
            <Route exact path="/" component={LandingDashboard} />
            <Route path="/admin" component={AdminDashboard} />
            <Route path="/social-protect" component={SocialProtectDashboard} />
            <Route
                path="/social-protect-add/posts"
                component={SocialProtectAddPost}
            />
            <Route
                path="/social-protect-add/profiles"
                component={SocialProtectAddProfiles}
            />
            <Route
                exact path="/login"
                render={ () => <Redirecter url={`${BASE_URL}/login`} /> }
            />
            <Route component={NoMatch} />
        </Switch>
    </App>
);