import { connect } from "react-redux";
import { UsersDetailBrands as component } from "./Brands";

const mapDispatchToProps = {};

const mapStateToProps = state => {
    return {};
};

export const UsersDetailBrands = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
