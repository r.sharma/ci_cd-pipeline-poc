import { createAction } from "redux-actions";

export const setActiveModuleStateAction = createAction( "setActiveModuleStateAction" );
export const setActiveModuleAction = createAction( "setActiveModuleAction" );
export const storeActiveModuleStateAction = createAction( "storeActiveModuleStateAction" );
export const storeActiveModuleAction = createAction( "storeActiveModuleAction" );
export const showViewDetailsPanelAction = createAction( "showViewDetailsPanelAction" );
export const sideBarStateChangeAction = createAction( "sideBarStateChangeAction" );
export const sideBarStateSaveAction = createAction( "sideBarStateSaveAction" );
