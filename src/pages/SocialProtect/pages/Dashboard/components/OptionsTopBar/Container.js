import { connect } from "react-redux";
import { OptionsTopBar as component } from "./OptionsTopBar";

import {
    selectedCardsList,
    filteredCardListDataSelector,
    selectedAllCardsStateSelector
} from "pages/SocialProtect/services/Card/selectors";
import {
    chooseCardToSelectAction,
    chooseAllCardAction,
    deleteAllSelectedCardsAction
} from "pages/SocialProtect/services/Card/actions";
import { startEnforcementAction, selectedPostMarkIdAction } from "pages/SocialProtect/services/Enforcement/actions";
import {
    selectInfringementTypeInTopBarAction,
    selectEnforcementStatusInTopBarAction
} from "pages/SocialProtect/services/Options/actions";
import {
    selectedPostMarkIdSelector,
    startEnforcementLoaderSelector,
    selectedEnforcementStatusIdSelector,
    selectedInfringementTypeIdSelector
} from "pages/SocialProtect/services/Options/selectors";
import { labelListSelector } from "services/Label/selectors";
import { activeModuleStateSelector } from "services/UserView/selectors";
import { statusListSelector } from "services/Status/selectors";
import { infringementTypeListSelector } from "services/InfringementType/selectors";

const mapDispatchToProps = {
    chooseCardToSelectAction,
    chooseAllCardAction,
    deleteAllSelectedCardsAction,
    selectedPostMarkIdAction,
    startEnforcementAction,
    selectInfringementTypeInTopBarAction,
    selectEnforcementStatusInTopBarAction
};

const mapStateToProps = state => {
    const selectedCardsListState = selectedCardsList( state );
    const cardListOnPageState = ( filteredCardListDataSelector( state ) && filteredCardListDataSelector( state ).results ) || null;
    const selectedAllCardsState = selectedAllCardsStateSelector( state );
    const labelListState = labelListSelector( state );
    const selectedPostMarkIdState = selectedPostMarkIdSelector( state );
    const activeModuleState = activeModuleStateSelector( state );
    const startEnforcementLoaderState = startEnforcementLoaderSelector( state );
    const statusListState = statusListSelector( state );
    const selectedEnforcementStatusIdState = selectedEnforcementStatusIdSelector( state );
    const infringementTypeListState = infringementTypeListSelector( state );
    const infringementTypeIdState = selectedInfringementTypeIdSelector( state );
    return {
        activeModuleState,
        cardListOnPageState,
        selectedCardsListState,
        selectedAllCardsState,
        labelListState,
        selectedPostMarkIdState,
        startEnforcementLoaderState,
        statusListState,
        selectedEnforcementStatusIdState,
        infringementTypeListState,
        infringementTypeIdState
    };
};

export const OptionsTopBar = connect(
    mapStateToProps,
    mapDispatchToProps,
)( component );
