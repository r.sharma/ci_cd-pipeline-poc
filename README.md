# Hello world
Rename `env.dist` to `.env`. Then run `npm i` to install, and `npm start` to start.

Have fun!

# Setup
A local installation of Revlect is preferred. The `.env` file is configured for this usecase. If you cannot use a local environment, see the `Proxy` chapter below.

## Proxy
1. First, add the server to your hosts file, e.g.: `127.0.0.1 localhost.staging.dev.pbp`
2. Add a `.env.local` file with your overrides. An example has been provided as `.env.local.dist`.
3. Whenever you start the project, do not run `npm start`, instead execute `npm run proxied`

# Warnings
YARN is strictly prohibited.
