import React from "react";
import { shallow } from "enzyme";
import { CollapsibleFilter } from "./CollapsibleFilter";

describe( "CollapsibleFilter", () => {
    it( "renders 1 <CollapsibleFilter /> component", () => {
        const component = shallow( <CollapsibleFilter /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <CollapsibleFilter title="35" /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check props title", () => {
        const component = shallow( <CollapsibleFilter title="test-title" /> );

        const PanelBarItem = component.find( "PanelBarItem" );
        expect( PanelBarItem ).toHaveLength( 1 );
        expect( PanelBarItem.props().title ).toEqual( "test-title" );
    } );

    describe( "check children", () => {
        const component = shallow(
            <CollapsibleFilter title="test-title">
                <div>test</div>
            </CollapsibleFilter>
        );

        const children = component.find( "PanelBarItem" ).children();

        expect( children ).toHaveLength( 1 );
        expect( children.type() ).toEqual( "div" );
        expect( children.text() ).toEqual( "test" );
    } );
} );
