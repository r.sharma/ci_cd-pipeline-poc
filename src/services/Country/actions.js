import { createAction } from "redux-actions";

export const loadCountryListAction = createAction( "loadCountryListAction" );
export const saveCountryListAction = createAction( "saveCountryListAction" );
