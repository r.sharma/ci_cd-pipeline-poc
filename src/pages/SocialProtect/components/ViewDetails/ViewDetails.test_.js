import React from "react";
import { shallow } from "enzyme";
import { ViewDetails } from "./ViewDetails";
import { cardListFixture } from "pages/SocialProtect/components/Cards/fixtures";

describe( "ViewDetails", () => {
    it( "renders 1 <ViewDetails /> component", () => {
        const component = shallow( <ViewDetails /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "check posts props in component", () => {
        const props = {
            activeModuleState: "posts",
            viewDetailItemDataState: {
                data: cardListFixture.results[0],
                index: 0,
                lengthList: 5
            },
            deleteOneCardsAction: () => {},
            showViewDetailsPanelAction: () => {},
            goToNextViewDetailsAction: () => {}
        };

        const component = shallow( <ViewDetails {...props} /> );
        expect( component.find( "PostsPanel" ) ).toHaveLength( 1 );
        expect( component.find( "PostsPanel" ).props().data ).toEqual(
            props.viewDetailItemDataState.data
        );
        expect( component.find( "PostsPanel" ).props().index ).toEqual(
            props.viewDetailItemDataState.index
        );
        expect( component.find( "PostsPanel" ).props().lengthList ).toEqual(
            props.viewDetailItemDataState.lengthList
        );

        expect( component.find( "ProfilesPanel" ) ).toHaveLength( 0 );
    } );

    describe( "check profiles props in component", () => {
        const props = {
            activeModuleState: "profiles",
            viewDetailItemDataState: {
                data: cardListFixture.results[0],
                index: 0,
                lengthList: 5
            },
            deleteOneCardsAction: () => {},
            showViewDetailsPanelAction: () => {},
            goToNextViewDetailsAction: () => {}
        };

        const component = shallow( <ViewDetails {...props} /> );
        expect( component.find( "ProfilesPanel" ) ).toHaveLength( 1 );
        expect( component.find( "ProfilesPanel" ).props().data ).toEqual(
            props.viewDetailItemDataState.data
        );
        expect( component.find( "ProfilesPanel" ).props().index ).toEqual(
            props.viewDetailItemDataState.index
        );
        expect( component.find( "ProfilesPanel" ).props().lengthList ).toEqual(
            props.viewDetailItemDataState.lengthList
        );

        expect( component.find( "PostsPanel" ) ).toHaveLength( 0 );
    } );
} );
