import { ApiCreator } from "utils/connected";
import { BASE_URL_API } from "config";
const api = ApiCreator( BASE_URL_API.replace( "/rest", "" ) );

export const getNewToken = () =>
    api.get( "socialmedia/generic/storage/postMark/list", {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        }
    } );
