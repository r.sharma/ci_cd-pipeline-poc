export const columnsUsers = [
    {
        title: "User ID",
        field: "userId",
        readOnly: true
    },
    {
        title: "Client",
        field: "client",
        readOnly: true
    },
    {
        title: "First Name",
        field: "firstName"
    },
    {
        title: "Last Name",
        field: "lastName"
    },
    {
        title: "Email address",
        field: "email"
    },
    {
        title: "Developer",
        field: "isDeveloper"
    },
    {
        title: "Demo",
        field: "isDemo"
    },
    {
        title: "Remote",
        field: "isRemote"
    },
    {
        title: "Bèta",
        field: "isBeta"
    },
    {
        title: "Enabled",
        field: "isEnabled"
    },
    {
        title: "Sisense",
        field: "isSisense"
    },
    {
        title: "Stats V2",
        field: "isStatsV2"
    }
];
