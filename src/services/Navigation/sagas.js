import { setActiveModuleAction, setActiveModuleStateAction } from "services/UserView/actions";
import { handleNavigationAction, onNavigationAction } from "./actions";
import { urlUtil } from "utils";
import { store } from "index";
import { history } from "./history";
import { call, put, takeLatest } from "redux-saga/effects";
import { validateRoute } from "./models";

// Utils
const {  getModuleStateFromUrl, getModuleFromUrl } = urlUtil;

function* fetchCurrentPageWorker() {
    if ( history ) {
        yield put( onNavigationAction( { location: history.location, firstInit: true } ) );

        // Listen to navigation changes
        history.listen( location => {
            store.dispatch( onNavigationAction( { location } ) );
        } );
    }
}

function* onNavigation( { payload: { location, firstInit } } ) {
    const activeModule = getModuleFromUrl( location.pathname );
    const activeModuleState = getModuleStateFromUrl( location.pathname );

    // In case we will get a 404, do not update the store because we'll error out
    if ( !validateRoute( activeModule, activeModuleState ) ) return;

    yield put( setActiveModuleAction( activeModule ) );
    yield put( setActiveModuleStateAction( activeModuleState ) );

    if ( !activeModuleState ) return;

    if ( !firstInit ) yield put( handleNavigationAction( { pathName: location.pathname } ) );
}

export function* watchingPage() {
    yield takeLatest( onNavigationAction, onNavigation );
    yield call( fetchCurrentPageWorker );
}