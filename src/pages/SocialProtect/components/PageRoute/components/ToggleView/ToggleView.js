import React, { PureComponent } from "react";
import { CustomizeBlock } from "./CustomizeBlock/";
import { block, mixedSvg, row } from "components";
import "./ToggleView.scss";

export class ToggleView extends PureComponent {
    state = {
        active: null
    };

    static getDerivedStateFromProps( nextProps, prevState ) {
        if ( nextProps.active ) {
            return {
                active: nextProps.active
            };
        }
        return null;
    }

    onChange = value => {
        const { pageViewChange = () => {} } = this.props;
        this.setState(
            {
                active: value
            },
            () => pageViewChange( value )
        );
    };

    render() {
        const { mixed = true } = this.props;
        const { active } = this.state;
        let wrapperClassName = "toggle-view-default";

        return (
            <div className={wrapperClassName}>
                <ul className="list-view">
                    <li>
                        <CustomizeBlock label="Customize:" />
                    </li>
                    <li>
                        <button type="button" onClick={() => this.onChange( "block" )}>
                            {block( active === "block" ? "#445bf4" : "#809096" )}
                        </button>
                    </li>
                    {mixed ? (
                        <li>
                            <button type="button" onClick={() => this.onChange( "mixed" )}>
                                {mixedSvg( active === "mixed" ? "#445bf4" : "#809096" )}
                            </button>
                        </li>
                    ) : null}
                    <li>
                        <button type="button" onClick={() => this.onChange( "row" )}>
                            {row( active === "row" ? "#445bf4" : "#809096" )}
                        </button>
                    </li>
                </ul>
            </div>
        );
    }
}
