import React from "react";
import {
    Checkbox,
    SocialTitle,
    StatusIcons,
    CollapsibleParagraph,
    Avatar
} from "components";
import { maximizeCard, externalLinkCard } from "components/Icons/Svg";
import { CardActions } from "pages/SocialProtect/components/Cards";
import moment from "moment";
import { dataFormat } from "config";
import "./ProfileItem.scss";

export const ProfileCardItem = ( {
    img = null,
    titleHeader = "",
    cardURL = null,
    name = "",
    date = "",
    text = "",
    followers = "0",
    posts = "",
    views = "",
    status = "",
    children,
    imgHeader = null,
    selectedCardsList = [],
    chooseCardToSelect = () => {},
    showViewDetailsPanelAction = () => {},
    deleteOneCardsAction = () => {},
    userPushToRouteAction = () => {},
    element = { id: null }
} ) => {
    return (
        <div className="card-item card-item__profile noselect">
            <div className="card-item-header">
                <Checkbox
                    handleChange={( { value } ) => {
                        chooseCardToSelect( {
                            value: value,
                            id: element.id
                        } );
                    }}
                    defaultValue={selectedCardsList.indexOf( element.id ) !== -1}
                />
                <SocialTitle code={imgHeader} title={titleHeader} />
                <CardActions
                    id={element.id}
                    deleteOneCardsAction={deleteOneCardsAction}
                    editBtn={false}
                />
            </div>
            <div className="card-item-body">
                <div className="img-box">
                    {img ? <Avatar size="65" data={[ img ]} /> : null}
                    {children ? <div className="children-box">{children}</div> : null}
                    <div className="card-item-body-footer">
                        <StatusIcons status={status} text={true} type="card-status" />
                    </div>
                </div>
                <div className="description">
                    <div className="title-description-card">
                        <div className="description-name">{name || "No title"}</div>
                        <span>{moment( date ).format( dataFormat )}</span>
                    </div>
                    <CollapsibleParagraph text={text} />
                    <div className="detail">
                        <div className="views-card">
                            <div>
                Followers: <span>{followers || "0"}</span>
                            </div>
                            <div>
                Posts: <span>{posts || "0"}</span>
                            </div>
                            <div>
                Views: <span>{views || "0"}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-item-footer">
                <div>
                    <a
                        href="/"
                        onClick={e => {
                            e.preventDefault();
                            if ( cardURL )
                                userPushToRouteAction( { profileURL: cardURL, module: "posts" } );
                        }}
                    >
                        {maximizeCard()}
                    </a>
                </div>
                <button
                    type={"button"}
                    onClick={() => showViewDetailsPanelAction( element.id )}
                >
          View details
                </button>
                <div>
                    {cardURL ? (
                        <a href={cardURL} rel="noopener noreferrer" target="_blank">
                            {externalLinkCard()}
                        </a>
                    ) : null}
                </div>
            </div>
        </div>
    );
};
