import React from "react";
import { storiesOf } from "@storybook/react";
import { CardItemImageViewSmall } from "./ItemImageViewSmall";

storiesOf( "Card Item", module ).add( "normal state", () => (
    <div className="row">
        <div className="col-md-4">
            <CardItemImageViewSmall
                img={
                    "http://cache.desktopnexus.com/thumbseg/1998/1998280-bigthumbnail.jpg"
                }
                imgHeader={{
                    src:
            "https://icon2.kisspng.com/20180410/rzw/kisspng-logo-computer-icons-youtube-symbol-instagram-logo-5acd6f97777305.1128861915234128874893.jpg",
                    title: "test"
                }}
                titleHeader="Instagram"
                name="Tremblant In Canada"
                date="04-21-2018"
                text="Can I buy a replica Nike Cal rugby jersey online? Can I buy a replica Nike Cal rugby jersey online? "
                channel="Fátima Cambeiro"
                posts="23"
                views="23"
                status="Unhandled"
            />
        </div>
    </div>
) );
