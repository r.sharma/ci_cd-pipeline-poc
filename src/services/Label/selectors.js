import { createSelector } from "reselect";

export const labelListIsLoadingSelector = createSelector(
    state => state.main.label.labelListIsLoading,
    state => state
);

export const labelListSelector = createSelector(
    state => {
        const { activeModuleState } = state.main.userView;
        if ( activeModuleState === "posts" ) {
            return state.main.label.postLabelList;
        } else if ( activeModuleState === "profiles" ) {
            return state.main.label.profileLabelList;
        }
        return null;
    },
    state => state
);