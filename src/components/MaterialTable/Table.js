import React from "react";
import PropTypes from "prop-types";

import { AlertDialog } from "components";
import MaterialTable, {
    MTableCell,
    MTableBodyRow,
    MTableToolbar
} from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

import IconButton from "@material-ui/core/IconButton";

import CheckCircleIcon from "@material-ui/icons/CheckCircleRounded";
import CancelIcon from "@material-ui/icons/CancelRounded";

require( "./Table.scss" );

const tableIcons = {
    Add: AddBox,
    Check: Check,
    Clear: Clear,
    Delete: DeleteOutline,
    DetailPanel: ChevronRight,
    Edit: Edit,
    Export: SaveAlt,
    Filter: FilterList,
    FirstPage: FirstPage,
    LastPage: LastPage,
    NextPage: ChevronRight,
    PreviousPage: ChevronLeft,
    ResetSearch: Clear,
    Search: Search,
    SortArrow: ArrowUpward,
    ThirdStateCheck: Remove,
    ViewColumn: ViewColumn
};

const defaultAlertState = {
    alertTitle: null,
    alertText: null,
    alertAcceptanceLabel: null,
    alertRejectionLabel: null,
    alertOnAccept: () => {},
    alertOnReject: () => {},
    alertIsOpen: false
};

class Table extends React.Component {
    initialState = {
        ...defaultAlertState,
        generatedColumns: null
    };

    state = this.initialState;

    handleDelete = data => {
        const { onDelete } = this.props;

        this.resetAlert();
        onDelete( data );
    };

    resetAlert = () => this.setState( { ...defaultAlertState } );

    offerDelete = data => {
        const { rowRepresents } = this.props;

        this.setState( {
            alertTitle: "Deletion",
            alertText: `You want to delete ${data.length} ${rowRepresents}`,
            alertAcceptanceLabel: "Affirmative",
            alertRejectionLabel: "Abort",
            alertOnAccept: () => this.handleDelete( data ),
            alertOnReject: () => this.resetAlert(),
            alertIsOpen: true
        } );
    };

    renderColumns = () => {
        const { columns, onEdit } = this.props;
        const shouldRenderEditColumn = false;

        const editColumn = {
            render: data => (
                <IconButton aria-label="Edit" onClick={() => onEdit( "userId", data )}>
                    <Edit />
                </IconButton>
            )
        };

        return [ onEdit && shouldRenderEditColumn && editColumn, ...columns ].filter(
            Boolean
        );
    };

    renderCell = ( cell, cellProps ) => {
        const { onEdit } = this.props;
        const { rowData } = cellProps;

        return (
            <span
                onClick={() => onEdit && onEdit( "userId", rowData )}
                role="presentation"
            >
                {typeof cell !== "boolean" ? (
                    cell
                ) : cell ? (
                    <CheckCircleIcon />
                ) : (
                    <CancelIcon />
                )}
            </span>
        );
    };

    getTableActions = () => {
        //@TODO: Mutations not implemented yet.
        return [];

        /*
        //const {onDelete, rowRepresents} = this.props;

        return [
            onDelete && {
                tooltip: `Remove all selected ${rowRepresents}`,
                icon: DeleteOutline,
                onClick: (e, data) => this.offerDelete(data),
            },
        ].filter(Boolean);
        */
    };

    getStyles = () => {
        const { hideBorder } = this.props;

        return {
            boxShadow: hideBorder ? "none" : ""
        };
    };

    render() {
        const { rows, title } = this.props;
        const {
            alertTitle,
            alertText,
            alertAcceptanceLabel,
            alertRejectionLabel,
            alertOnAccept,
            alertOnReject,
            alertIsOpen
        } = this.state;

        return (
            <>
                <MaterialTable
                    title={title || ""}
                    icons={tableIcons}
                    style={this.getStyles()}
                    components={{
                        Cell: props => {
                            const filteredProps = { ...props, value: null };
                            let filteredValue = props.value;

                            return (
                                <MTableCell {...filteredProps}>
                                    {this.renderCell( filteredValue, props )}
                                </MTableCell>
                            );
                        },
                        Row: props => {
                            const { onEdit } = this.props;

                            return (
                                <MTableBodyRow
                                    {...props}
                                    onRowClick={() => onEdit && onEdit( "userId", props.data )}
                                    className="tableRow"
                                />
                            );
                        },
                        Toolbar: props => {
                            const { headercolor } = this.props;

                            return (
                                <div style={{ background: headercolor || "" }}>
                                    <MTableToolbar {...props} />
                                    {/*
                                        <div>
                                            <IconButton
                                                style={{padding: '9px'}}
                                                aria-label="Add"
                                                onClick={() => console.log('Adding a user!')}
                                            >
                                                <AddBox />
                                            </IconButton>
                                        </div>
                                    */}
                                </div>
                            );
                        }
                    }}
                    columns={this.renderColumns()}
                    data={rows}
                    options={{
                        selection: true
                    }}
                    actions={this.getTableActions()}
                />

                <AlertDialog
                    title={alertTitle}
                    text={alertText}
                    acceptanceLabel={alertAcceptanceLabel}
                    rejectionLabel={alertRejectionLabel}
                    onAccept={alertOnAccept}
                    onReject={alertOnReject}
                    isOpen={alertIsOpen}
                />
            </>
        );
    }
}

Table.propTypes = {
    rows: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    title: PropTypes.string,
    hideBorder: PropTypes.bool,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    rowRepresents: PropTypes.string.isRequired,
    headercolor: PropTypes.string
};

//@TODO: Selection and editing buttons do not show up at the same time currently https://github.com/mbrn/material-table/issues/676*/
/*
    editable={{
        onRowAdd: newData =>
            new Promise((resolve, reject) => {
                setTimeout(() => {
                    {
                        const data = this.state.data;
                        data.push(newData);
                        this.setState({data}, () => resolve());
                    }
                    resolve();
                }, 1000);
            }),
        onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
                setTimeout(() => {
                    {
                        const data = this.state.data;
                        const index = data.indexOf(oldData);
                        data[index] = newData;
                        this.setState({data}, () => resolve());
                    }
                    resolve();
                }, 1000);
            }),
        onRowDelete: oldData =>
            new Promise((resolve, reject) => {
                setTimeout(() => {
                    {
                        let data = this.state.data;
                        const index = data.indexOf(oldData);
                        data.splice(index, 1);
                        this.setState({data}, () => resolve());
                    }
                    resolve();
                }, 1000);
            }),
    }}
*/

export { Table };
