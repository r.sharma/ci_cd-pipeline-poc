import React from "react";
import { shallow, mount } from "enzyme";
import { Avatar } from "./Avatar";

describe( "Avatar", () => {
    it( "renders 1 <Avatar /> component", () => {
        const component = shallow( <Avatar /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper instance should be null", () => {
        const component = shallow( <Avatar size="35" /> );
        const instance = component.instance();

        expect( instance ).toEqual( null );
    } );

    describe( "check count children", () => {
        const component = mount(
            <Avatar
                size="35"
                data={[ "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg" ]}
            />
        );
        expect( component.find( ".avatar-box" ).children().length ).toEqual( 1 );
    } );

    describe( "check img size", () => {
        const component = mount(
            <Avatar
                size="35"
                data={[ "https://avatarfiles.alphacoders.com/518/thumb-51852.jpg" ]}
            />
        );
        expect( component.find( ".avatar-box div img" ).prop( "style" ) ).toHaveProperty(
            "height",
            "35px"
        );
    } );
} );
