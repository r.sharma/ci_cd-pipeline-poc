// Priority - init first
export { watchingLabelList } from "services/Label/sagas";
export {
    setActiveModuleStateUserViewWatcher,
    setActiveModuleUserViewWatcher
} from "services/UserView/sagas";

export {
    addBrandToFavoritesWatcher,
    chooseActiveBrandWatcher,
    activeBrandConflictErrorActionWatcher,
    brandsInititialisationWatcher
} from "services/Brand/sagas";

// Common sagas - init after
export {
    cardListWatcher,
    selectCardWatcher,
    selectAllCardWatcher,
    deleteOneCardsWatcher,
    deleteAllSelectedCardsWatcher,
    loadMoreActionWatcher,
    changeOrderDirectionWatcher,
    updateCardItemActionWatcher,
    addCardItemActionWatcher,
    cardActiveBrandSwitchedSPWatcher,
    setActiveModuleStateSPWatcher,
    navigationSPWatcher
} from "pages/SocialProtect/services/Card/sagas";

export {
    goToNextViewDetailsWatcher,
    getNewDataWithChangingUserViewWatcher,
    fetchUserViewSettings,
    pageListViewChangeWatcher,
    tableViewChangeWatcher,
    tableColumnSelectedChangeWatcher,
    tableSequenceColumnsChangeWatcher,
    pageCardDisplayViewChangeWatcher,
    sPShowViewDetailsPanelWatcher
} from "pages/SocialProtect/services/UserView/sagas";

export {
    changeSearchWordProfileWatcher,
    changeSearchWordTitleWatcher,
    filterValuesInObjectSaveWatcher,
    searchByFilterWatcher,
    searchByFilterInTimeWatcher
} from "pages/SocialProtect/services/Options/sagas";

export { startEnforcementWatcher, selectedNewPostMarkWatcher } from "pages/SocialProtect/services/Enforcement/sagas";

export {
    exportDataWatcher,
    exportErrorLinksToCsvWatcher
} from "pages/SocialProtect/components/SideBar/services/ExportData?sagas";

export {
    importDataWatcher,
    sendImportFileWatcher,
    clearImportStoreWatcher,
    copyErrorLinksImportWatcher,
    sendOnlySuccessfullyImportDataWatcher,
    goToThePostsAfterImportWatcher
} from "pages/SocialProtect/components/SideBar/services/ImportData/sagas";

export {
    userPushToRouteWatcher,
    removeChipsWatcher
} from "pages/SocialProtect/components/PageRoute/services/sagas";

export {
    adminUsersShowViewDetailsPanelWatcher,
    adminUsersActiveBrandSwitchedWatcher,
    adminUsersSetActiveModuleWatcher,
    getUsersListWatcher,
    saveUsersDetailWatcher
} from "pages/Administration/services/User/sagas";

export { sideBarStateChangeWatcher } from "services/UserView/sagas";
export { watchingPage } from "services/Navigation/sagas";

export { watchingCountry } from "services/Country/sagas";
export { watchingStatus } from "services/Status/sagas";
export { watchingPlatform } from "services/Platform/sagas";
export { infringementTypeInitialisationWatcher } from "services/InfringementType/sagas";
export { getNewTokenWatcher } from "services/Token/sagas";
export { showAndHideGlobalMessageWatcher } from "services/Message/sagas";
