import React from "react";
import { shallow } from "enzyme";
import { TextInput } from "./TextInput";

describe( "TextInput", () => {
    it( "renders 1 <TextInput /> component", () => {
        const component = shallow( <TextInput /> );
        expect( component ).toHaveLength( 1 );
    } );

    describe( "shallow wrapper", () => {
        const props = {
            label: "labelText",
            labelClassName: "label-default",
            inputClassName: "inputClassName",
            placeholder: "placeholder",
            id: "id",
            wrapperStyle: null,
            childrenStyle: null,
            type: "text",
            meta: {
                touched: null,
                error: null,
                warning: null,
                success: null
            },
            children: null
        };
        const component = shallow( <TextInput {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual( props.labelClassName );
        expect( component.find( ".title" ).text() ).toEqual( props.label );
        const input = component.find( "input" );
        expect( input.props().className ).toEqual( props.inputClassName );
        expect( input.props().placeholder ).toEqual( props.placeholder );
        expect( input.props().id ).toEqual( props.id );
        expect( input.props().type ).toEqual( props.type );
    } );

    describe( "shallow wrapper error", () => {
        const props = {
            label: "labelText",
            labelClassName: "label-default",
            inputClassName: "inputClassName",
            placeholder: "placeholder",
            id: "id",
            wrapperStyle: null,
            childrenStyle: null,
            type: "text",
            meta: {
                touched: true,
                error: "error-message",
                warning: null,
                success: null
            },
            children: null
        };
        const component = shallow( <TextInput {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual(
            props.labelClassName + " error"
        );
        expect( component.find( ".title" ).text() ).toEqual( props.label );
        const input = component.find( "input" );
        expect( input.props().className ).toEqual( props.inputClassName );
        expect( input.props().placeholder ).toEqual( props.placeholder );
        expect( input.props().id ).toEqual( props.id );
        expect( input.props().type ).toEqual( props.type );

        expect( component.find( ".error-message" ).text() ).toEqual( props.meta.error );
    } );

    describe( "shallow wrapper success", () => {
        const props = {
            label: "labelText",
            labelClassName: "label-default",
            inputClassName: "inputClassName",
            placeholder: "placeholder",
            id: "id",
            wrapperStyle: null,
            childrenStyle: null,
            type: "text",
            meta: {
                touched: null,
                error: null,
                warning: null,
                success: true
            },
            children: null
        };
        const component = shallow( <TextInput {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual(
            props.labelClassName + " success"
        );
        expect( component.find( ".title" ).text() ).toEqual( props.label );
        const input = component.find( "input" );
        expect( input.props().className ).toEqual( props.inputClassName );
        expect( input.props().placeholder ).toEqual( props.placeholder );
        expect( input.props().id ).toEqual( props.id );
        expect( input.props().type ).toEqual( props.type );

        expect( component.find( ".error-message" ) ).toHaveLength( 0 );
    } );

    describe( "shallow wrapper with-children", () => {
        const props = {
            label: "labelText",
            labelClassName: "label-default",
            inputClassName: "inputClassName",
            placeholder: "placeholder",
            id: "id",
            wrapperStyle: null,
            childrenStyle: null,
            type: "text",
            meta: {
                touched: null,
                error: null,
                warning: null,
                success: false
            },
            children: () => <div>test</div>
        };
        const component = shallow( <TextInput {...props} /> );
        const instance = component.instance();
        expect( instance ).toEqual( null );

        expect( component.props().className ).toEqual(
            props.labelClassName + " with-children"
        );
        expect( component.find( ".title" ).text() ).toEqual( props.label );
        const input = component.find( "input" );
        expect( input.props().className ).toEqual( props.inputClassName );
        expect( input.props().placeholder ).toEqual( props.placeholder );
        expect( input.props().id ).toEqual( props.id );
        expect( input.props().type ).toEqual( props.type );

        expect( component.find( ".error-message" ) ).toHaveLength( 0 );
    } );
} );
