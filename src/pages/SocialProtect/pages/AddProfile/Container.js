import { connect } from "react-redux";
import { AddProfile as component } from "./AddProfile";
import {
    brandsSelector,
    brandsFavoriteListSelector,
    activeBrandSelector
} from "services/Brand/selectors";
import {
    addBrandToFavoritesAction,
    saveActiveBrandAction
} from "services/Brand/actions";

const mapDispatchToProps = {
    addBrandToFavoritesAction,
    saveActiveBrandAction
};

const mapStateToProps = state => {
    const brandsState = brandsSelector( state );
    const brandsFavoriteListState = brandsFavoriteListSelector( state );
    const activeBrandListState = activeBrandSelector( state );
    return {
        brandsState,
        brandsFavoriteListState,
        activeBrandListState
    };
};

export const SocialProtectAddProfiles = connect(
    mapStateToProps,
    mapDispatchToProps
)( component );
